﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float zoomSpeed;
    public float turnSpeed;

    public City city;
    public TextInfo textInfo;
    public Text cityInfo;

    private Transform tr;
    private Rigidbody rb;

    private Vector3 mouseOrigin;
    private bool isRotating;

    void Start()
    {
        tr = GetComponent<Transform>();
        rb = GetComponent<Rigidbody>();

        cityInfo.text = "";
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = tr.rotation * new Vector3(moveHorizontal, 0, moveVertical);
        movement.y = 0;

        float zoom = Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;
        Vector3 zoomMovement = tr.rotation * new Vector3(0, 0, zoom);

        rb.velocity = movement * speed; ;
        rb.velocity += zoomMovement * 50;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameObject[] selects = GameObject.FindGameObjectsWithTag("Select");
            foreach (GameObject select in selects)
                select.tag = "City";
        }

        if (Input.GetMouseButtonDown(0))
            leftClick();

        if (Input.GetMouseButtonDown(2))
            centerClick();

        cityInfo.text = get_city_informations_text();

        if (Input.GetMouseButtonDown(1))
        {
            mouseOrigin = Input.mousePosition;
            isRotating = true;
        }

        if (!Input.GetMouseButton(1))
            isRotating = false;

        if (isRotating)
        {
            Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - mouseOrigin);

            transform.RotateAround(transform.position, transform.right, -pos.y * turnSpeed);
            transform.RotateAround(transform.position, Vector3.up, pos.x * turnSpeed);
        }
    }

    #region Private methods
    private void leftClick()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        GameObject[] selects = GameObject.FindGameObjectsWithTag("Select");

        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (Physics.Raycast(ray, out hit, 1000.0f))
            {
                if (hit.collider.tag == "City")
                    hit.collider.tag = "Select";
            }
        }
        else if (Physics.Raycast(ray, out hit, 1000.0f))
        {
            if (selects.Length != 0)
                foreach (GameObject select in selects)
                    select.tag = "City";
            if (hit.collider.tag == "City")
                hit.collider.tag = "Select";
        }
    }

    private void centerClick()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        GameObject[] selects = GameObject.FindGameObjectsWithTag("Select");

        if (Physics.Raycast(ray, out hit, 1000.0f) && hit.collider.tag == "Terrain")
        {
            Vector3 position = new Vector3(hit.point.x, hit.point.y + (10 / 3), hit.point.z);
            Instantiate(city, position, new Quaternion());
        }
    }

    private string get_city_informations_text()
    {
        string infos = "";
        GameObject[] selects = GameObject.FindGameObjectsWithTag("Select");
        if (selects.Length != 0)
            infos = "City\nPopulation : 1000";

        return infos;
    }
    #endregion
}
