﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum BiomeType
{
    None = 0x0,
    PreWater,
    PreLand
}

class Biomes
{
    private int FSize;

    private int FBumps;
    private double FStartAngle;
    private double FEndLength;

    public Biomes(int size)
    {
        FSize = size;

        FBumps = UnityEngine.Random.Range(1, 6);
        FStartAngle = UnityEngine.Random.Range(0f, 2 * Convert.ToSingle(Math.PI));
        FEndLength = UnityEngine.Random.Range(size / 4, Convert.ToSingle(Math.Sqrt(Math.Pow(size / 2, 2) * 2)) / 2);
    }

    public void AssignWaterLand()
    {
        foreach (Node node in Graph.Instance().GetNodes())
        {
            if (node.edge == true)
                node.type = BiomeType.PreWater;
            else
            {
                float distanceFromCenter = node.Position.magnitude;
                double angle = Convert.ToSingle(Math.Atan2(node.Position.y, node.Position.x));

                double r1 = 0.5 + 0.40 * Math.Sin(FStartAngle + FBumps * angle + Math.Cos((FBumps + 3) * angle));
                double r2 = 0.5 - 0.40 * Math.Sin(FStartAngle + FBumps * angle - Math.Sin((FBumps + 2) * angle));

                if (r1 * distanceFromCenter > FEndLength * 0.75 || r2 * distanceFromCenter > FEndLength * 0.75)
                    node.type = BiomeType.PreWater;
                else
                    node.type = BiomeType.PreLand;
            }
        }

    }

    public void AssignCornerElevations()
    {
        Queue<Node> openList = new Queue<Node>();

        foreach (Node node in Graph.Instance().GetNodes())
        {
            if (node.corner == true)
            {
                node.height = 0.0f;
                openList.Enqueue(node);
            }
            else
                node.height = Single.PositiveInfinity;
        }

        while (openList.Count != 0)
        {
            Node actual = openList.Dequeue();
            foreach (Node neighbor in actual.Sons.Keys)
            {
                float newElevation = 0.01f + actual.height;
                if (neighbor.type != BiomeType.PreWater)
                    newElevation += 1;

                if (newElevation < neighbor.height)
                {
                    neighbor.height = newElevation;
                    openList.Enqueue(neighbor);
                }
            }
        }
    }

    public void RedistributeElevations()
    {
        float SCALE_FACTOR = 1.1f;
        double x, y;
        int i = 0;
        
        SortedList<Node, float> orderedHeight = new SortedList<Node,float>();
        foreach (Node node in Graph.Instance().GetNodes())
        {
            orderedHeight.Add(node, node.height);
        }
    
        foreach (Node node in orderedHeight.Keys) {
            y = i / (orderedHeight.Count - 1);

            x = Math.Sqrt(SCALE_FACTOR) - Math.Sqrt(SCALE_FACTOR * (1 - y));
            if (x > 1.0)
                x = 1.0;

            node.height = Convert.ToSingle(x);

            i++;
        }
    }
}
