﻿using UnityEngine;
using System;
using Voronoi;

public class LineDrawer : MonoBehaviour
{
	public Material lineMat;

	public LineDrawer()
	{
	}

	public void Awake()
	{
		lineMat = new Material(Shader.Find("Sprites/Default"));
	}

	public void drawVoronoiEdges()
	{
		foreach (Edge edge in GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerController>().voronoi.graph.edges)
		{
			drawLine(edge.va, edge.vb);
		}
	}

	private void drawLine(Point startPoint, Point endPoint)
	{
		Vector3 start = startPoint.ToVector3();
		Vector3 end = endPoint.ToVector3();

		GL.Begin(GL.LINES);
		lineMat.SetPass(0);
		GL.Color(new Color(lineMat.color.r, lineMat.color.g, lineMat.color.b, lineMat.color.a));
		GL.Vertex3(start.x, start.y, start.z);
		GL.Vertex3(end.x, end.y, end.z);
		GL.End();
	}

	void OnPostRender()
	{
		drawVoronoiEdges();
	}
}
