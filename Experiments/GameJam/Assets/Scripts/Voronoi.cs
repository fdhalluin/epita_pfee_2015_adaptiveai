﻿using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class VoronoiGenerator
{
    private List<Voronoi.Point> FSites;
    private Voronoi.FortuneVoronoi FVoronoi;
    public Voronoi.VoronoiGraph graph;

    public VoronoiGenerator()
	{
        FSites = new List<Voronoi.Point>();
        FVoronoi = new Voronoi.FortuneVoronoi();
    }

    public void Execute(int numSites, Bounds bounds, bool clear = true, bool relax = false, int relaxCount = 2)
    {
        CreateSites(numSites, bounds, clear, relax, relaxCount);
    }

    void CreateSites(int numSites, Bounds bounds, bool clear, bool relax, int relaxCount)
    {
        List<Voronoi.Point> sites = new List<Voronoi.Point>();
        if (!clear)
        {
            sites = this.FSites.Take(this.FSites.Count).ToList();
        }

        for (int i = 0; i < numSites; i++)
        {
            Voronoi.Point site = new Voronoi.Point(UnityEngine.Random.Range(bounds.min.x, bounds.max.x), UnityEngine.Random.Range(bounds.min.z, bounds.max.z), 0);
            sites.Add(site);
        }

        Compute(sites, bounds);

        if (relax)
        {
            RelaxSites(relaxCount, bounds);
        }
    }

    void RelaxSites(int iterations, Bounds bounds)
    {
        for (int i = 0; i < iterations; i++)
        {
            if (!this.graph)
            {
                return;
            }

            Voronoi.Point site;
            List<Voronoi.Point> sites = new List<Voronoi.Point>();
            float dist = 0;

            float p = 1 / graph.cells.Count * 0.1f;

            for (int iCell = graph.cells.Count - 1; iCell >= 0; iCell--)
            {
                Voronoi.Cell cell = graph.cells[iCell];
                float rn = Random.value;

                // probability of apoptosis
                if (rn < p)
                {
                    continue;
                }

                site = CellCentroid(cell);
                dist = Distance(site, cell.site);

                // don't relax too fast
                if (dist > 2)
                {
                    site.x = (site.x + cell.site.x) / 2;
                    site.y = (site.y + cell.site.y) / 2;
                }
                // probability of mytosis
                if (rn > (1 - p))
                {
                    dist /= 2;
                    sites.Add(new Voronoi.Point(site.x + (site.x - cell.site.x) / dist, site.y + (site.y - cell.site.y) / dist));
                }
                sites.Add(site);
            }

            Compute(sites, bounds);
        }
    }

    private void Compute(List<Voronoi.Point> sites, Bounds bounds)
    {
        this.FSites = sites;
        this.graph = this.FVoronoi.Compute(sites, bounds);
    }

    float Distance(Voronoi.Point a, Voronoi.Point b)
    {
        float dx = a.x - b.x;
        float dy = a.y - b.y;
        return Mathf.Sqrt(dx * dx + dy * dy);
    }

    Voronoi.Point CellCentroid(Voronoi.Cell cell)
    {
        float x = 0f;
        float y = 0f;
        Voronoi.Point p1, p2;
        float v;

        for (int iHalfEdge = cell.halfEdges.Count - 1; iHalfEdge >= 0; iHalfEdge--)
        {
            Voronoi.HalfEdge halfEdge = cell.halfEdges[iHalfEdge];
            p1 = halfEdge.GetStartPoint();
            p2 = halfEdge.GetEndPoint();
            v = p1.x * p2.y - p2.x * p1.y;
            x += (p1.x + p2.x) * v;
            y += (p1.y + p2.y) * v;
        }
        v = CellArea(cell) * 6;
        return new Voronoi.Point(x / v, y / v);
    }

    float CellArea(Voronoi.Cell cell)
    {
        float area = 0.0f;
        Voronoi.Point p1, p2;

        for (int iHalfEdge = cell.halfEdges.Count - 1; iHalfEdge >= 0; iHalfEdge--)
        {
            Voronoi.HalfEdge halfEdge = cell.halfEdges[iHalfEdge];
            p1 = halfEdge.GetStartPoint();
            p2 = halfEdge.GetEndPoint();
            area += p1.x * p2.y;
            area -= p1.y * p2.x;
        }
        area /= 2;
        return area;
    }
}

