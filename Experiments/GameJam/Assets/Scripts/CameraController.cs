﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    // Movement
    public float moveSpeed;
    public float rotationSpeed;

    private Vector3 targetPosition;

    // Zoom
    public float minZoom;
    public float maxZoom;
    public float sensitivity;
    public float currentZoom;
    public float dampingFactor;
    public AnimationCurve zoomCurve;

    // Rotation
    public float minAngle;
    public float maxAngle;

    private float targetZoom;

    private bool isRotating;
    private float angle;

    void Start()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
        if (Physics.Raycast(ray, out hit, 1000.0f))
        {
            Vector3 dir = transform.position - hit.point;
            Vector3 planeDir = new Vector3(dir.x, 0, dir.z);
            angle = Vector3.Angle(dir, planeDir);
        }
    }

    void Update()
    {
        isRotating = Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl);

        if (isRotating)
            rotate();
        else
            move();

        zoom();

        transform.position = targetPosition - transform.rotation * Vector3.forward * Mathf.Lerp(minZoom, maxZoom, currentZoom);
    }

    private void rotate()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));

        if (Physics.Raycast(ray, out hit, 1000.0f))
        {
            if (minAngle - Input.GetAxis("Vertical") * rotationSpeed <= angle && angle <= maxAngle - Input.GetAxis("Vertical") * rotationSpeed)
                transform.RotateAround(hit.point, transform.rotation * new Vector3(1, 0, 0), Input.GetAxis("Vertical") * rotationSpeed);

            transform.RotateAround(hit.point, new Vector3(0, 1, 0), Input.GetAxis("Horizontal") * rotationSpeed);

            Vector3 dir = transform.position - hit.point;
            Vector3 planeDir = new Vector3(dir.x, 0, dir.z);
            angle = Vector3.Angle(dir, planeDir);
        }
    }

    private void move()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        float yRoation = transform.eulerAngles.y;

        Quaternion moveOrientation = Quaternion.Euler(0, yRoation, 0);
        Vector3 movement = moveOrientation * new Vector3(moveHorizontal, 0, moveVertical);

        targetPosition += movement * moveSpeed * (currentZoom + 0.1f) * Time.deltaTime;
    }

    private void zoom()
    {
        float moveZoom = Input.GetAxis("Mouse ScrollWheel");
        targetZoom -= moveZoom * sensitivity;
        targetZoom = Mathf.Clamp01(targetZoom);
        currentZoom = Utils.Damp(currentZoom, zoomCurve.Evaluate(targetZoom), dampingFactor, Time.deltaTime);
    }
}
