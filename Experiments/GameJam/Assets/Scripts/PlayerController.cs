﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;

public class PlayerController : MonoBehaviour
{
    public GameObject tile;
    public int numSites = 10;
    public int numRelax = 5;
    public int size = 10;

    public VoronoiGenerator voronoi;

    private Bounds FBounds;
    private List<GameObject> FTiles;
    private Biomes FBiomes;

    void Start()
    {
        UnityEngine.Random.seed = 2;

        FTiles = new List<GameObject>();
        FBounds = new Bounds(new Vector3(0, 0, 0), new Vector3(size, 0, size));
        FBiomes = new Biomes(size);

        voronoi = new VoronoiGenerator();

        voronoi.Execute(numSites, FBounds, true, true, numRelax);

        CreateChunks(voronoi);

        FBiomes.AssignWaterLand();
        FBiomes.AssignCornerElevations();
        //FBiomes.RedistributeElevations();
    }

    void CreateChunks(VoronoiGenerator voronoi)
    {
        foreach (GameObject obj in FTiles)
        {
            GameObject.Destroy(obj);
        }

        FTiles.Clear();

        foreach (Voronoi.Cell cell in voronoi.graph.cells)
        {
            GameObject tileClone = Instantiate(tile, cell.site.ToVector3(), Quaternion.identity) as GameObject;
            tileClone.name = "Tile " + cell.site.id;
            tileClone.transform.parent = GameObject.FindGameObjectWithTag("Background").transform;
            FTiles.Add(tileClone);
            tileClone.GetComponent<Tile>().Initiate();
            tileClone.GetComponent<Tile>().CreateFanMesh(cell);
        }

        foreach (Voronoi.Edge edge in voronoi.graph.edges)
        {
            if (edge.lSite != null && edge.rSite != null)
            {
                GameObject lObject = GameObject.Find("/Background/Tile " + edge.lSite.id);
                GameObject rObject = GameObject.Find("/Background/Tile " + edge.rSite.id);

                lObject.GetComponent<Tile>().node.AddSon(rObject.GetComponent<Tile>());
                rObject.GetComponent<Tile>().node.AddSon(lObject.GetComponent<Tile>());
            }
            else if (edge.lSite != null)
            {
                GameObject lObject = GameObject.Find("/Background/Tile " + edge.lSite.id);
                
                if (lObject.GetComponent<Tile>().node.edge == true)
                    lObject.GetComponent<Tile>().node.corner = true;
                else
                    lObject.GetComponent<Tile>().node.edge = true;
            }
            else if (edge.rSite != null)
            {
                GameObject rObject = GameObject.Find("/Background/Tile " + edge.lSite.id);

                if (rObject.GetComponent<Tile>().node.edge == true)
                    rObject.GetComponent<Tile>().node.corner = true;
                else
                    rObject.GetComponent<Tile>().node.edge = true;
            }
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Backspace))
            Destroy(GameObject.FindWithTag("Select"));
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameObject[] selects = GameObject.FindGameObjectsWithTag("Select");
            foreach (GameObject select in selects)
                select.tag = "Node";
            foreach (GameObject neighbor in GameObject.FindGameObjectsWithTag("Neighbor"))
                neighbor.tag = "Node";
        }

        #region MouseLeftClick Handling
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            float rayLimit = (float)Math.Sqrt(Math.Pow((double)transform.position.y, 2) + Math.Pow((double)FBounds.extents.x * Math.Sqrt(2), 2));
            GameObject[] selects = GameObject.FindGameObjectsWithTag("Select");
            if (Input.GetKey(KeyCode.LeftShift))
            {
                if (Physics.Raycast(ray, out hit, rayLimit))
                {
                    if (hit.collider.tag == "Node")
                        hit.collider.tag = "Select";
                }
            }
            else if (Input.GetKey(KeyCode.LeftControl))
            {
                if (Physics.Raycast(ray, out hit, rayLimit))
                {
                    Node actualNode = hit.collider.GetComponent<Tile>().node;
                    foreach (GameObject select in selects)
                    {
                        select.GetComponent<Tile>().node.AddSon(actualNode);
                    }
                }
            }
            else if (Input.GetKey(KeyCode.LeftAlt))
            {
                if (selects.Length == 1)
                {
                    if (Physics.Raycast(ray, out hit, rayLimit))
                    {
                        Node actualNode = selects[0].GetComponent<Tile>().node;
                        foreach (GameObject select in selects)
                            select.tag = "Node";
                        if (hit.collider.tag == "Node")
                        {
                            foreach (Node node in PathFinding.pathFinding(actualNode, hit.collider.GetComponent<Tile>().node))
                            {
                                node.Tile.tag = "Select";
                            }
                        }
                    }
                }
            }
            else
            {
                if (Physics.Raycast(ray, out hit, rayLimit))
                {
                    Debug.Log("HERE");
                    if (selects.Length != 0)
                    {
                        foreach (GameObject select in selects)
                            select.tag = "Node";
                        foreach (GameObject neighbor in GameObject.FindGameObjectsWithTag("Neighbor"))
                            neighbor.tag = "Node";
                    }
                    if (hit.collider.tag == "Node")
                    {
                        hit.collider.tag = "Select";
                        foreach (Node son in hit.collider.gameObject.GetComponent<Tile>().node.Sons.Keys)
                        {
                            son.Tile.tag = "Neighbor";
                        }
                    }
                    else if (hit.collider.tag == "Ground")
                    {
                        Instantiate(tile,
                                    ray.GetPoint(hit.distance),
                                    new Quaternion());
                    }
                }
            }
        }
        #endregion
    }
}
