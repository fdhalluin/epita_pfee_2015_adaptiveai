﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Voronoi
{
    public class Edge
    {
        public Point lSite;
        public Point rSite;

        public Point va;
        public Point vb;

        public Edge(Point lSite = null, Point rSite = null)
        {
            this.lSite = lSite;
            this.rSite = rSite;
            this.va = null;
            this.vb = null;
        }

        public void SetStartPoint(Point lSite, Point rSite, Point vertex)
        {
            if (!this.va && !this.vb)
            {
                this.va = vertex;
                this.lSite = lSite;
                this.rSite = rSite;
            }
            else if (this.lSite == rSite)
            {
                this.vb = vertex;
            }
            else
            {
                this.va = vertex;
            }
        }

        public void SetEndPoint(Point lSite, Point rSite, Point vertex)
        {
            this.SetStartPoint(rSite, lSite, vertex);
        }

        public static implicit operator bool(Edge a)
        {
            return a != null;
        }
    }
}