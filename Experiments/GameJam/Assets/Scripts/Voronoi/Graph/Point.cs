﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Voronoi
{
    public class Point
    {
        public float x;
        public float y;
        public float z;

        public int id;

        public List<HalfEdge> halfEdges;

        public Point(float x, float y, float z = 0)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.halfEdges = new List<HalfEdge>();
        }

        public Point SetId(int id)
        {
            this.id = id;
            return this;
        }

        public static implicit operator bool(Point a)
        {
            return a != null;
        }

        public override string ToString()
        {
            return string.Concat("(", this.x, ", ", this.y, ", ", this.z, ")");
        }
		
		public Vector3 ToVector3()
		{
			return new Vector3(this.x, this.z, this.y);
		}
    }
}