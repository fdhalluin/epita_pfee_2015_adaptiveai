﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;

public class PathFinding
{
    static public List<Node> pathFinding(Node start, Node goal)
    {
        List<Node> closedList = new List<Node>();
        List<Node> openList = new List<Node>();

        Dictionary<Node, double> gScore = new Dictionary<Node, double>();
        Dictionary<Node, double> fScore = new Dictionary<Node, double>();

        Dictionary<Node, Node> origin = new Dictionary<Node, Node>();

        openList.Add(start);
        gScore.Add(start, 0);
        fScore.Add(start, manhattanDistance(start, goal));

        while (openList.Count != 0)
        {
            Node actual = popLowest(openList, fScore);
            if (actual.Tile.name.Equals(goal.Tile.name))
                return reconstructPath(origin, actual);
            
            closedList.Add(actual);

            foreach (Node node in actual.Sons.Keys)
            {
                if (closedList.Contains(node))
                    continue;

                double actualScore = gScore[actual];

                if (!openList.Contains(actual) || gScore[actual] > actualScore)
                {
                    origin[node] = actual;
                    gScore[node] = actualScore;
                    fScore[node] = gScore[node] + manhattanDistance(node, goal);

                    if (!openList.Contains(node))
                        openList.Add(node);
                }
            }
        }

        return null;
    }

    static private Node popLowest(List<Node> openList, Dictionary<Node, double> score)
    {
        Node result = null;
        double valueLowest = double.MaxValue;

        foreach (Node node in openList)
        {
            if (score.ContainsKey(node) && valueLowest > score[node])
            {
                result = node;
                valueLowest = score[node];
            }
        }

        openList.Remove(result);
        return result;
    }

    static private float manhattanDistance(Node start, Node goal)
    {
        return Math.Abs(goal.Position.x - start.Position.x)
            + Math.Abs(goal.Position.y - start.Position.y);
    }

	static private float sqEuclideanDistance(Node par_start, Node par_end)
	{
		float tmpHeight = par_end.height - par_start.height;
		return (par_end.Position - par_start.Position).SqrMagnitude() - tmpHeight * tmpHeight;
	}

    static private List<Node> reconstructPath(Dictionary<Node, Node> origin, Node actual)
    {
        List<Node> path = new List<Node>();

        path.Add(actual);

        while (origin.ContainsKey(actual))
        {
            actual = origin[actual];
            path.Insert(0, actual);
        }

        return path;
    }
}
