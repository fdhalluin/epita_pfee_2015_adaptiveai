﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;

public class Graph
{
    //============
    // Singleton
    //============

    private static Graph FInstance;

    static public Graph Instance()
    {
        if (FInstance == null)
            FInstance = new Graph();
        return FInstance;
    }

    //============
    // Variables
    //============

    private Dictionary<string, Node> FGraph;

    //============
    // Des/Constructor
    //============

    private Graph()
    {
        FGraph = new Dictionary<string, Node>();
    }

    //============
    // Other methods
    //============

    public Node AddNode(Tile tile)
    {
        Node tmp = new Node(tile);
        FGraph.Add(tmp.Tile.name, tmp);

        return tmp;
    }

    public Node GetNode(string key)
    {
        return FGraph[key];
    }

    public void RenameNode(string key, string newKey)
    {
        Node node = FGraph[key];
        FGraph.Remove(key);
        FGraph.Add(newKey, node);
        node.Tile.name = newKey;
    }

    public string GetRandomNode()
    {
        string[] keys = new string[FGraph.Count];
        FGraph.Keys.CopyTo(keys, 0);

        return keys[UnityEngine.Random.Range(0, FGraph.Count)];
    }

    public void RemoveNode(Node key)
    {
        FGraph.Remove(key.Tile.name);

        foreach (Node station in FGraph.Values)
        {
            if (station.Sons.ContainsKey(key))
                station.Sons.Remove(key);
        }
    }

    public Dictionary<string, Node>.ValueCollection GetNodes()
    {
        return FGraph.Values;
    }

    public bool Exists(string key)
    {
        return FGraph.ContainsKey(key);
    }

    public int GetSize()
    {
        return FGraph.Count;
    }
}
