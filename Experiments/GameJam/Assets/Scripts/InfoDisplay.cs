﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InfoDisplay : MonoBehaviour
{
    public string text;

	void Start ()
    {
	
	}
	
	void Update ()
    {
        GameObject selected = GameObject.FindGameObjectWithTag("Select");
        if (selected)
        {
            gameObject.GetComponent<Canvas>().enabled = true;
            text = selected.GetComponent<Tile>().node.height + "\n" + selected.GetComponent<Tile>().node.type.ToString();
            ChangeDisplay();
        }
        else
            gameObject.GetComponent<Canvas>().enabled = false;
	}

    public void ChangeDisplay()
    {
        GetComponentInChildren<Text>().text = text;
    }
}