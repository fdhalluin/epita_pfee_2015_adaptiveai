﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Text;

[System.Serializable]
public class Node
{
    //============
    // Variables
    //============

    private Tile FTile;

    private Dictionary<Node, float> FSons;
    private Vector2 FPosition;

    public string text;

    public BiomeType type;
    public bool edge;
    public bool corner;
    public float height;

    #region Getter / Setter
    //============
    // Se/Getter
    //============

    public Dictionary<Node, float> Sons
    {
        get { return FSons; }
    }

    public Tile Tile
    {
        get { return FTile; }
    }

    public Vector2 Position
    {
        get { return FPosition; }
    }
    #endregion

    //============
    // Des/Constructor
    //============

    public Node(Tile tile)
    {
        FTile = tile;
        FPosition = new Vector2(tile.transform.position.x, tile.transform.position.z);
        FSons = new Dictionary<Node, float>();

        this.height = 0.0f;
        this.type = BiomeType.None;
        this.edge = false;
    }

    //============
    // Other methods
    //============

    public void AddSon(Node node)
    {
        if (Graph.Instance().Exists(node.Tile.name) && FTile.name != node.Tile.name)
        {
            FSons.Add(node, 1.0f);
        }
    }

    public void AddSon(Tile tile)
    {
        if (Graph.Instance().Exists(tile.name) && FTile.name != tile.name)
        {
            FSons.Add(tile.node, 1.0f);
        }
    }

    public void RemoveSon(Node node)
    {
        if (FSons.ContainsKey(node))
        {
            FSons.Remove(node);
        }
    }

    public Node RandomSon()
    {
        Node[] keys = new Node[FSons.Count];
        FSons.Keys.CopyTo(keys, 0);

        return keys[UnityEngine.Random.Range(0, FSons.Count)];
    }
}

public class Tile : MonoBehaviour
{
    public Node node;
    public Material material;
    private Mesh mesh;

    public void Initiate()
    {
        node = Graph.Instance().AddNode(this);
    }

    public void CreateFanMesh(Voronoi.Cell cell)
    {
        if (cell.halfEdges.Count > 0)
        {
            this.GetComponent<MeshFilter>().sharedMesh = this.mesh = new Mesh();
            this.name = "Tile " + cell.site.id;

            Vector3[] vertices = new Vector3[cell.halfEdges.Count + 1];
            int[] triangles = new int[(cell.halfEdges.Count + 0) * 3];

            vertices[0] = cell.site.ToVector3() - this.transform.position;
            triangles[0] = 0;
            for (int v = 1, t = 1; v < vertices.Length; v++, t += 3)
            {
                vertices[v] = cell.halfEdges[v - 1].GetStartPoint().ToVector3() - this.transform.position;
                triangles[t] = v;
                triangles[t + 1] = v + 1;
            }
            triangles[triangles.Length - 1] = 1;

            this.mesh.vertices = vertices;
            this.mesh.triangles = triangles;
            this.mesh.RecalculateBounds();
            this.mesh.RecalculateNormals();

            this.GetComponent<MeshCollider>().sharedMesh = this.mesh;

            this.GetComponent<Renderer>().sharedMaterial = this.material;
        }
    }
    
    void Update()
    {
        if (this.node.type == BiomeType.PreWater)
            this.GetComponent<MeshRenderer>().material.color = Color.blue;
        else if (this.node.type == BiomeType.PreLand)
            this.GetComponent<MeshRenderer>().material.color = new Color(0, 255 / (255 * node.height), 0);
        else
        {
            if (this.tag == "Select")
                this.GetComponent<MeshRenderer>().material.color = Color.green;
            else if (this.tag == "Neighbor")
                this.GetComponent<MeshRenderer>().material.color = Color.yellow;
            else
                this.GetComponent<MeshRenderer>().material.color = Color.grey;
        }
    }

    void OnDestroy()
    {
        Graph.Instance().RemoveNode(node);
        node = null;
    }
}
