﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;

public class PlayerController : MonoBehaviour
{
    // Movement
    public float speed;

    // Zoom
    public float minZoom;
    public float maxZoom;
    public float sensitivity;

    public Station station;
    public Metro metro;
    public TextInfo textInfo;

    public List<string> pathBuilt = new List<string>();

    private Color[] FLineColors;
    private int FColorIndex;

    void Start()
    {
        setLineColors();
    }

    void Update()
    {
		if (Input.GetKeyDown(KeyCode.Space))
		{
			uint nb_stations = 30;
			Vector3[] stations = Graph.GenerateFromSeed(nb_stations, UnityEngine.Random.Range(0, int.MaxValue));
			string[] stations_names = new string[stations.Length];
			for (uint i = 0; i < stations.Length; i++)
			{
				Station tmp_station = Instantiate(station, stations[i], new Quaternion()) as Station;
				string orig_name = tmp_station.node.name;
				string new_name = "Station " + (i+1).ToString();
				stations_names[i] = new_name;
				Graph.Instance().RenameNode(orig_name, new_name);
			}

			uint[][] lines = Graph.GenerateLines(stations);
			for (uint i = 0; i < lines.Length; i++)
			{
				List<string> line_stations_name = new List<string>(lines[i].Length);
				for (uint j = 0; j < lines[i].Length; j++)
				{
					line_stations_name.Add(stations_names[lines[i][j]]);
				}
				LineHandler.Instance().AddLine((int)i, line_stations_name);
			}
		}
        else if (Input.GetKeyDown(KeyCode.Backspace))
            Destroy(GameObject.FindWithTag("Select"));
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameObject[] selects = GameObject.FindGameObjectsWithTag("Select");
            foreach (GameObject select in selects)
                select.tag = "Station";
        }
        else if (Input.GetKeyDown(KeyCode.T))
        {
            GameObject[] selects = GameObject.FindGameObjectsWithTag("Select");
            foreach (GameObject select in selects)
            {
                Node node = select.GetComponent<Station>().node;
                node.AddTraveler(new Agent.Traveler(node.name));
            }
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                GameObject[] selects = GameObject.FindGameObjectsWithTag("Select");
                foreach (GameObject select in selects)
                    pathBuilt.Add(select.GetComponent<Station>().node.name);
            }
            else if (Input.GetKeyDown(KeyCode.C))
            {
                if (pathBuilt.Count != 0)
                {
                    LineHandler.Instance().AddLine(LineHandler.Instance().GetSize(), pathBuilt);
                    pathBuilt.Clear();
                }
            }
        }

        #region MouseLeftClick Handling
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            GameObject[] selects = GameObject.FindGameObjectsWithTag("Select");
            if (Input.GetKey(KeyCode.LeftShift))
            {
                if (Physics.Raycast(ray, out hit, 100.0f))
                {
                    if (hit.collider.tag == "Station")
                        hit.collider.tag = "Select";
                }
            }
            else
            {
                if (Physics.Raycast(ray, out hit, 100.0f))
                {
                    if (selects.Length != 0)
                        foreach (GameObject select in selects)
                            select.tag = "Station";
                    if (hit.collider.tag == "Station")
                        hit.collider.tag = "Select";
                    else if (hit.collider.tag == "Ground")
                    {
                        Vector3 position = GetComponent<Camera>().transform.position;
                        position.y = 0;
                        Instantiate(station,
                                    ray.GetPoint(hit.distance),
                                    new Quaternion());
                    }
                }
            }
        }
        #endregion

        Agent.AgentHandler.Instance().Update();
    }

    void FixedUpdate()
    {
        float zoom = GetComponent<Rigidbody>().position.y;
        zoom -= Input.GetAxis("Mouse ScrollWheel") * sensitivity;

        Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        GetComponent<Rigidbody>().velocity = movement * speed;

        GetComponent<Rigidbody>().position = new Vector3(GetComponent<Rigidbody>().position.x, Mathf.Clamp(zoom, minZoom, maxZoom), GetComponent<Rigidbody>().position.z);
    }

    public Color GetNextLineColor()
    {
        Color result = FLineColors[FColorIndex];
        FColorIndex = (FColorIndex + 1) % FLineColors.Length;

        return result;
    }

    private void setLineColors()
    {
        FColorIndex = 0;
        FLineColors = new Color[11];
        FLineColors[0] = Color.yellow;
        FLineColors[1] = Color.red;
        FLineColors[2] = new Color(98f / 255f, 34f / 255f, 128f / 255f, 1);     // Violet
        FLineColors[3] = new Color(0, 101f / 255f, 174f / 255f, 1);             // Bleu
        FLineColors[4] = new Color(159f / 255f, 151f / 255f, 26f / 255f, 1);    // Kaki
        FLineColors[5] = new Color(140f / 255f, 94f / 255f, 36f / 255f, 1);     // Marron
        FLineColors[6] = new Color(241f / 255f, 144f / 255f, 67f / 255f, 1);    // Orange
        FLineColors[7] = new Color(242f / 255f, 164f / 255f, 183f / 255f, 1);  // Rose
        FLineColors[8] = Color.green;
        FLineColors[9] = Color.magenta;
        FLineColors[10] = Color.cyan;
    }
}
