﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agent
{
    public enum VehicleState
    {
        None = 0x0,
        InStation,
        Travelling,
        Departing,
        Arriving
    }

    public enum TravelerState
    {
        None = 0x0,
        InVehicle,
        InStation,
        LeavingVehicle,
        EnteringVehicle,
        Arrived
    }

    public enum AgentType
    {
        None = 0x0,
        Traveler,
        Vehicle
    }

    public abstract class Agent
    {
        public string FOrigine;
        public string FDestination;

        #region Getter / Setter
        public string Origine
        {
            get { return FOrigine; }
        }

        public string Destination
        {
            get { return FDestination; }
        }
        #endregion

        public abstract void Update();
    }

    public class AgentHandler
    {

        //============
        // Singleton
        //============

        private static AgentHandler FInstance;

        static public AgentHandler Instance()
        {
            if (FInstance == null)
                FInstance = new AgentHandler();
            return FInstance;
        }

        //============
        // Variables
        //============

        private List<Traveler> FTravelers;
        private List<Vehicle> FVehicles;

        //============
        // Des/Constructor
        //============

        private AgentHandler()
        {
            FTravelers = new List<Traveler>();
            FVehicles = new List<Vehicle>();
        }

        //============
        // Other methods
        //============

        public void Update()
        {
            foreach (Traveler traveler in FTravelers)
            {
                if (traveler.State == TravelerState.Arrived || traveler.State == TravelerState.InStation || traveler.State == TravelerState.LeavingVehicle)
                    traveler.Update();
            }

            foreach (Vehicle vehicle in FVehicles)
            {
                vehicle.Update();
            }
        }

        public void AddAgent(Agent agent, AgentType type)
        {
            if (type == AgentType.Traveler)
                FTravelers.Add((Traveler)agent);
            else if (type == AgentType.Vehicle)
                FVehicles.Add((Vehicle)agent);
        }
    }

    public class Traveler : Agent
    {
        private Vehicle FCurrentVehicle;
        private List<string> FPath;

        private TravelerState FState;
        private TravelerState FNextState;

        private Dictionary<KeyValuePair<string, string>, float> FInfos;
        private bool FInfoChanged;

        private float FTimeSinceLastUpdate;

        #region Getter / Setter
        public TravelerState State
        {
            get { return FState; }
        }
        #endregion

        public Traveler(string position)
        {
            FOrigine = position;
            FDestination = null;
            FCurrentVehicle = null;

            FState = TravelerState.Arrived;
            FNextState = TravelerState.Arrived;

            FInfos = new Dictionary<KeyValuePair<string, string>, float>();
            FInfoChanged = false;

            FTimeSinceLastUpdate = 0;

            AgentHandler.Instance().AddAgent(this, AgentType.Traveler);
        }

        public void EnterVehicleTo(Vehicle vehicle)
        {
            FCurrentVehicle = vehicle;
            FDestination = FindDestination();

            vehicle.AddTraveler(this);
            Graph.Instance().GetNode(FOrigine).RemoveTraveler(this);
        }

        public void ExitVehicleTo(string station)
        {
            FCurrentVehicle.RemoveTraveler(this);

            FCurrentVehicle = null;
            FDestination = null;
            FOrigine = station;
            Graph.Instance().GetNode(FOrigine).AddTraveler(this);
        }

        public override void Update()
        {
            switch (FState)
            {
                #region InVehicle
                case TravelerState.InVehicle:
                    if (FCurrentVehicle.State == VehicleState.Arriving && FPath.Count != 0)
                            FPath.RemoveAt(0);

                    if (FCurrentVehicle.State == VehicleState.InStation && FCurrentVehicle.Origine == FDestination)
                        FNextState = TravelerState.LeavingVehicle;

                    break;
                #endregion
                #region LeavingVehicle
                case TravelerState.LeavingVehicle:
                    ExitVehicleTo(FDestination);
                    FNextState = TravelerState.InStation;
                    break;
                #endregion
                #region InStation
                case TravelerState.InStation:
                    if (FPath.Count == 0)
                        FNextState = TravelerState.Arrived;
                    else
                    {
                        Node node = Graph.Instance().GetNode(FOrigine);

                        // Add and remove infos from station
                        UpdateInfos(node);

                        // Exit Vehicle if line destination is reached
                        foreach (Vehicle vehicle in node.Vehicles)
                        {
                            if (vehicle.State == VehicleState.InStation
                                && vehicle.Travelers.Count + 1 <= vehicle.TravelerCapacity
                                && vehicle.Destination == FPath[0])
                            {
                                EnterVehicleTo(vehicle);
                                FNextState = TravelerState.InVehicle;
                                break;
                            }
                        }
                    }
                    break;
                #endregion
                #region Arrived
                case TravelerState.Arrived:
                    if (UnityEngine.Random.Range(0, 100) == 99)
                    {
                        FDestination = Graph.Instance().GetRandomNode();
                        FPath = PathFinding.pathFinding(Graph.Instance().GetNode(FOrigine), Graph.Instance().GetNode(FDestination), FInfos);
                        if (FPath != null)
                        {
                            FPath.RemoveAt(0);
                            FNextState = TravelerState.InStation;
                        }
                    }
                    break;
                #endregion
                default:
                    break;
            }
            FState = FNextState;
        }

        public void AddInfo(KeyValuePair<string, string> stations, float value)
        {
            FInfos[stations] = value;
            FInfoChanged = true;
        }

        public void RemoveInfo(KeyValuePair<string, string> stations)
        {
            if (FInfos.ContainsKey(stations))
                FInfos.Remove(stations);
            FInfoChanged = true;
        }

        private void UpdateInfos(Node node)
        {
            if (node.HasInfos)
            {
                foreach (KeyValuePair<string, string> stations in node.Infos.Keys)
                {
                    float u1 = UnityEngine.Random.Range(0, 1);
                    float u2 = UnityEngine.Random.Range(0, 1);
                    float randStdNormal = Mathf.Sqrt(-2.0f * Mathf.Log(u1)) * Mathf.Sin(2.0f * Mathf.PI * u2);
                    float randNormal = 0.5f + 20 * randStdNormal;
                    AddInfo(stations, node.Infos[stations] + randNormal);
                }
            }

            float time = Time.realtimeSinceStartup;

            List<KeyValuePair<string, string>> keys = new List<KeyValuePair<string, string>>(node.Infos.Keys);

            foreach (KeyValuePair<string, string> stations in keys)
            {
                FInfos[stations] -= time - FTimeSinceLastUpdate;
                if (FInfos[stations] <= 0)
                    RemoveInfo(stations);
            }
            FTimeSinceLastUpdate = Time.realtimeSinceStartup;

            if (FInfoChanged)
            {
                FInfoChanged = false;
                Node start = Graph.Instance().GetNode(FOrigine);
                Node goal = Graph.Instance().GetNode(FPath[FPath.Count - 1]);
                FPath = PathFinding.pathFinding(start, goal, FInfos);
            }
        }

        private string FindDestination()
        {
            string destination = FOrigine;
            int line = FCurrentVehicle.Line;

            foreach (string station in FPath)
            {
                List<int> dest_lines = Graph.Instance().GetNode(station).Lines;
                if (!dest_lines.Contains(line))
                    return destination;

                destination = station;
            }

            return destination;
        }
    }

    [System.Serializable]
    public class Vehicle : Agent
    {
        public int lineNumber;

        private double FProgression;
        private List<Traveler> FTravelers;
        private int FTravelerCapacity;

        private VehicleState FState;
        private VehicleState FNextState;
        private bool FDirection;

        private Vector2 FDirectionVector2;
        private double FDistance;

        private float FTimeSinceLastUpdate;

        private GameObject FDisplay;

        public Vehicle(int line, GameObject display, int capacity)
        {
            FProgression = 0.0f;
            FTravelers = new List<Traveler>();

            this.lineNumber = line;

            FOrigine = LineHandler.Instance().GetLine(line).GetComponent<Line>().Stations[0];
            FDestination = LineHandler.Instance().GetLine(line).GetComponent<Line>().Stations[1];


            FState = VehicleState.InStation;
            FNextState = VehicleState.Departing;
            FDirection = true;

            FDisplay = display;

            FTravelerCapacity = capacity;

            AgentHandler.Instance().AddAgent(this, AgentType.Vehicle);
        }

        #region Getter / Setter
        public float Progression
        {
            get { return (float)FProgression; }
        }

        public int Line
        {
            get { return lineNumber; }
        }

        public List<Traveler> Travelers
        {
            get { return FTravelers; }
        }

        public VehicleState State
        {
            get { return FState; }
        }

        public Vector2 DirectionVector2
        {
            get { return FDirectionVector2; }
        }

        public GameObject Display
        {
            get { return FDisplay; }
        }

        public int TravelerCapacity
        {
            get { return FTravelerCapacity; }
        }
        #endregion

        public void AddTraveler(Traveler traveler)
        {
            FTravelers.Add(traveler);
        }

        public void RemoveTraveler(Traveler traveler)
        {
            if (FTravelers.Contains(traveler))
                FTravelers.Remove(traveler);
        }

        public override void Update()
        {
            foreach (Traveler traveler in FTravelers)
                traveler.Update();

            FState = FNextState;
            Line line = LineHandler.Instance().GetLine(lineNumber);
            switch (FState)
            {
                #region Arriving
                case VehicleState.Arriving:
                    FOrigine = FDestination;

                    if (FOrigine == line.Stations[0] || FOrigine == line.Stations[line.Stations.Count - 1])
                        FDirection = !FDirection;

                    int position = line.Stations.LastIndexOf(FOrigine);

                    if (FDirection)
                        FDestination = line.Stations[position + 1];
                    else
                        FDestination = line.Stations[position - 1];

                    FTimeSinceLastUpdate = Time.realtimeSinceStartup;

                    Graph.Instance().GetNode(FOrigine).AddVehicle(this);

                    FNextState = VehicleState.InStation;
                    break;
                #endregion
                #region Departing
                case VehicleState.Departing:
                    Node origine = Graph.Instance().GetNode(FOrigine);
                    Node destination = Graph.Instance().GetNode(FDestination);

                    origine.RemoveVehicle(this);

                    FDirectionVector2 = new Vector2(destination.Position.x - origine.Position.x, destination.Position.y - origine.Position.y);
                    FDistance = Math.Sqrt(Math.Pow(destination.Position.x - origine.Position.x, 2.0f) + Math.Pow(destination.Position.y - origine.Position.y, 2.0f));

                    FTimeSinceLastUpdate = Time.realtimeSinceStartup;
                    FProgression = 0.0f;
                    FNextState = VehicleState.Travelling;
                    break;
                #endregion
                #region InStation
                case VehicleState.InStation:
                    if (Time.realtimeSinceStartup - FTimeSinceLastUpdate >= LineHandler.waitTimePerType[line.LineType])
                        FNextState = VehicleState.Departing;
                    break;
                #endregion
                #region Travelling
                case VehicleState.Travelling:
                    FProgression += (((Time.realtimeSinceStartup - FTimeSinceLastUpdate) * LineHandler.speedPerType[line.LineType]) / FDistance) * Graph.Instance().GetNode(FOrigine).Sons[FDestination];
                    if (FProgression >= 1.0f)
                    {
                        FProgression = 1.0f;
                        FNextState = VehicleState.Arriving;
                    }
                    FTimeSinceLastUpdate = Time.realtimeSinceStartup;
                    break;
                #endregion
                default:
                    break;
            }
        }
    }
}
