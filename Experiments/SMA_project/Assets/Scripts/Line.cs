﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;


public enum Type
{
    None = 0x0,
    Metro,
    Bus
}

public class LineHandler
{
    //============
    // Global
    //============

    // Speed is expressed as distance unite by time unite
    public static Dictionary<Type, float> speedPerType = new Dictionary<Type, float>();

    // WaitTime is the number of time unite to wait for the vehicule to depart
    public static Dictionary<Type, float> waitTimePerType = new Dictionary<Type, float>();
    
    //============
    // Singleton
    //============

    private static LineHandler FInstance;

    static public LineHandler Instance()
    {
        if (FInstance == null)
        {
            FInstance = new LineHandler();
            foreach (Type type in Enum.GetValues(typeof(Type)))
            {
                speedPerType.Add(type, 2);
                waitTimePerType.Add(type, 1);
            }
        }
        return FInstance;
    }

    //============
    // Variables
    //============

    private Dictionary<int, GameObject> FLines;

    //============
    // Des/Constructor
    //============

    private LineHandler()
    {
        FLines = new Dictionary<int, GameObject>();
    }

    //============
    // Other methods
    //============

    public Line AddLine(int key, List<string> stations)
    {
        GameObject obj = new GameObject("Line" + FLines.Count);
        FLines.Add(key, obj);
        Line tmp = obj.AddComponent<Line>().Initiate(key, stations, Type.Metro);


        for (int i = 0; i < stations.Count; i++)
        {
            Node node = Graph.Instance().GetNode(stations[i]);
            if (i - 1 >= 0)
                node.AddSon(stations[i - 1]);
            if (i + 1 < stations.Count)
                node.AddSon(stations[i + 1]);
        }

        return tmp;
    }

    public Line GetLine(int key)
    {
        return FLines[key].GetComponent<Line>();
    }

    public Dictionary<int, GameObject>.KeyCollection GetLineList()
    {
        return FLines.Keys;
    }

    public void RemoveLine(int key)
    {
        GameObject obj = FLines[key];
        Line line = obj.GetComponent<Line>();

        foreach (string station in line.Stations)
        {
            Graph.Instance().GetNode(station).RemoveLine(key);

            // TODO: Missing clean remove of all the sons
        }

        MonoBehaviour.Destroy(obj);
        FLines.Remove(key);
    }

    public void RemoveStation(string station)
    {
        foreach (GameObject obj in FLines.Values)
        {
            if (obj != null)
            {
                Line line = obj.GetComponent<Line>();
                for (int i = 0; i < line.Stations.Count; i++)
                    if (line.Stations[i] == station)
                    {
                        if (i > 0)
                        {
                            Node node = Graph.Instance().GetNode(line.Stations[i - 1]);
                            node.RemoveSon(station);
                            if (i < (line.Stations.Count - 1))
                                node.AddSon(line.Stations[i + 1]);
                        }
                        if (i < (line.Stations.Count - 1))
                        {
                            Node node = Graph.Instance().GetNode(line.Stations[i + 1]);
                            node.RemoveSon(station);
                            if (i > 0)
                                node.AddSon(line.Stations[i - 1]);
                        }
                        line.Stations.RemoveAt(i);
                    }
            }
        }
    }

    public bool Exists(int key)
    {
        return FLines.ContainsKey(key);
    }

    public int GetSize()
    {
        return FLines.Count;
    }

}

public class Line : MonoBehaviour
{
    public Color color;
    public int lineNumber;

    private List<string> FStations;
    private List<Agent.Vehicle> FVehicle;
    private Type FType;

    private LineRenderer FLineRenderer;

    public Line Initiate(int key, List<string> stations, Type type)
    {
        color = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerController>().GetNextLineColor();

        lineNumber = key;
        FStations = new List<string>();
        foreach (string station in stations)
        {
            FStations.Add(station);
            Graph.Instance().GetNode(station).AddLine(key);
        }
        FVehicle = new List<Agent.Vehicle>();
        FType = type;

        return this;
    }

    #region Getter/Setter
    public List<string> Stations
    {
        get { return FStations; }
    }

    public Type LineType
    {
        get { return FType; }
    }
    #endregion

    void Start()
    {
        // Line Renderer
        FLineRenderer = gameObject.AddComponent<LineRenderer>();
        FLineRenderer.material = new Material(Shader.Find("Sprites/Default"));
        FLineRenderer.SetColors(color, color);
        FLineRenderer.SetWidth(0.08F, 0.08F);
        FLineRenderer.SetVertexCount(FStations.Count);

        for (int i = 0; i < FStations.Count; i++)
        {
            Node node = Graph.Instance().GetNode(FStations[i]);
            FLineRenderer.SetPosition(i, new Vector3(node.Position.x, 0, node.Position.y));
        }
    }

    public void AddVehicle()
    {
        Node node = Graph.Instance().GetNode(FStations[0]);

        Metro clone = Instantiate(
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerController>().metro,
            new Vector3(node.Position.x, 0, node.Position.y),
            new Quaternion()) as Metro;
        clone.vehicle = new Agent.Vehicle(lineNumber, clone.gameObject, 50);
        FVehicle.Add(clone.vehicle);
        node.AddVehicle(clone.vehicle);
    }

    public void DeleteVehicle()
    {
        if (FVehicle.Count > 0)
        {
            Graph.Instance().GetNode(FVehicle[0].Origine).RemoveVehicle(FVehicle[0]);
            Destroy(FVehicle[0].Display);
            FVehicle.RemoveAt(0);
        }
    }

    public void RenameStation(string key, string newKey)
    {
        for (int i = 0; i < FStations.Count; i++)
        {
            if (FStations[i] == key)
            {
                FStations.RemoveAt(i);
                FStations.Insert(i, newKey);
            }
        }
    }
}
