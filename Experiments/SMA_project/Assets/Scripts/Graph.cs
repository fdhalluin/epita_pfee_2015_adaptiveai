﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;

public class Graph
{
    //============
    // Singleton
    //============

    private static Graph FInstance;

    static public Graph Instance()
    {
        if (FInstance == null)
            FInstance = new Graph();
        return FInstance;
    }

    //============
    // Variables
    //============

    private Dictionary<string, Node> FGraph;

    //============
    // Des/Constructor
    //============

    private Graph()
    {
        FGraph = new Dictionary<string, Node>();
    }

    //============
    // Other methods
    //============

    public Node AddNode(string key, GameObject station)
    {
        Node tmp = new Node(key, station);
        FGraph.Add(key, tmp);

        return tmp;
    }

    public Node GetNode(string key)
    {
        return FGraph[key];
    }

    public void RenameNode(string key, string newKey)
    {
        Node node = FGraph[key];
        FGraph.Remove(key);
        FGraph.Add(newKey, node);
        node.name = newKey;

        foreach (int line in node.Lines)
        {
            Line lineObject = LineHandler.Instance().GetLine(line);
            lineObject.RenameStation(key, newKey);
        }
    }

    public string GetRandomNode()
    {
        string[] keys = new string[FGraph.Count];
        FGraph.Keys.CopyTo(keys, 0);

        return keys[UnityEngine.Random.Range(0, FGraph.Count)];
    }

    public void RemoveNode(string key)
    {
        FGraph.Remove(key);

        foreach (Node station in FGraph.Values)
        {
            if (station.Sons.ContainsKey(key))
                station.Sons.Remove(key);
        }

        LineHandler.Instance().RemoveStation(key);
    }

    public bool Exists(string key)
    {
        return FGraph.ContainsKey(key);
    }

    public int GetSize()
    {
        return FGraph.Count;
    }

	//============
	// Generation functions
	//============

	// generate random points
	public static Vector3[] GenerateFromSeed(uint nbStations, int genSeed)
	{
		Vector3[] temp_pos = new Vector3[nbStations];
		Vector3[] stations_pos;
		Vector2 region_orig = new Vector2(-5.0f, -5.0f);
		Vector2 region_dims = new Vector2(10.0f, 10.0f);

		Vector3 rand_pos = new Vector3();
		uint i = 0;
		uint j = 0;
		uint n = 0;
		float tmp_min_sq_dist = 0;

		float min_sq_dist = System.Math.Min(region_dims.x, region_dims.y) / nbStations;
		UnityEngine.Random.seed = genSeed;

		for (i = 0; i < nbStations; i++)
		{
			rand_pos.x = (UnityEngine.Random.value * region_dims.x) + region_orig.x;
			rand_pos.z = (UnityEngine.Random.value * region_dims.y) + region_orig.y;

			for (j = 0; j < i; j++)
			{
				tmp_min_sq_dist = (rand_pos - temp_pos[j]).sqrMagnitude;
				if (tmp_min_sq_dist < min_sq_dist)
					break;
			}

			if (tmp_min_sq_dist >= min_sq_dist)
			{
				temp_pos[n] = rand_pos;
				n++;
			}
		}

		stations_pos = new Vector3[n];
		System.Array.Copy(temp_pos, stations_pos, n);
		return stations_pos;
	}

	// distances and angle helpers
	private static Vector3 GetCenterPoint(Vector3[] points)
	{
		Vector3 center = new Vector3();

		for (uint i = 0; i < points.Length; i++)
			center += points[i];
		center /= points.Length;

		return center;
	}

	private static float[] GetSquaredDistancesToRefPoint(Vector3[] points, Vector3 refPoint)
	{
		float[] sq_dists = new float[points.Length];

		for (uint i = 0; i < points.Length; i++)
			sq_dists[i] = (points[i] - refPoint).sqrMagnitude;

		return sq_dists;
	}

	private static uint GetIndexMaxDist(float[] dists)
	{
		uint index = 0;
		float max_dist = dists[0];

		for (uint i = 1; i < dists.Length; i++)
			if (dists[i] > max_dist)
				index = i;

		return index;
	}

	private static uint GetIndexMinDist(float[] dists)
	{
		uint index = 0;
		float min_dist = dists[0];
		
		for (uint i = 1; i < dists.Length; i++)
			if (dists[i] < min_dist)
				index = i;

		return index;
	}

	private static uint[] GetIndexesInAngleFromRefPointAndDirection(Vector3[] points, Vector3 refPoint, Vector3 direction, float maxAngle)
	{
		uint[] tmp_idxs = new uint[points.Length];
		uint[] indexes;
		uint n = 0;

		for (uint i = 0; i < points.Length; i++)
		{
			if (Vector3.Angle(direction, points[i] - refPoint) <= maxAngle)
			{
				if (points[i] == refPoint)
					continue;
				tmp_idxs[n] = i;
				n++;
			}
		}

		indexes = new uint[n];
		Array.Copy(tmp_idxs, indexes, n);
		return indexes;
	}

	// lines helpers
	private static List<uint> GetLinesIndexesOfPointIndex(uint pointIndex, uint[][] lines, uint linesBuilt)
	{
		List<uint> lines_indexes = new List<uint>();

		for (uint i = 0; i < linesBuilt; i++)
		{
			{
			for (uint j = 0; j < lines[i].Length; j++)
				if (lines[i][j] == pointIndex)
				{
					lines_indexes.Add(i);
					break;
				}
			}
		}

		return lines_indexes;
	}

	private static uint GetStationIndexInLine(uint pointIndex, uint[] line)
	{
		uint station_idx = (uint)line.Length;

		for (uint i = 0; i < line.Length; i++)
		{
			if (line[i] == pointIndex)
			{
				station_idx = i;
				break;
			}
		}

		return station_idx;
	}

	// generate lines
	public static uint[][] GenerateLines(Vector3[] points)
	{
		float max_angle = 45.0f;
		uint[][] tmp_lines = new uint[points.Length][]; // to store the table of lines
		for (uint i = 0; i < points.Length; i++)
			tmp_lines[i] = new uint[points.Length];
		uint n_line = 0;
		List<uint> idxs_isolated_points = new List<uint>(points.Length); // to store the isolated points (candidates for lines beginnings)

		for (uint i = 0; i < points.Length; i++) // all the points are isolated at the beginning
			idxs_isolated_points.Add(i);
		Vector3 center = GetCenterPoint(points);
		float[] sq_dists_center = GetSquaredDistancesToRefPoint(points, center);

		while (idxs_isolated_points.Count != 0) // while there are isolated points
		{
			uint idx = GetIndexMaxDist(sq_dists_center); // getting the farthest point from the center

			// removal of the point from the points to consider
			sq_dists_center[idx] = 0.0f;
			idxs_isolated_points.Remove(idx);

			// setting the first point as the first station of the line
			tmp_lines[n_line][0] = idx;
			uint n_station = 1;
			Vector3 direction = center - points[idx];

			// getting possible points for the next station in the line
			uint[] angle_points_idxs = GetIndexesInAngleFromRefPointAndDirection(points, points[idx], direction, max_angle);

			while (angle_points_idxs.Length != 0) // while there are points ahead of the current one
			{
				// getting the coordinates of the points ahead
				Vector3[] angle_points = new Vector3[angle_points_idxs.Length];
				for (uint i = 0; i < angle_points_idxs.Length; i++)
					angle_points[i] = points[angle_points_idxs[i]];

				// calculating the distances of the points ahead to the current point and getting the closest one
				float[] sq_dists = GetSquaredDistancesToRefPoint(angle_points, points[idx]);
				uint next_idx = GetIndexMinDist(sq_dists);

				// getting the lines of the current point and the closest point
				List<uint> idx_lines = GetLinesIndexesOfPointIndex(idx, tmp_lines, n_line);
				List<uint> next_idx_lines = GetLinesIndexesOfPointIndex(angle_points_idxs[next_idx], tmp_lines, n_line);

				// initializing variables for the loop on possible points
				bool are_same_line = false;
				float sup_max_dist = sq_dists[GetIndexMaxDist(sq_dists)] + 1.0f;
				uint j = 0;

				for (j = 0; j < angle_points.Length; j++) // making sure the current point and the next point are already directly connected
				{
					foreach (uint line_i in idx_lines)
					{
						uint idx_line_pos = GetStationIndexInLine(idx, tmp_lines[line_i]);
						if (next_idx_lines.Contains(line_i))
					    {
							uint next_idx_line_pos = GetStationIndexInLine(angle_points_idxs[next_idx], tmp_lines[line_i]);
							if (idx_line_pos + 1 == next_idx_line_pos
							    || idx_line_pos == next_idx_line_pos + 1)
							{
								are_same_line = true;
								sq_dists[next_idx] = sup_max_dist;
								break;
							}
						}
					}

					// the next point is the right one
					if (!are_same_line)
						break;

					// the next point is the next closest point ahead
					are_same_line = false;
					next_idx = GetIndexMinDist(sq_dists);
					next_idx_lines = GetLinesIndexesOfPointIndex(angle_points_idxs[next_idx], tmp_lines, n_line);
				}

				if (are_same_line || j == angle_points.Length) // the terminus has been reached
					break;

				// the current point is added to the line and removed from the isolated points
				tmp_lines[n_line][n_station] = angle_points_idxs[next_idx];
				n_station++;
				idxs_isolated_points.Remove(angle_points_idxs[next_idx]);

				// re-positionning the angle to select the next station
				direction = points[angle_points_idxs[next_idx]] - points[idx];
				idx = angle_points_idxs[next_idx];
				angle_points_idxs = GetIndexesInAngleFromRefPointAndDirection(points, points[idx], direction, max_angle);
			}

			// resizing the line to its true size and getting on the next line
			uint[] tmp_line = new uint[n_station];
			Array.Copy(tmp_lines[n_line], tmp_line, n_station);
			tmp_lines[n_line] = tmp_line;
			n_line++;
		}

		// resizing the table of lines to its true size and returning the generated lines
		uint[][] lines = new uint[n_line][]; // table of lines of points indexes to be returned
		Array.Copy(tmp_lines, lines, n_line);
		return lines;
	}
}
