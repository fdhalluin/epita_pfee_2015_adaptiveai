﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Text;

[System.Serializable]
public class Node
{
    //============
    // Variables
    //============

    public string name;

    private Dictionary<string, float> FSons;
    private Boolean FState;
    private Vector2 FPosition;
    private List<int> FLines;
    private List<Agent.Vehicle> FVehicles;
    private List<Agent.Traveler> FTravelers;

    private Boolean FHasInfos;
    private Dictionary<KeyValuePair<string, string>, float> FInfos;

    private GameObject FStation;

    #region Getter / Setter
    //============
    // Se/Getter
    //============

    public Dictionary<string, float> Sons
    {
        get { return FSons; }
    }

    public GameObject Station
    {
        get { return FStation; }
    }

    public Boolean State
    {
        get { return FState; }
    }

    public Vector2 Position
    {
        get { return FPosition; }
    }

    public List<int> Lines
    {
        get { return FLines; }
    }

    public List<Agent.Vehicle> Vehicles
    {
        get { return FVehicles; }
    }

    public List<Agent.Traveler> Travelers
    {
        get { return FTravelers; }
    }

    public Boolean HasInfos
    {
        get { return FHasInfos; }
    }

    public Dictionary<KeyValuePair<string, string>, float> Infos
    {
        get { return FInfos; }
    }
    #endregion

    //============
    // Des/Constructor
    //============

    public Node(string name, GameObject station)
    {
        this.name = name;
        FSons = new Dictionary<string, float>();
        FState = true;

        FStation = station;
        FPosition = new Vector2(station.transform.position.x, station.transform.position.z);

        FLines = new List<int>();
        FVehicles = new List<Agent.Vehicle>();
        FTravelers = new List<Agent.Traveler>();
        FHasInfos = true;
        FInfos = new Dictionary<KeyValuePair<string, string>, float>(); 
    }

    public Node(string name, GameObject station, bool hasInfos)
    {
        this.name = name;
        FSons = new Dictionary<string, float>();
        FState = true;

        FStation = station;
        FPosition = new Vector2(station.transform.position.x, station.transform.position.z);

        FLines = new List<int>();
        FVehicles = new List<Agent.Vehicle>();
        FTravelers = new List<Agent.Traveler>();
        FHasInfos = hasInfos;
        FInfos = new Dictionary<KeyValuePair<string, string>, float>();
    }

    //============
    // Other methods
    //============

    public void AddSon(string node)
    {
        if (Graph.Instance().Exists(node) && name != node)
        {
            FSons.Add(node, 1.0f);
        }
    }

    public void RemoveSon(string node)
    {
        if (FSons.ContainsKey(node))
        {
            FSons.Remove(node);
        }
    }

    public string RandomSon()
    {
        string[] keys = new string[FSons.Count];
        FSons.Keys.CopyTo(keys, 0);

        return keys[UnityEngine.Random.Range(0, FSons.Count)];
    }

    public void AddLine(int line)
    {
        if (!FLines.Contains(line))
            FLines.Add(line);
    }

    public void RemoveLine(int line)
    {
        if (FLines.Contains(line))
            FLines.Remove(line);
    }

    public void AddVehicle(Agent.Vehicle vehicle)
    {
        if (!FVehicles.Contains(vehicle))
            FVehicles.Add(vehicle);
    }

    public void RemoveVehicle(Agent.Vehicle vehicle)
    {
        if (FVehicles.Contains(vehicle))
            FVehicles.Remove(vehicle);
    }

    public void AddTraveler(Agent.Traveler traveler)
    {
        if (!FTravelers.Contains(traveler))
            FTravelers.Add(traveler);
    }

    public void RemoveTraveler(Agent.Traveler traveler)
    {
        if (FTravelers.Contains(traveler))
            FTravelers.Remove(traveler);
    }

    public void AddInfo(KeyValuePair<string, string> stations, float value)
    {
        FInfos[stations] = value;
    }

    public void RemoveInfo(KeyValuePair<string, string> stations)
    {
        if (FInfos.ContainsKey(stations))
            FInfos.Remove(stations);
    }
}

public class Station : MonoBehaviour
{
    public Node node;

    public static int count = 0;

    private TextInfo FTextInfo;

    private float FLastUpdate;

    void Awake()
    {
        count += 1;
        node = Graph.Instance().AddNode("station" + count, gameObject);
        FLastUpdate = Time.realtimeSinceStartup;
    }

    void Start()
    {
        // TextInfo
        FTextInfo = Instantiate(GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerController>().textInfo);
        FTextInfo.target = transform;
        FTextInfo.offset = new Vector3(-1, 0, 1.8f);
        FTextInfo.text = "";
    }

    void Update()
    {
        if (this.tag == "Select")
        {
            FTextInfo.text = node.name + "\n" + node.Travelers.Count + " travelers\nHas infos : ";
            if (node.HasInfos)
                FTextInfo.text += "yes";
            else if (FTextInfo.gameObject != null)
                FTextInfo.text += "no";
            this.GetComponent<MeshRenderer>().material.color = Color.green;
        }
        else
        {
            if (FTextInfo.gameObject != null)
                FTextInfo.text = "";
            this.GetComponent<MeshRenderer>().material.color = Color.gray;
        }

        if (node.HasInfos)
        {
            if (UnityEngine.Random.Range(0, 10000) == 9999)
            {
                string son = node.RandomSon();
                node.AddInfo(new KeyValuePair<string, string>(node.name, son), 100.0f);
                node.Sons[son] = 0.1f;
            }

            List<KeyValuePair<string, string>> keys = new List<KeyValuePair<string, string>>(node.Infos.Keys);

            foreach (KeyValuePair<string, string> path in keys)
            {
                float remaningTime = node.Infos[path] - (Time.realtimeSinceStartup - FLastUpdate);
                if (remaningTime <= 0.0f)
                {
                    node.RemoveInfo(path);
                    if (path.Key == node.name)
                        node.Sons[path.Value] = 1.0f;
                }
                else
                    node.Infos[path] = remaningTime;
            }

            if (UnityEngine.Random.Range(0, 100) == 99)
            {
                foreach (string sonName in node.Sons.Keys)
                {
                    Node son = Graph.Instance().GetNode(sonName);
                    foreach (KeyValuePair<KeyValuePair<string, string>, float> info in node.Infos)
                        son.AddInfo(info.Key, info.Value);
                }
            }
        }
    }

    void OnDestroy()
    {
        if (FTextInfo != null)
            Destroy(FTextInfo.gameObject);
        Graph.Instance().RemoveNode(node.name);
        node = null;
    }
}
