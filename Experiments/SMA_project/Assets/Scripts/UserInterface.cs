﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;

public class UserInterface : MonoBehaviour
{
    public ActionType action;
    public Line line;

    public enum ActionType
    {
        None = 0x0,
        Select,
        Add,
        Delete
    }

    void OnGUI()
    {
        if (LineHandler.Instance().GetSize() == 0)
            return;

        foreach (int key in LineHandler.Instance().GetLineList())
        {
            Line actualLine = LineHandler.Instance().GetLine(key);

            GUI.contentColor = actualLine.color;
            GUILayout.BeginHorizontal();

            if (GUILayout.Button(actualLine.transform.name))
            {
                action = ActionType.Select;
                line = actualLine;
            }

            if (GUILayout.Button("+"))
            {
                action = ActionType.Add;
                line = actualLine;
            }

            if (GUILayout.Button("-"))
            {
                action = ActionType.Delete;
                line = actualLine;
            }

            GUILayout.EndHorizontal();
        }

        if (action != ActionType.None)
            ManipulateChosenLine();
    }

    public void ManipulateChosenLine()
    {
        if (action == ActionType.Add)
        {
            line.AddVehicle();
        }
        else if (action == ActionType.Delete)
        {
            line.DeleteVehicle();
        }
        else if (action == ActionType.Select)
        {

        }

        action = ActionType.None;
    }

    void Awake()
    {
        action = ActionType.None;
        line = null;
    }
}
