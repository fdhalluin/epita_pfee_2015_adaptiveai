﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System.Collections;

public class Metro : MonoBehaviour
{
    public Agent.Vehicle vehicle;

    private TextInfo FTextInfo;

    void Start()
    {
        // TextInfo
        FTextInfo = Instantiate(GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerController>().textInfo);
        FTextInfo.target = transform;
        FTextInfo.offset = new Vector3(-1, 0, 0.8f);
        FTextInfo.text = "";
    }
	
	void Update ()
    {
        UpdatePosition();

        if (vehicle.State == Agent.VehicleState.Travelling)
            FTextInfo.text = vehicle.Travelers.Count + " travelers";
        else
            FTextInfo.text = "";
    }

    public void UpdatePosition()
    {
        if (vehicle.State == Agent.VehicleState.Travelling)
        {
            float angle = Vector2.Angle(new Vector2(0, 1), vehicle.DirectionVector2);
            if (vehicle.DirectionVector2.x < 0)
                angle = 180 - angle;

            Vector2 origine = Graph.Instance().GetNode(vehicle.Origine).Position;
            Vector3 new_pos = new Vector3(origine.x + vehicle.DirectionVector2.x * vehicle.Progression, 0, origine.y + vehicle.DirectionVector2.y * vehicle.Progression);

            transform.position = new_pos;
            transform.eulerAngles = new Vector3(0, angle, 0);
        }
    }

    public void OnDestroy()
    {
        if (FTextInfo != null)
            Destroy(FTextInfo.gameObject);
    }
}
