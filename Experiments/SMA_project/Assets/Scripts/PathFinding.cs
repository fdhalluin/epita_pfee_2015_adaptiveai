﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;

public class PathFinding
{
    // TODO: missing waiting time

    static public List<string> pathFinding(Node start, Node goal, Dictionary<KeyValuePair<string, string>, float> informations)
    {
        List<string> closedList = new List<string>();
        List<string> openList = new List<string>();

        Dictionary<string, double> gScore = new Dictionary<string, double>();
        Dictionary<string, double> fScore = new Dictionary<string, double>();

        Dictionary<string, string> origin = new Dictionary<string, string>();

        openList.Add(start.name);
        gScore.Add(start.name, 0);
        fScore.Add(start.name, manhattanDistance(start, goal));

        while (openList.Count != 0)
        {
            Node actual = Graph.Instance().GetNode(popLowest(openList, fScore));
            if (actual.name.Equals(goal.name))
                return reconstructPath(origin, actual.name);
            
            closedList.Add(actual.name);

            foreach (string node in actual.Sons.Keys)
            {
                if (closedList.Contains(node))
                    continue;

                double actualScore;
                if (informations.ContainsKey(new KeyValuePair<string, string>(actual.name, node)))
                {
                    actualScore = gScore[actual.name] * (-1) * (-1 + Math.Log(actual.Sons[node]));
                }
                else
                    actualScore = gScore[actual.name];

                if (!openList.Contains(actual.name) || gScore[actual.name] > actualScore)
                {
                    origin[node] = actual.name;
                    gScore[node] = actualScore;
                    fScore[node] = gScore[node] + manhattanDistance(Graph.Instance().GetNode(node), goal);

                    if (!openList.Contains(node))
                        openList.Add(node);
                }
            }
        }

        return null;
    }

    static private string popLowest(List<string> openList, Dictionary<string, double> score)
    {
        string result = null;
        double valueLowest = double.MaxValue;

        foreach (string node in openList)
        {
            if (score.ContainsKey(node) && valueLowest > score[node])
            {
                result = node;
                valueLowest = score[node];
            }
        }

        openList.Remove(result);
        return result;
    }

    static private float manhattanDistance(Node start, Node goal)
    {
        return Math.Abs(goal.Position.x - start.Position.x)
            + Math.Abs(goal.Position.y - start.Position.y);
    }

    static private List<string> reconstructPath(Dictionary<string, string> origin, string actual)
    {
        List<string> path = new List<string>();

        path.Add(actual);

        while (origin.ContainsKey(actual))
        {
            actual = origin[actual];
            path.Insert(0, actual);
        }

        return path;
    }
}
