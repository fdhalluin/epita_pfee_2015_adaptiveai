﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;

public class KnnElementComparer : IComparer<double[]>
{
	private int index_compared_dim;

	public KnnElementComparer(int par_index_compared_dim)
	{
		if (par_index_compared_dim >= 0)
			index_compared_dim = par_index_compared_dim;
		else
			index_compared_dim = 0;
	}

	public int Compare(double[] first_elem, double[] other_elem)
	{
		if (first_elem != null && index_compared_dim < first_elem.Length)
		{
			if (other_elem != null && index_compared_dim < other_elem.Length)
			{
				if (first_elem[index_compared_dim] > other_elem[index_compared_dim])
					return 1;
				if (first_elem[index_compared_dim] < other_elem[index_compared_dim])
					return -1;
			}

			return 0;
		}

		return 0;
	}
}

public class KnnScript : MonoBehaviour
{
	private string[][] data = null;
	public int train_elems_count = 0;

	private int[][] sorted_indexes = null;
	private double[][] train_elements = null;
	private double[][] test_elements = null;

	void Start ()
	{
		ReadFile("./Assets/Datas/abalone.txt");

		//-----test sur les fonctions matricielle-----
		/*double[][] test = new double[][] {new double[]{1,2,3}, new double[]{4,5,6}};
		double[][] transpose = Transpose (test);
		double[][] product = MatriceProduct (test, transpose);
		for (int i = 0; i < 2; ++i) {
			for (int j =0; j < 3; ++j) {
				Debug.Log ("mat[" + i + "][" + j + "]=" + test [i] [j]);
				Debug.Log ("transpose[" + j + "][" + i + "]=" + transpose [j] [i]);
			}
			for (int j = 0; j < 2; ++j)
				Debug.Log ("product" + i + "][" + j + "]=" + product [i] [j]);
		}*/

		// -----test sur le knn---------
		Debug.Log("Hey, what's going on ?");
		Debug.Log("performance for Knn with k=5 is " + KNearestNeighbour(5));

		// ----test sur GetClasses-----------
		/*Debug.Log ("data count = " + data.Count ());
		for (int i = 0; i < 5; ++i) {
			Debug.Log ("data classes = " + getClasses(data)[i]);
		}*/
	}

	private string[][] InitStrMatrix(int rows, int columns)
	{
		string[][] matrix = new string[rows][];

		for (int i = 0; i < rows; ++i)
		{
			matrix[i] = new string[columns];
		}

		return matrix;
	}

	private double[][] InitDoubleMatrix(int rows, int columns)
	{
		double[][] matrix = new double[rows][];

		for (int i = 0; i < rows; ++i)
		{
			matrix[i] = new double[columns];
		}

		return matrix;
	}

	private void ReadFile(string filename)
	{
		var sr = File.OpenText(filename);

		List<List<string>> tmp = sr.ReadToEnd().Split('\n').Select(s=>s.Split(',').ToList()).ToList();
		tmp.RemoveAt(tmp.Count - 1); //remove the last one (empty string)
		data = InitStrMatrix(tmp.Count, tmp.ElementAt(0).Count);
		data = tmp.Select(a => a.ToArray ()).ToArray();

		sr.Close();
	}

	private double[][] ConvertDataToElements(int lower_index, int upper_index)
	{
		if (lower_index < 0
		    || upper_index < 0
		    || lower_index > upper_index
		    || data == null
		    || upper_index > data.Count())
			return null;

		double[][] result_elements = new double[upper_index - lower_index + 1][];
		int nb_dims = data[0].Count() - 2;

		for (int i = lower_index; i <= upper_index; i++)
		{
			result_elements[i - lower_index] = new double[nb_dims];

			for (int j = 1; j <= nb_dims; j++)
				result_elements[i - lower_index][j - 1] = Convert.ToDouble(data[i][j]);
		}

		return result_elements;
	}

	private void PrintData(List<List<string>> data)
	{
		for (int i = 0; i < data.Count; ++i)
		{
			for (int j = 0; j < data[i].Count; ++j)
				Debug.Log ("Data[" + i + "][" + j + "]=" + data[i][j]);		
		}
	}

	private int BinarySearchIndex(int[] par_sorted_index, int par_dim_index, double par_value)
	{
		if (par_sorted_index == null
		    || par_dim_index < 0)
			return -1;

		int lower_bound = 0;
		int upper_bound = par_sorted_index.GetLength(0) - 1;
		int result_index = 0;

		while (lower_bound <= upper_bound)
		{
			result_index = lower_bound + (upper_bound - lower_bound) / 2;
			double tmp_value = train_elements[result_index][par_dim_index];

			if (tmp_value < par_value)
				upper_bound = result_index - 1;
			else if (tmp_value > par_value)
				lower_bound = result_index + 1;
			else
				break;
		}

		return result_index;
	}

	private int[][] SortIndexesByDimension()
	{
		if (train_elements == null || train_elements[0] == null)
			return null;

		int nb_elems = train_elements.GetLength(0);
		int nb_dims = train_elements[0].GetLength(0);
		int[][] sorted_indexes = new int[nb_dims][];

		for (int i = 0; i < nb_dims; i++)
		{
			sorted_indexes[i] = new int[nb_elems];
			for (int j = 0; j < nb_elems; j++)
				sorted_indexes[i][j] = j;

			double[][] tmp_elements = new double[nb_elems][];
			train_elements.CopyTo(tmp_elements, nb_elems);
			KnnElementComparer comparer = new KnnElementComparer(i);
			Array.Sort(tmp_elements, sorted_indexes[i], comparer);
		}

		return sorted_indexes;
	}

	private List<int> GetElementsInterval(int[] par_sorted_index, int par_dim_index, double par_value, double par_width)
	{
		int tmp_left = BinarySearchIndex(par_sorted_index, par_dim_index, par_value - par_width);
		int tmp_right = BinarySearchIndex(par_sorted_index, par_dim_index, par_value + par_width);

		int lower_idx = Array.IndexOf(par_sorted_index, tmp_left);
		int upper_idx = Array.IndexOf(par_sorted_index, tmp_right);

		int[] tmp_result = new int[upper_idx - lower_idx + 1];
		Array.Copy(par_sorted_index, lower_idx, tmp_result, 0, upper_idx - lower_idx + 1);

		return tmp_result.ToList();
	}

	private List<int> GetNClosestElements(double[] par_ref_elem, int par_nb_elems)
	{
		if (train_elements == null
		    || par_ref_elem == null
		    || par_nb_elems <= 0)
			return null;

		int nb_elems = train_elements.Count();
		int nb_dims = train_elements[0].Count();

		if (sorted_indexes == null)
			sorted_indexes = SortIndexesByDimension();

		double width = 1.0f;
		List<int> closest_elems_indexes = GetElementsInterval(sorted_indexes[0], 0, par_ref_elem[0], width);

		while (true)
		{
			for (int i = 1; i < nb_dims; i++)
			{
				List<int> tmp_elems_indexes = GetElementsInterval(sorted_indexes[i], i, par_ref_elem[i], width);
				closest_elems_indexes = closest_elems_indexes.Intersect(tmp_elems_indexes).ToList();
			}

			if (closest_elems_indexes.Count() >= par_nb_elems
			    || closest_elems_indexes.Count() >= nb_elems)
				break;

			width *= 2;
			closest_elems_indexes = GetElementsInterval(sorted_indexes[0], 0, par_ref_elem[0], width);
		}

		return closest_elems_indexes;
	}

	// distance Euclidienne sur les 7 dimensions numériques (pas le genre)
	private double SquaredDistance(double[] par_first_elem, double[] par_other_elem)
	{
		if (par_first_elem == null
			|| par_other_elem == null
		    || par_first_elem.Count() != par_other_elem.Count())
			return 0;

		int nb_dims = par_first_elem.Count();
		double distance = 0;

		for (int i = 1; i < nb_dims; i++)
			distance += Math.Pow(par_first_elem[i] - par_other_elem[i], 2);

		return distance;
	}

	//renvoie la liste des classes
	private string[] getClasses(string[][] par_data_set)
	{
		if (par_data_set == null || par_data_set[0] == null)
			return null;
		string[] data_classes = new string[par_data_set.Count()];
		for (int i = 0; i < par_data_set.Count(); ++i)
			data_classes [i] = par_data_set [i] [8];
		return data_classes;
	}

	//renvoie les données centré-réduites
	private double[][] NormalizeElements(double[][] toNorm)
	{
		int nbElement = toNorm.Count();
		int dim = toNorm[0].Count();
		double[] averages = new double[dim];

		for (int i = 0; i < nbElement; ++i)
		{
			for (int j = 0; j < dim; ++j)
				averages[j] += toNorm[i][j]/nbElement;
		}

		double[] standardDeviation = new double[dim];

		for (int j = 0; j < dim; ++j)
		{
			for (int i = 0; i < nbElement; ++i)
			{
				toNorm[i][j] -= averages[j];
				standardDeviation[j] += Math.Pow(toNorm[i][j], 2);
			}

			//Debug.Log("sum ^2 ["+j+"] = " + standardDeviation[j]);
			standardDeviation[j] = Math.Sqrt (standardDeviation[j] / nbElement);
			//Debug.Log("sigma["+j+"] = " + standardDeviation[j]);

			for (int i = 0; i < nbElement; ++i)
			{
				toNorm[i][j] /= standardDeviation[j];
			}
		}

		return toNorm;
	}

	//Calcul le taux de réussite du classifieur
	private double Performance(string[] par_classification, string[] par_labels)
	{
		if (par_classification == null || par_labels == null)
			return 0;

		double perf = 0;

		for (int i = 0; i < par_classification.Count(); ++i)
		{
			if (par_classification[i] == par_labels[i])
				perf += 1;
		}
		perf /= par_classification.Count();

		return perf;
	}

	private double[][] Transpose(double[][] par_matrix)
	{
		int rows = par_matrix.Count();
		int columns = par_matrix [0].Count ();
		double[][] transposed = InitDoubleMatrix (columns, rows);

		for (int  i = 0; i < rows; ++i)
		{
			for (int j = 0; j < columns; ++j)
			{
				transposed[j][i] = par_matrix[i][j];
			}
		}

		return transposed;
	}

	private double[][] MatriceProduct(double[][] par_mat, double[][] par_mat_2)
	{
		int rows = par_mat.Count();
		int columns = par_mat_2[0].Count();
		int cross_dim = par_mat [0].Count();

		if (cross_dim != par_mat_2.Count()) {
			return null;
		}

		double[][] product = InitDoubleMatrix (rows, columns);
		for (int i = 0; i < rows; ++i)
		{
			for(int j = 0; j < columns; ++j)
			{
				double sum = 0;
				for (int k = 0; k < cross_dim; ++k)
				{
					sum += par_mat[i][k] * par_mat_2[k][j]; 
				}
				product[i][j] = sum;
			}
		}
		return product;
	}

	//TODO
	public double[][] PrincipalComposantAnalysis(double[][] par_norm_data)
	{
		//calculer la matrice de correlation
		//diagonaliser et trouver les 3 plus grandes vp
		//garder les trois dimensions correspondantes

		//double[][] correlation = MatriceProduct(Transpose (par_norm_data), par_norm_data);
		return par_norm_data;
	}

	//TODO
	public double KNearestNeighbour(int k)
	{
		double[][] orig_elements = ConvertDataToElements(0, data.Count() - 1);
		double[][] normalized_elements = NormalizeElements(orig_elements);

		double[][] train_elements = InitDoubleMatrix(train_elems_count, 7); // hardcoded
		double[][] test_elements = InitDoubleMatrix(4177-train_elems_count, 7);

		Array.Copy(normalized_elements, 0, train_elements, 0, train_elems_count);
		Array.Copy(normalized_elements, train_elems_count, test_elements, 0, data.Count() - train_elems_count);

		string[] elements_classes = new string[test_elements.Count()];
		string[] elements_labels = new string[test_elements.Count()];
		
		for (int i = 0; i < test_elements.Count(); i++)
		{
			List<int> closest_elements_indexes = GetNClosestElements(test_elements[i], k);
			List<double> closest_elements_distances = new List<double>(closest_elements_indexes.Count());

			if (closest_elements_indexes != null)
			{
				if (closest_elements_distances != null)
				{
					foreach (int index in closest_elements_indexes)
						closest_elements_distances.Add( SquaredDistance(train_elements [index], test_elements [index]) );

					int[] tmp_indexes = closest_elements_indexes.ToArray();
					double[] tmp_distances = closest_elements_distances.ToArray();
					Array.Sort(tmp_distances, tmp_indexes);

					Dictionary<string, int> classes_occs = new Dictionary<string, int>();

					for (int j = 0; j < k; j++)
					{
						string tmp_elem_class = data [tmp_indexes [j]].Last();
						if (classes_occs.ContainsKey(tmp_elem_class))
							classes_occs[tmp_elem_class]++;
						else
							classes_occs[tmp_elem_class] = 1;
					}

					string class_max_ocss = classes_occs.Aggregate((l, r) => l.Value > r.Value ? l : r).Key;
					elements_classes [i] = class_max_ocss;
				}
			}
		}

		//normalise data
		//call rectangle fonction to get some points (neightbors)
		//for neightbor in neightbors
			//for element in train
				//euclidian(neightbor, element)

		double precision = Performance(elements_classes, elements_labels); //= Performance (classified, labels);
		return precision;
	}
	
}
