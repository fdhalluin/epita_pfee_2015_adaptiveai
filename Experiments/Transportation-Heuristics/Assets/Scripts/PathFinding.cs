﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;

public class PathFinding
{
    static public List<Node> pathFinding(Node start, Node goal)
    {
        List<Node> closedList = new List<Node>();
        List<Node> openList = new List<Node>();

        Dictionary<Node, double> gScore = new Dictionary<Node, double>();
        Dictionary<Node, double> fScore = new Dictionary<Node, double>();

        Dictionary<Node, Node> origin = new Dictionary<Node, Node>();

        openList.Add(start);
        gScore.Add(start, 0);
        fScore.Add(start, manhattanDistance(start, goal));

        while (openList.Count != 0)
        {
            Node actual = popLowest(openList, fScore);
            if (actual.UnityNode.name.Equals(goal.UnityNode.name))
                return reconstructPath(origin, actual);
            
            closedList.Add(actual);

            foreach (Node node in actual.Sons.Keys)
            {
                if (closedList.Contains(node))
                    continue;

                double actualScore = gScore[actual];

                if (!openList.Contains(actual) || gScore[actual] > actualScore)
                {
                    origin[node] = actual;
                    gScore[node] = actualScore;
                    fScore[node] = gScore[node] + manhattanDistance(node, goal);

                    if (!openList.Contains(node))
                        openList.Add(node);
                }
            }
        }

        return null;
    }

    static private Node popLowest(List<Node> openList, Dictionary<Node, double> score)
    {
        Node result = null;
        double valueLowest = double.MaxValue;

        foreach (Node node in openList)
        {
            if (score.ContainsKey(node) && valueLowest > score[node])
            {
                result = node;
                valueLowest = score[node];
            }
        }

        openList.Remove(result);
        return result;
    }

    static private float manhattanDistance(Node start, Node goal)
    {
        return Math.Abs(goal.Position.x - start.Position.x)
            + Math.Abs(goal.Position.y - start.Position.y);
    }

    static private List<Node> reconstructPath(Dictionary<Node, Node> origin, Node actual)
    {
        List<Node> path = new List<Node>();

        path.Add(actual);

        while (origin.ContainsKey(actual))
        {
            actual = origin[actual];
            path.Insert(0, actual);
        }

        return path;
    }
}
