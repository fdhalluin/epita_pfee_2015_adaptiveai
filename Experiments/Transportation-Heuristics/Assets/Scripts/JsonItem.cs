﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

public class JsonItem
{
	static public Dictionary<string, Item> Init(string filename)
	{
		Dictionary<string, Item> items = new Dictionary<string, Item>();
		using (FileStream fs = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
		using (BufferedStream bs = new BufferedStream(fs))
		using (StreamReader sr = new StreamReader(bs))
		{
            string line;
            List<List<float>> coords = new List<List<float>>();
            List<float> geo = new List<float>();
            string name = string.Empty;
            int statut = 0;
            while ((line = sr.ReadLine()) != null)
			{
				//Debug.Log(line);
				//Debug.Log(line.Split(new Char[] { ':' })[0]);
				if (line.Split(new Char[] { ':' })[0].Contains("geo_shape"))
				{
					sr.ReadLine();
					sr.ReadLine();
					line = sr.ReadLine();
					//Debug.Log(Regex.Replace(line, @"[\[\]']+", "").Trim(' '));
					string[] s = Regex.Replace(line, @"[\[\]']+", "").Trim(' ').Split(':')[1].Split(',');
					for (int i = 0; i < s.Length / 2; i += 2)
					{
						List<float> tmp = new List<float>();
						//Debug.Log(s[i]);
                        tmp.Add(Single.Parse(s[i]));
                        tmp.Add(Single.Parse(s[i + 1]));
						coords.Add(tmp);
					}
				}
				else if (line.Split(':')[0].Contains("nom"))
					name = line.Replace("\"", "").Split(':')[1].Trim(new Char[] { ' ', ',' });
				else if (line.Split(':')[0].Contains("geo_point_2d"))
				{
					string[] s = Regex.Replace(line.Split(':')[1], @"[\[\]']+", "").Split(',');
					geo.Add(Single.Parse(s[0]));
                    geo.Add(Single.Parse(s[1]));
				}
				else if (line.Split(':')[0].Contains("statut"))
					statut = Convert.ToInt32(line.Split(':')[1]);
                else if (line.Contains("record_timestamp"))
                {
                    items.Add(name, new Item(name, coords, geo, statut));
                    name = string.Empty;
                    statut = 0;
                    geo.Clear();
                    coords.Clear();
                }
			}
		}

        return items;
	}
}

public class Item
{
	private string FName;
	private List<List<float>> FCoordinates;
    private int FStatus;
    private List<float> FGeoPoint2D;

	public Item(string name, List<List<float>> coordinates, List<float> geo_point_2d, int statut)
	{
		FName = name;
        FCoordinates = new List<List<float>>();
        FGeoPoint2D = new List<float>();
        FStatus = statut;

        foreach (float value in geo_point_2d)
        {
            FGeoPoint2D.Add(value);
        }

        foreach (List<float> values in coordinates)
        {
            List<float> tmp = new List<float>();
            foreach (float value in values)
            {
                tmp.Add(value);
            }
            FCoordinates.Add(tmp);
        }
    }


    public string Name
    {
        get { return FName; }
    }

    public List<List<float>> Coordinates
    {
        get { return FCoordinates; }
    }

    public List<float> Position
    {
        get { return FGeoPoint2D; }
    }

    public int Status
    {
        get { return FStatus; }
    }
}
