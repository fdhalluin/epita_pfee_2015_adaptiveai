﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;

public class Graph
{
    //============
    // Singleton
    //============

    private static Graph FInstance;

    static public Graph Instance()
    {
        if (FInstance == null)
            FInstance = new Graph();
        return FInstance;
    }

    //============
    // Variables
    //============

    private Dictionary<string, Node> FGraph;

    //============
    // Des/Constructor
    //============

    private Graph()
    {
        FGraph = new Dictionary<string, Node>();
    }

    //============
    // Other methods
    //============

    public Node AddNode(GameObject node)
    {
        Node tmp = new Node(node);
        FGraph.Add(tmp.UnityNode.name, tmp);

        return tmp;
    }

    public Node GetNode(string key)
    {
        return FGraph[key];
    }

    public void RenameNode(string key, string newKey)
    {
        Node node = FGraph[key];
        FGraph.Remove(key);
        FGraph.Add(newKey, node);
        node.UnityNode.name = newKey;
    }

    public string GetRandomNode()
    {
        string[] keys = new string[FGraph.Count];
        FGraph.Keys.CopyTo(keys, 0);

        return keys[UnityEngine.Random.Range(0, FGraph.Count)];
    }

    public void RemoveNode(Node key)
    {
        FGraph.Remove(key.UnityNode.name);

        foreach (Node station in FGraph.Values)
        {
            if (station.Sons.ContainsKey(key))
                station.Sons.Remove(key);
        }
    }

    public bool Exists(string key)
    {
        return FGraph.ContainsKey(key);
    }

    public int GetSize()
    {
        return FGraph.Count;
    }
}
