﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;

public class PlayerController : MonoBehaviour
{
    // Movement
    public float speed;

    // Zoom
    public float minZoom;
    public float maxZoom;
    public float sensitivity;

    public UnityNode node;

    void Start()
    {
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Backspace))
            Destroy(GameObject.FindWithTag("Select"));
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameObject[] selects = GameObject.FindGameObjectsWithTag("Select");
            foreach (GameObject select in selects)
                select.tag = "Node";
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            Dictionary<string, Item> items = JsonItem.Init("ile_de_france.json");
            Debug.Log(items.Count);
            
            float moyX = 0;
            float moyY = 0;
            foreach (Item item in items.Values)
            {
                moyX += item.Position[0];
                moyY += item.Position[1];
            }
            moyX /= items.Count;
            moyY /= items.Count;

            foreach (Item item in items.Values)
            {
                Vector3 position = new Vector3((item.Position[0] - moyX) * 10, 0, (item.Position[1] - moyY) * 10);
                var clone = Instantiate(node,
                                        position,
                                        new Quaternion()) as UnityNode;
                clone.name = item.Name;
                clone.Initiate();
            }

            GameObject[] nodes = GameObject.FindGameObjectsWithTag("Node");

            foreach (GameObject node in nodes)
            {
                foreach (List<float> values in items[node.GetComponent<UnityNode>().name].Coordinates)
                {
                    foreach (GameObject potentialSon in nodes)
                    {
                        if (potentialSon.GetComponent<CapsuleCollider>().bounds.Contains(new Vector3((values[0] - moyX) * 10, 0, (values[1] - moyY) * 10)))
                        {
                            Debug.Log("HERE");
                            node.GetComponent<UnityNode>().node.AddSon(Graph.Instance().GetNode(potentialSon.GetComponent<UnityNode>().name));
                        }
                    }
                }
            }
        }

        #region MouseLeftClick Handling
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            GameObject[] selects = GameObject.FindGameObjectsWithTag("Select");
            if (Input.GetKey(KeyCode.LeftShift))
            {
                if (Physics.Raycast(ray, out hit, 100.0f))
                {
                    if (hit.collider.tag == "Node")
                        hit.collider.tag = "Select";
                }
            }
            else if (Input.GetKey(KeyCode.LeftControl))
            {
                if (Physics.Raycast(ray, out hit, 100.0f))
                {
                    Node actualNode = hit.collider.GetComponent<UnityNode>().node;
                    foreach (GameObject select in selects)
                    {
                        select.GetComponent<UnityNode>().node.AddSon(actualNode);
                    }
                }
            }
            else if (Input.GetKey(KeyCode.LeftAlt))
            {
                if (selects.Length == 1)
                {
                    if (Physics.Raycast(ray, out hit, 100.0f))
                    {
                        Node actualNode = selects[0].GetComponent<UnityNode>().node;
                        foreach (GameObject select in selects)
                            select.tag = "Node";
                        if (hit.collider.tag == "Node")
                        {
                            foreach (Node node in PathFinding.pathFinding(actualNode, hit.collider.GetComponent<UnityNode>().node))
                            {
                                node.UnityNode.tag = "Select";
                            }
                        }
                    }
                }
            }
            else
            {
                if (Physics.Raycast(ray, out hit, 100.0f))
                {
                    if (selects.Length != 0)
                        foreach (GameObject select in selects)
                            select.tag = "Node";
                    if (hit.collider.tag == "Node")
                        hit.collider.tag = "Select";
                    else if (hit.collider.tag == "Ground")
                    {
                        Instantiate(node,
                                    ray.GetPoint(hit.distance),
                                    new Quaternion());
                    }
                }
            }
        }
        #endregion
    }

    void FixedUpdate()
    {
        float zoom = GetComponent<Rigidbody>().position.y;
        zoom -= Input.GetAxis("Mouse ScrollWheel") * sensitivity;

        Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        GetComponent<Rigidbody>().velocity = movement * speed;

        GetComponent<Rigidbody>().position = new Vector3(GetComponent<Rigidbody>().position.x, Mathf.Clamp(zoom, minZoom, maxZoom), GetComponent<Rigidbody>().position.z);
    }
}
