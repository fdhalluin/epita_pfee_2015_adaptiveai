﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Text;

[System.Serializable]
public class Node
{
    //============
    // Variables
    //============

    private Dictionary<Node, float> FSons;
    private Vector2 FPosition;

    private GameObject FUnityNode;

    #region Getter / Setter
    //============
    // Se/Getter
    //============

    public Dictionary<Node, float> Sons
    {
        get { return FSons; }
    }

    public GameObject UnityNode
    {
        get { return FUnityNode; }
    }

    public Vector2 Position
    {
        get { return FPosition; }
    }
    #endregion

    //============
    // Des/Constructor
    //============

    public Node(GameObject unityNode)
    {
        FSons = new Dictionary<Node, float>();

        FUnityNode = unityNode;
        FPosition = new Vector2(unityNode.transform.position.x, unityNode.transform.position.z);
    }

    //============
    // Other methods
    //============

    public void AddSon(Node node)
    {
        if (Graph.Instance().Exists(node.UnityNode.name) && FUnityNode.name != node.UnityNode.name)
        {
            FSons.Add(node, 1.0f);
        }
    }

    public void RemoveSon(Node node)
    {
        if (FSons.ContainsKey(node))
        {
            FSons.Remove(node);
        }
    }

    public Node RandomSon()
    {
        Node[] keys = new Node[FSons.Count];
        FSons.Keys.CopyTo(keys, 0);

        return keys[UnityEngine.Random.Range(0, FSons.Count)];
    }
}

public class UnityNode : MonoBehaviour
{
    public Node node;

    public void Initiate()
    {
        node = Graph.Instance().AddNode(this.gameObject);
    }

    void Update()
    {
        if (this.tag == "Select")
        {
            this.GetComponent<MeshRenderer>().material.color = Color.green;
        }
        else
        {
            this.GetComponent<MeshRenderer>().material.color = Color.gray;
        }

    }

    void OnDestroy()
    {
        Graph.Instance().RemoveNode(node);
        node = null;
    }
}
