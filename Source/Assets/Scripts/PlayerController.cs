﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;

public enum Mode
{
    Basic,
    Moisture,
    Height,
    Damages,
    Injured
}

public enum MedicalIA
{
	Basic,
	Adaptative
}

public enum EngineerIA
{
	Basic,
	Adaptative
}

public class PlayerController : MonoBehaviour
{
    public int seed;
	public int eventSeed;

    // prefabs
    // ===================
    public GameObject tile;
    public GameObject city;
    public GameObject suburb;
    public GameObject medicalTeam;
    public GameObject engineerTeam;

    
    public int numSites = 10;
    public int numRelax = 5;
    public int size = 10;

    public List<Node> path;

    private Bounds FBounds;
    private Biomes FBiomes;
    private Generation FGeneration;

    private float FTimeLastSelect;

    public float funds = 300000;

	//private bool rescueStarted;

    public Mode FMode;
	public Mode FOldMode;

	public void SetMode(Mode mode)
	{
		FMode = mode;
	}

    void Start()
    {
        VoronoiGenerator voronoi;
        
        Graph.Delete();
        Groupments.Delete();
        
        seed = GameObject.Find("StartScreen").GetComponent<StartScreen>().GetSeed();
		eventSeed = GameObject.Find("StartScreen").GetComponent<StartScreen>().GetEventSeed();

		//rescueStarted = false;

        UnityEngine.Random.seed = seed;

        FBounds = new Bounds(new Vector3(0, 0, 0), new Vector3(size, 0, size));

        FMode = Mode.Basic;

        FBiomes = new Biomes(size, numSites);
        FGeneration = new Generation(size, numSites);

        voronoi = new VoronoiGenerator();
        path = new List<Node>();

        voronoi.Execute(numSites, FBounds, true, true, numRelax);

        CreateChunks(voronoi);

        FBiomes.AssignCoastAndLand();
        FBiomes.AssignCornerElevations();
        FBiomes.NormalizeElevation();
        FBiomes.AssignPolygonElevations();
        FBiomes.CalculateDownslopes();
        FBiomes.CreateRivers();
        FBiomes.AssignCornerMoisture();
        FBiomes.AssignPolygonMoisture();
        FBiomes.AssignBiomes();

        foreach (Node node in Graph.Instance().GetNodes())
        {
            node.Tile.material = new Material(node.Tile.material);
            node.Tile.selectedMaterial = new Material(node.Tile.selectedMaterial);
            node.Tile.selectedCityMaterial = new Material(node.Tile.selectedMaterial);

            node.Tile.material.color = View.colorFromBiome(node);
            node.Tile.selectedMaterial.color = View.colorFromBiome(node);
            node.Tile.selectedCityMaterial.color = View.colorFromBiome(node);

            node.Tile.GetComponent<MeshRenderer>().sharedMaterial = node.Tile.material;
        }

        FGeneration.CreateRoads();
        FGeneration.AssignCities();
        FGeneration.AssignGroupements();
        FGeneration.GroupCities();

		//End of generation
		UnityEngine.Random.seed = eventSeed;

        GetComponent<LineDrawer>().Initiate();
    }

    void CreateChunks(VoronoiGenerator voronoi)
    {
        foreach (Voronoi.Cell cell in voronoi.graph.cells)
        {
            GameObject tileClone = Instantiate(tile, cell.site.ToVector3(), Quaternion.identity) as GameObject;
            tileClone.name = "Tile " + cell.site.id;
            tileClone.transform.parent = GameObject.FindGameObjectWithTag("Background").transform;
            tileClone.GetComponent<Tile>().Initiate();
            tileClone.GetComponent<Tile>().CreateFanMesh(cell);
            tileClone.GetComponent<Tile>().AddCorners(cell);
        }

        foreach (Voronoi.Edge edge in voronoi.graph.edges)
        {
            if (edge.lSite != null && edge.rSite != null)
            {
                GameObject lObject = GameObject.Find("/Background/Tile " + edge.lSite.id);
                GameObject rObject = GameObject.Find("/Background/Tile " + edge.rSite.id);

                lObject.GetComponent<Tile>().node.AddSon(rObject.GetComponent<Tile>());
                rObject.GetComponent<Tile>().node.AddSon(lObject.GetComponent<Tile>());
            }
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Backspace))
            Destroy(GameObject.FindWithTag("Selected"));
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameObject[] selectedTiles = GameObject.FindGameObjectsWithTag("Selected");
            GameObject[] selectedCityTiles = GameObject.FindGameObjectsWithTag("SelectedCity");
            GameObject[] selectedHospitalTiles = GameObject.FindGameObjectsWithTag("SelectedHospital");
            GameObject[] selectedDepotTiles = GameObject.FindGameObjectsWithTag("SelectedDepot");
            foreach (GameObject tile in selectedTiles)
                tile.tag = "Node";
            foreach (GameObject cityTile in selectedCityTiles)
                cityTile.tag = "Node";
            foreach (GameObject hospitalTile in selectedHospitalTiles)
                hospitalTile.tag = "Node";
            foreach (GameObject depotTile in selectedDepotTiles)
                depotTile.tag = "Node";
        }

        if (ModeHasChanged())
        {
            DisplayMode();
        }

        #region Shift + KeyboardKey
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                LineDrawer lineDrawer = gameObject.GetComponent<LineDrawer>();

                if (!lineDrawer.edgeDisplayType)
                {
                    lineDrawer.edgeDisplayType = true;
                    FBiomes.AssignHeightToMeshesThird();
                    lineDrawer.RecalculateAdditionalEdgesThird();
                }
                else
                {
                    lineDrawer.edgeDisplayType = false;
                    FBiomes.UnassignHeightToMeshesThird();
                }
            }
            if (Input.GetKeyDown(KeyCode.T))
            {
                GameObject.Find("Timer Text").GetComponent<Timer>().AddTime(600);
            }
            if (Input.GetKeyDown(KeyCode.Q))
            {
                Destroy(GameObject.Find("StartScreen"));
                Application.LoadLevel("Start");
            }
            //if (Input.GetKeyDown(KeyCode.C))
            //{
            //    GameObject cities = GameObject.FindGameObjectWithTag("Cities");
            //    for (int i = 0; i < cities.transform.childCount; i++)
            //    {
            //        cities.transform.GetChild(i).GetComponent<MeshRenderer>().enabled = !cities.transform.GetChild(i).GetComponent<MeshRenderer>().enabled;
            //    }
            //}
            if (Input.GetKeyDown(KeyCode.Space))
            {
				StartRescue();
            }
            if (Input.GetKeyDown(KeyCode.A))
            {
                GameObject.FindGameObjectWithTag("Canvas").GetComponent<InfoDisplay>().AddVehicleToBuilding();
            }
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                Time.timeScale += 0.5f;
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                Time.timeScale -= 0.5f;
            }

            //if (Input.GetKeyDown(KeyCode.Space))
            //{
            //    GameObject[] selectedTiles = GameObject.FindGameObjectsWithTag("Selected");
            //    GameObject[] selectedCityTiles = GameObject.FindGameObjectsWithTag("SelectedCity");
            //    foreach (GameObject tile in selectedTiles)
            //        tile.tag = "Node";
            //    foreach (GameObject cityTile in selectedCityTiles)
            //        cityTile.tag = "Node";

            //    InfoDisplay infoDisplay = GameObject.FindGameObjectWithTag("Canvas").GetComponent<InfoDisplay>();
            //    while (infoDisplay.buildingsLeft != 0)
            //    {
            //        Graph.Instance().GetRandomNode().Tile.tag = "Selected";
            //        infoDisplay.PutHospital();
            //    }
            //    while (infoDisplay.buildingsLeft != 0)
            //    {
            //        Graph.Instance().GetRandomNode().Tile.tag = "Selected";
            //        infoDisplay.PutDepot();
            //    }
            //}
        }
        #endregion

        #region MouseLeftClick Handling
        if (Input.GetMouseButtonDown(0))
        {
            if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(-1))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                double groundLimit = Math.Pow(Math.Abs(transform.position.x) + FBounds.extents.x, 2) + Math.Pow(Math.Abs(transform.position.z) + FBounds.extents.z, 2);
                float rayLimit = Convert.ToSingle(Math.Pow(transform.position.y, 2) + groundLimit);

                GameObject[] selectedTiles = GameObject.FindGameObjectsWithTag("Selected");
                GameObject[] selectedCityTiles = GameObject.FindGameObjectsWithTag("SelectedCity");
                GameObject[] selectedHospitalTiles = GameObject.FindGameObjectsWithTag("SelectedHospital");
                GameObject[] selectedDepotTiles = GameObject.FindGameObjectsWithTag("SelectedDepot");

                if (Physics.Raycast(ray, out hit, rayLimit))
                {
                    foreach (GameObject tile in selectedTiles)
                        tile.tag = "Node";
                    foreach (GameObject tile in selectedCityTiles)
                        tile.tag = "Node";
                    foreach (GameObject tile in selectedHospitalTiles)
                        tile.tag = "Node";
                    foreach (GameObject tile in selectedDepotTiles)
                        tile.tag = "Node";

                    if (hit.collider.tag == "Node")
                    {
                        GameObject collider = hit.collider.gameObject;
                        collider.tag = "Selected";

                        Node node = collider.GetComponent<Tile>().node;
                        if (node.city != null || node.suburb != null)
                            collider.tag = "SelectedCity";
                        else if (node.Tile.hasHospital)
                            collider.tag = "SelectedHospital";
                        else if (node.Tile.hasDepot)
                            collider.tag = "SelectedDepot";
                    }
                    else if (hit.collider.tag == "City")
                    {
                        Node node = hit.collider.GetComponent<City>().center;
                        node.Tile.tag = "SelectedCity";
                    }
                    else if (hit.collider.tag == "Suburb")
                    {
                        Node node = hit.collider.GetComponent<Suburb>().node;
                        node.Tile.tag = "SelectedCity";
                    }
                    else if (hit.collider.tag == "Hospital")
                    {
                        Node node = hit.collider.GetComponent<Hospital>().location;
                        node.Tile.tag = "SelectedHospital";
                    }
                    else if (hit.collider.tag == "Depot")
                    {
                        Node node = hit.collider.GetComponent<Depot>().location;
                        node.Tile.tag = "SelectedDepot";
                    }
                    else if (hit.collider.tag == "Selected")
                    {
                        if (Time.realtimeSinceStartup - FTimeLastSelect < 1)
                        {
                            // TODO: Function using double click
                        }
                    }
                    FTimeLastSelect = Time.realtimeSinceStartup;
                }
            }
        }
        #endregion
    }

	public void DisplayMode()
	{
        if (FOldMode == Mode.Height)
            FBiomes.UnassignHeightToMeshes();
        if (FMode == Mode.Basic)
            foreach (Node node in Graph.Instance().GetNodes())
                node.Tile.AssignNewColor(View.colorFromBiome(node));
        else if (FMode == Mode.Moisture)
            foreach (Node node in Graph.Instance().GetNodes())
                node.Tile.AssignNewColor(View.colorFromMoisture(node));
        else if (FMode == Mode.Height)
            FBiomes.AssignHeightToMeshes();
        else if (FMode == Mode.Damages)
            foreach (Node node in Graph.Instance().GetNodes())
                node.Tile.AssignNewColor(View.colorFromDamages(node));
        else if (FMode == Mode.Injured)
            foreach (Node node in Graph.Instance().GetNodes())
                node.Tile.AssignNewColor(View.colorFromInjured(node));
        FOldMode = FMode;
	}

	private bool ModeHasChanged()
	{
		return FOldMode != FMode;
	}

	//shift + i method
	public static void StartRescue()
	{
       //if (rescueStarted == false) {
		foreach (Node node in Graph.Instance().GetNodes())
		{
			if (Biomes.isDamageable (node.type))
			{
				node.InitializeDamage(0.6f);
				Graph.Instance().totalDamages += node.damaged;
			}
		}

		Dictionary<string, Groupment>.ValueCollection groupments = Groupments.Instance().GetGroupments ();
		
		foreach (Groupment groupment in groupments)
		{
			groupment.InitiateInjuries(0.3f);
			Groupments.Instance().totalInjured += groupment.Injured;

			GameObject[] medicalTeams = GameObject.FindGameObjectsWithTag("MedicalTeam");
            foreach (GameObject team in medicalTeams)
                team.GetComponent<MedicalTeam>().SetActive();

			GameObject[] engineerTeams = GameObject.FindGameObjectsWithTag("EngineerTeam");
			foreach (GameObject team in engineerTeams)
				team.GetComponent<EngineerTeam>().SetActive();

			GameObject.Find("Timer Text").GetComponent<Timer>().setStarted(true);
		}
			//rescueStarted = true;
		//}
	}
}
