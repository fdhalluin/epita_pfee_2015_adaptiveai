﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Voronoi
{
    public class HalfEdge
    {
        public Point site;
        public Edge edge;
        public float angle;

        public HalfEdge(Edge edge, Point lSite, Point rSite)
        {
            this.site = lSite;
            this.edge = edge;
            // 'angle' is a value to be used for properly sorting the
            // halfsegments counterclockwise. By convention, we will
            // use the angle of the line defined by the 'site to the left'
            // to the 'site to the right'.
            // However, border edges have no 'site to the right': thus we
            // use the angle of line perpendicular to the halfsegment (the
            // edge should have both end points defined in such case.)
            if (rSite)
            {
                this.angle = Mathf.Atan2(rSite.y - lSite.y, rSite.x - lSite.x);
            }
            else
            {
                Point va = edge.va;
                Point vb = edge.vb;
                // rhill 2011-05-31: used to call getStartpoint()/getEndpoint(),
                // but for performance purpose, these are expanded in place here.
                this.angle = (edge.lSite == lSite) ? Mathf.Atan2(vb.x - va.x, va.y - vb.y)
                                                   : Mathf.Atan2(va.x - vb.x, vb.y - va.y);
            }
        }

        public Point GetStartPoint()
        {
            return this.edge.lSite == this.site ? this.edge.va : this.edge.vb;
        }

        public Point GetEndPoint()
        {
            return this.edge.lSite == this.site ? this.edge.vb : this.edge.va;
        }

        public static implicit operator bool(HalfEdge a)
        {
            return a != null;
        }
    }
}