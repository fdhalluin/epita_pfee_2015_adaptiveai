﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartScreen : MonoBehaviour {
	
	private int FSeed;
    private int FDefaultSeed;
	private int FEventSeed;
    private int FDefaultEventSeed;

	public MedicalIA medicalIAChosen;
	public EngineerIA engineerIAChosen;

	public GameObject canvas;
	public GameObject optionCanvas;

	public int score;
	public double percentInjured;
	public double percentDamage;

    public Text MapPlaceholderText;
    public Text EventPlaceholderText;
	
	void Start()
	{
		DontDestroyOnLoad(gameObject);

        if (Application.isEditor)
            FDefaultSeed = 0;
        else
            FDefaultSeed = Random.Range(0, 1000);
        FSeed = FDefaultSeed;

        if (Application.isEditor)
            FDefaultEventSeed = 0;
        else
            FDefaultEventSeed = Random.Range(0, 1000);
        FEventSeed = FDefaultEventSeed;

        MapPlaceholderText = GameObject.FindGameObjectWithTag("MapPlaceholder").GetComponent<Text>();
        EventPlaceholderText = GameObject.FindGameObjectWithTag("EventPlaceholder").GetComponent<Text>();

        MapPlaceholderText.text = FDefaultSeed.ToString();
        EventPlaceholderText.text = FDefaultEventSeed.ToString();
	}

    void Update()
    {
        if (Application.loadedLevel == 0)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                Application.LoadLevel("Main");
            }
            else if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (Application.isEditor)
                    Debug.Log("Kikoo");
                else
                    Application.Quit();
            }
        }
    }
    
    public void EndEdit()
	{
		InputField seedText = GameObject.Find("MapSeed").GetComponent<InputField>();
		if (!int.TryParse(seedText.text, out FSeed))
		{
			FSeed = FDefaultSeed;
		}
		InputField eventSeedText = GameObject.Find("EventSeed").GetComponent<InputField>();
		if (!int.TryParse(eventSeedText.text, out FEventSeed))
		{
			FEventSeed = FDefaultEventSeed;
		}
	}

	public void OnClick()
	{
		Application.LoadLevel("Main");
	}

	public void OptionOnClick()
	{
		canvas.SetActiveRecursively(false);
		optionCanvas.SetActiveRecursively(true);
	}

	public void basicMedicalIA()
	{
		medicalIAChosen = MedicalIA.Basic;
	}

	public void adaptativeMedicalIA()
	{
		medicalIAChosen = MedicalIA.Adaptative;
	}

	public void basicEngineerIA()
	{
		engineerIAChosen = EngineerIA.Basic;
	}

	public void adaptativeEngineerIA()
	{
		engineerIAChosen = EngineerIA.Adaptative;
	}

	public void back()
	{
		canvas.SetActiveRecursively(true);
		optionCanvas.SetActiveRecursively(false);
	}

	public int GetSeed()
	{
		return FSeed;
	}

	public void SetSeed(int specifiedSeed)
	{
		FSeed = specifiedSeed;
	}

	public int GetEventSeed()
	{
		return FEventSeed;
	}
	
	public void SetEventSeed(int specifiedSeed)
	{
		FEventSeed = specifiedSeed;
	}
}