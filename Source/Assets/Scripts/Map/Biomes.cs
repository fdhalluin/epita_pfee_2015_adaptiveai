﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum BiomePreType
{
    None = 0x0,
    PreOcean,
    PreWater,
    PreLand,
    PreCoast
}

public enum BiomeType
{
    None = 0x0,
    Ocean,
    Marsh,
    Ice,
    Lake,
    Beach,
    Snow,
    Tundra,
    Bare,
    Scorched,
    Taiga,
    Shrubland,
    TemperateDesert,
    TemperateRainForest,
    TemperateDeliduousForest,
    Grassland,
    TropicalRainForest,
    TropicalSeasonalForest,
    SubtropicalDesert
}

public enum SimpleBiomeType
{
    None = 0x0,
    Ocean,
    Ice,
    Lake,
    Beach,
    Snow,
    Grassland,
    Forest,
    Desert
}

class Biomes
{
    private int FSize;
    private int FSite;

    private int FBumps;
    private double FStartAngle;
    private double FDipAngle;
    private double FDipWidth;

    public Biomes(int size, int site)
    {
        FSize = size;
        FSite = site;

        FBumps = UnityEngine.Random.Range(1, 6);
        FStartAngle = UnityEngine.Random.Range(0f, 2 * Convert.ToSingle(Math.PI));
        FDipAngle = UnityEngine.Random.Range(0.0f, 2 * Convert.ToSingle(Math.PI));
        FDipWidth = UnityEngine.Random.Range(0.2f, 0.7f);

    }

    public void AssignCoastAndLand()
    {
        foreach (Corner corner in Graph.Instance().GetCorners())
        {
            if (corner.GetSizeNodes() <= 2)
                corner.type = BiomePreType.PreOcean;
            else
            {
                float distanceFromCenter = corner.Position.magnitude;
                double angle = Convert.ToSingle(Math.Atan2(corner.Position.z, corner.Position.x));

                double r1 = 0.5 + 0.40 * Math.Sin(FStartAngle + FBumps * angle + Math.Cos((FBumps + 3) * angle));
                double r2 = 0.7 - 0.20 * Math.Sin(FStartAngle + FBumps * angle - Math.Sin((FBumps + 2) * angle));

                if (Math.Abs(angle - FDipAngle) < FDipWidth
                    || Math.Abs(angle - FDipAngle + Math.PI) < FDipWidth
                    || Math.Abs(angle - FDipAngle - Math.PI) < FDipWidth)
                {
                    r1 = r2 = 0.2;

                }

                if (distanceFromCenter < r1 * FSize / 2 || (distanceFromCenter > r1 * FSize / 2 * 1.07 && distanceFromCenter < r2 * FSize / 2))
                    corner.type = BiomePreType.PreLand;
                else
                    corner.type = BiomePreType.PreWater;
            }
        }

        Queue<Corner> openList = new Queue<Corner>();
        foreach (Corner corner in Graph.Instance().GetCorners())
        {
            if (corner.GetSizeNodes() <= 2)
            {
                corner.type = BiomePreType.PreOcean;
                openList.Enqueue(corner);
            }
        }

        while (openList.Count > 0)
        {
            Corner actual = openList.Dequeue();
            foreach (Corner neighbor in actual.Sons)
            {
                if (neighbor.type != BiomePreType.PreOcean && neighbor.type != BiomePreType.PreLand)
                {
                    neighbor.type = BiomePreType.PreOcean;
                    openList.Enqueue(neighbor);
                }
            }
        }

        foreach (Node node in Graph.Instance().GetNodes())
            node.SetPreTypeResult();

        foreach (Corner corner in Graph.Instance().GetCorners())
            corner.SetNodesMajorType();
    }

    public void AssignCornerElevations()
    {
        Queue<Corner> openList = new Queue<Corner>();

        foreach (Corner corner in Graph.Instance().GetCorners())
        {
            if (corner.GetSizeNodes() <= 2)
            {
                corner.height = 0.0f;
                openList.Enqueue(corner);
            }
            else
                corner.height = Single.PositiveInfinity;
        }

        while (openList.Count != 0)
        {
            Corner actual = openList.Dequeue();
            foreach (Corner neighbor in actual.Sons)
            {
                float newElevation = 0.01f + actual.height;
                if (neighbor.type != BiomePreType.PreOcean)
                    newElevation += 1;

                if (newElevation < neighbor.height)
                {
                    neighbor.height = newElevation;
                    openList.Enqueue(neighbor);
                }
            }
        }
    }

    public void NormalizeElevation()
    {
        float maxHeight = -1;
        foreach (Corner corner in Graph.Instance().GetCorners())
        {
            if (corner.height > maxHeight)
                maxHeight = corner.height;
        }

        foreach (Corner corner in Graph.Instance().GetCorners())
        {
            if (corner.type == BiomePreType.PreOcean)
                corner.height = 0;
            else
                corner.height = Convert.ToSingle(Math.Pow(corner.height / maxHeight, 2));
        }
    }

    public void AssignPolygonElevations()
    {
        List<Node> closedList = new List<Node>();

        foreach (Node node in Graph.Instance().GetNodes())
        {
            if (node.preType == BiomePreType.PreWater && !closedList.Contains(node))
            {
                List<Node> actualList = new List<Node>();
                Queue<Node> openList = new Queue<Node>();
 
                openList.Enqueue(node);
                actualList.Add(node);
                
                while (openList.Count > 0)
                {
                    Node actual = openList.Dequeue();
                    closedList.Add(actual);
                    foreach (Node neighbor in actual.Sons.Keys)
                    {
                        if (neighbor.preType == BiomePreType.PreWater && !closedList.Contains(neighbor))
                        {
                            openList.Enqueue(neighbor);
                            actualList.Add(neighbor);
                        }
                    }
                }

                float newHeight = 0;

                foreach (Node actual in actualList)
                {
                    float sum = 0;
                    foreach (Corner corner in actual.Corners)
                    {
                        sum += corner.height;
                    }
                    newHeight += sum / actual.Corners.Count;
                }

                newHeight /= actualList.Count;

                foreach (Node actual in actualList)
                    foreach (Corner corner in actual.Corners)
                        corner.height = newHeight;
            }
        }

        foreach (Node node in Graph.Instance().GetNodes())
        {
            float sumElevation = 0;
            foreach (Corner corner in node.Corners)
            {
                sumElevation += corner.height;
            }
            node.height = sumElevation / node.Corners.Count;
        }
    }

    public void CalculateDownslopes()
    {
        foreach (Corner actual in Graph.Instance().GetCorners())
        {
            actual.downslope = actual;
            foreach (Corner neighbor in actual.Sons)
            {
                if (neighbor.height < actual.downslope.height)
                    actual.downslope = neighbor;
            }
        }
    }

    public void CreateRivers()
    {
        for (int i = 0; i < FSite / 8; i++)
        {
            Corner corner = Graph.Instance().Corner(Graph.Instance().GetRandomCorner());
            if (corner.type == BiomePreType.PreOcean
                || corner.height < 0.3
                || corner.height > 0.9)
                continue;

            while (corner.type != BiomePreType.PreOcean)
            {
                if (corner == corner.downslope)
                    break;

                corner.river = corner.river + 1;
                corner.downslope.river = corner.downslope.river + 1;

                corner = corner.downslope;
            }
        }
    }

    public void AssignCornerMoisture()
    {
        Queue<Corner> openList = new Queue<Corner>();

        foreach (Corner corner in Graph.Instance().GetCorners())
        {
            if (corner.type == BiomePreType.PreWater || (corner.river > 0 && corner.type != BiomePreType.PreOcean))
            {
                corner.moisture = corner.river > 0 ? Math.Min(3.0, (0.2 * corner.river)) : 1.0;
                openList.Enqueue(corner);
            }
            else
                corner.moisture = 0.0f;
        }

        while (openList.Count > 0) {
            Corner corner = openList.Dequeue();

            foreach (Corner neighbor in corner.Sons) {
                double newMoisture = corner.moisture * 0.9;
                if (newMoisture > neighbor.moisture) {
                    neighbor.moisture = newMoisture;
                    openList.Enqueue(neighbor);
                }
            }
        }

        List<Corner> sortedList = new List<Corner>();

        foreach (Corner corner in Graph.Instance().GetCorners())
        {
            if (corner.type == BiomePreType.PreOcean)
                corner.moisture = 1;
            else
            {
                int i = 0;
                for (; i < sortedList.Count; i++)
                {
                    if (sortedList[i].moisture > corner.moisture)
                    {
                        sortedList.Insert(i, corner);
                        break;
                    }
                }
                if (i == sortedList.Count)
                    sortedList.Add(corner);
            }
        }

        for (double i = 0; i < sortedList.Count; i++)
            sortedList[Convert.ToInt32(i)].moisture = i / (sortedList.Count - 1);


        //double maxMoisture = -1;
        //foreach (Corner corner in Graph.Instance().GetCorners())
        //{
        //    if (corner.moisture > maxMoisture)
        //        maxMoisture = corner.moisture;
        //}

        //foreach (Corner corner in Graph.Instance().GetCorners())
        //{
        //    if (corner.type == BiomePreType.PreOcean)
        //        corner.moisture = 1;
        //    else
        //        corner.moisture /= maxMoisture;
        //}
    }

    public void AssignPolygonMoisture()
    {
        foreach (Node node in Graph.Instance().GetNodes())
        {
            double sumMoisture = 0;
            foreach (Corner corner in node.Corners)
            {
                sumMoisture += corner.moisture;
            }
            node.moisture = sumMoisture / node.Corners.Count;
        }
    }

    static public BiomeType getBiome(Node node)
    {
        if (node.height == 0 || node.preType == BiomePreType.PreOcean)
        {
            node.height = 0;
            return BiomeType.Ocean;
        }
        else if (node.preType == BiomePreType.PreWater)
        {
            if (node.height < 0.1)
                return BiomeType.Marsh;
            else if (node.height > 0.8)
                return BiomeType.Ice;
            else
                return BiomeType.Lake;
        }
        else if (node.preType == BiomePreType.PreCoast)
        {
            return BiomeType.Beach;
        }
        else if (node.height > 0.8)
        {
            if (node.moisture > 0.50)
                return BiomeType.Snow;
            else if (node.moisture > 0.33)
                return BiomeType.Tundra;
            else if (node.moisture > 0.16)
                return BiomeType.Bare;
            else
                return BiomeType.Scorched;
        }
        else if (node.height > 0.6)
        {
            if (node.moisture > 0.66)
                return BiomeType.Taiga;
            else if (node.moisture > 0.33)
                return BiomeType.Shrubland;
            else
                return BiomeType.TemperateDesert;
        }
        else if (node.height > 0.3)
        {
            if (node.moisture > 0.83)
                return BiomeType.TemperateRainForest;
            else if (node.moisture > 0.50)
                return BiomeType.TemperateDeliduousForest;
            else if (node.moisture > 0.16)
                return BiomeType.Grassland;
            else
                return BiomeType.TemperateDesert;
        }
        else
        {
            if (node.moisture > 0.66)
                return BiomeType.TropicalRainForest;
            else if (node.moisture > 0.33)
                return BiomeType.TropicalSeasonalForest;
            else if (node.moisture > 0.16)
                return BiomeType.Grassland;
            else
                return BiomeType.SubtropicalDesert;
        }
    }
    
    public void AssignBiomes()
    {
        foreach (Node node in Graph.Instance().GetNodes())
        {
            node.type = getBiome(node);
        }
    }

    static public bool isWalkable(BiomeType biome)
    {
        if (biome == BiomeType.Lake || biome == BiomeType.Ocean)
            return false;
        return true;
    }

    static public bool isNavigable(BiomeType biome)
    {
        if (biome == BiomeType.Lake || biome == BiomeType.Ocean)
            return true;
        return false;
    }

    static public bool isDamageable(BiomeType biome)
    {
        if (biome == BiomeType.Lake || biome == BiomeType.Ocean || biome == BiomeType.Snow || biome == BiomeType.Marsh)
            return false;
        return true;
    }

	static public bool isConstructible(BiomeType biome)
	{
		if (biome == BiomeType.Lake || biome == BiomeType.Marsh || biome == BiomeType.Ocean || biome == BiomeType.Ice)
			return false;
		return true;
	}

    public void AssignHeightToMeshes()
    {
        float ratio = FSize / 10.0f;

        foreach (Node node in Graph.Instance().GetNodes())
        {
            node.Tile.transform.Translate(0, node.height * ratio, 0);

            Vector3[] vertices = node.Tile.mesh.vertices;

            for (int i = 1; i < vertices.Length; i++)
            {
                Corner corner = Graph.Instance().GetCorner(new Vector3(Convert.ToSingle(Math.Round(node.Tile.transform.position.x + vertices[i].x, 2)), 0, Convert.ToSingle(Math.Round(node.Tile.transform.position.z + vertices[i].z, 2))));
                vertices[i].y = (corner.height - node.height) * ratio;
                corner.AssignHeight(ratio);
            }

            node.Tile.mesh.vertices = vertices;

            node.Tile.mesh.RecalculateBounds();
            node.Tile.mesh.RecalculateNormals();

            node.Tile.GetComponent<MeshCollider>().sharedMesh = null;
            node.Tile.GetComponent<MeshCollider>().sharedMesh = node.Tile.mesh;
        }
    }

	public void UnassignHeightToMeshes()
	{
		foreach (Node node in Graph.Instance().GetNodes())
		{
            node.Tile.transform.Translate(0, -node.height * FSize / 10.0f, 0);
            
            Vector3[] vertices = node.Tile.mesh.vertices;
			
            for (int i = 1; i < vertices.Length; i++)
			{
				vertices[i].y = 0;
			}
			node.Tile.mesh.vertices = vertices;
			
			node.Tile.mesh.RecalculateBounds();
			node.Tile.mesh.RecalculateNormals();
			
			node.Tile.GetComponent<MeshCollider>().sharedMesh = null;
			node.Tile.GetComponent<MeshCollider>().sharedMesh = node.Tile.mesh;
		}
		
		foreach (Corner corner in Graph.Instance().GetCorners())
		{
			corner.DisableHeight();
		}
	}

    public void AssignHeightToMeshesSecond()
    {
        float ratio = FSize / 20.0f;

        foreach (Node node in Graph.Instance().GetNodes())
        {
            Vector3[] vertices = new Vector3[node.Tile.mesh.vertexCount * 2 - 1];

            node.Tile.transform.Translate(0, node.height * ratio, 0);

            int cornerNumber = node.Tile.mesh.vertexCount - 1;

            //vertices[0].y += node.height * ratio;

            for (int i = 1; i < cornerNumber + 1; i++)
            {
                vertices[i] = node.Tile.mesh.vertices[i];
                vertices[i + cornerNumber] = node.Tile.mesh.vertices[i];
                vertices[i + cornerNumber].y = -node.height * ratio;
            }

            int[] triangles = new int[node.Tile.mesh.triangles.Length + cornerNumber * 6];

            for (int i = 0; i < node.Tile.mesh.triangles.Length; i++)
            {
                triangles[i] = node.Tile.mesh.triangles[i];
            }

            for (int i = 1, t = node.Tile.mesh.triangles.Length; i < cornerNumber + 1; i++, t += 6)
            {
                triangles[t] = i + 1;
                triangles[t + 1] = i;
                triangles[t + 2] = i + 1 + cornerNumber;
                triangles[t + 3] = i;
                triangles[t + 4] = i + cornerNumber;
                triangles[t + 5] = i + 1 + cornerNumber;
            }

            triangles[triangles.Length - 6] = 1;
            triangles[triangles.Length - 4] = cornerNumber + 1;
            triangles[triangles.Length - 1] = cornerNumber + 1;

            node.Tile.mesh.vertices = vertices;
            node.Tile.mesh.triangles = triangles;

            node.Tile.mesh.RecalculateBounds();
            node.Tile.mesh.RecalculateNormals();

            node.Tile.GetComponent<MeshCollider>().sharedMesh = null;
            node.Tile.GetComponent<MeshCollider>().sharedMesh = node.Tile.mesh;
        }

        foreach (Corner corner in Graph.Instance().GetCorners())
            corner.AssignHeight(ratio);
    }

    public void UnassignHeightToMeshesSecond()
    {
        float ratio = FSize / 20.0f;

        foreach (Node node in Graph.Instance().GetNodes())
        {
            int cornerNumber = node.Tile.mesh.vertexCount / 2;
            Vector3[] vertices = new Vector3[cornerNumber + 1];

            node.Tile.transform.Translate(0, -node.height * ratio, 0);


            for (int i = 0; i < cornerNumber + 1; i++)
            {
                vertices[i] = node.Tile.mesh.vertices[i];
                vertices[i].y = 0;
            }

            int[] triangles = new int[cornerNumber * 3];

            for (int i = 0; i < cornerNumber * 3; i++)
            {
                triangles[i] = node.Tile.mesh.triangles[i];
            }

            node.Tile.mesh.triangles = triangles;
            node.Tile.mesh.vertices = vertices;

            node.Tile.mesh.RecalculateBounds();
            node.Tile.mesh.RecalculateNormals();

            node.Tile.GetComponent<MeshCollider>().sharedMesh = null;
            node.Tile.GetComponent<MeshCollider>().sharedMesh = node.Tile.mesh;
        }

        foreach (Corner corner in Graph.Instance().GetCorners())
            corner.DisableHeight();
    }

    public void AssignHeightToMeshesThird()
    {
        float ratio = FSize / 20.0f;

        foreach (Node node in Graph.Instance().GetNodes())
        {
            Vector3[] vertices = new Vector3[node.Tile.mesh.vertexCount * 2 - 1];

            node.Tile.transform.Translate(0, node.height * ratio, 0);

            int cornerNumber = node.Tile.mesh.vertexCount - 1;

            vertices[0] = node.Tile.mesh.vertices[0];

            for (int i = 1; i < cornerNumber + 1; i++)
            {
                vertices[i] = node.Tile.mesh.vertices[i];
                vertices[i] /= 1.5f;
                vertices[i].y = 0;
                vertices[i + cornerNumber] = node.Tile.mesh.vertices[i];
                Corner corner = Graph.Instance().GetCorner(new Vector3(Convert.ToSingle(Math.Round(node.Tile.transform.position.x + node.Tile.mesh.vertices[i].x, 2)), 0, Convert.ToSingle(Math.Round(node.Tile.transform.position.z + node.Tile.mesh.vertices[i].z, 2))));
                vertices[i + cornerNumber].y = (corner.height - node.height) * ratio;
                corner.AssignHeight(ratio);
            }

            int[] triangles = new int[node.Tile.mesh.triangles.Length + cornerNumber * 6];

            for (int i = 0; i < node.Tile.mesh.triangles.Length; i++)
            {
                triangles[i] = node.Tile.mesh.triangles[i];
            }

            for (int i = 1, t = node.Tile.mesh.triangles.Length; i < cornerNumber + 1; i++, t += 6)
            {
                triangles[t] = i + 1;
                triangles[t + 1] = i;
                triangles[t + 2] = i + 1 + cornerNumber;
                triangles[t + 3] = i;
                triangles[t + 4] = i + cornerNumber;
                triangles[t + 5] = i + 1 + cornerNumber;
            }

            triangles[triangles.Length - 6] = 1;
            triangles[triangles.Length - 4] = cornerNumber + 1;
            triangles[triangles.Length - 1] = cornerNumber + 1;

            node.Tile.mesh.vertices = vertices;
            node.Tile.mesh.triangles = triangles;

            node.Tile.mesh.RecalculateBounds();
            node.Tile.mesh.RecalculateNormals();

            node.Tile.GetComponent<MeshCollider>().sharedMesh = null;
            node.Tile.GetComponent<MeshCollider>().sharedMesh = node.Tile.mesh;
        }
    }

    public void UnassignHeightToMeshesThird()
    {
        float ratio = FSize / 20.0f;

        foreach (Node node in Graph.Instance().GetNodes())
        {
            int cornerNumber = node.Tile.mesh.vertexCount / 2;
            Vector3[] vertices = new Vector3[cornerNumber + 1];

            node.Tile.transform.Translate(0, -node.height * ratio, 0);


            for (int i = 0; i < cornerNumber + 1; i++)
            {
                vertices[i] = node.Tile.mesh.vertices[i];
                vertices[i] *= 1.5f;
                vertices[i].y = 0;
            }

            int[] triangles = new int[cornerNumber * 3];

            for (int i = 0; i < cornerNumber * 3; i++)
            {
                triangles[i] = node.Tile.mesh.triangles[i];
            }

            node.Tile.mesh.triangles = triangles;
            node.Tile.mesh.vertices = vertices;

            node.Tile.mesh.RecalculateBounds();
            node.Tile.mesh.RecalculateNormals();

            node.Tile.GetComponent<MeshCollider>().sharedMesh = null;
            node.Tile.GetComponent<MeshCollider>().sharedMesh = node.Tile.mesh;
        }

        foreach (Corner corner in Graph.Instance().GetCorners())
            corner.DisableHeight();
    }
}
