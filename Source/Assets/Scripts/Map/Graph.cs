﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;

public class Graph
{
    //============
    // Singleton
    //============

    private static Graph FInstance;

    static public Graph Instance()
    {
        if (FInstance == null)
            FInstance = new Graph();
        return FInstance;
    }

    static public bool Delete()
    {
        if (FInstance != null && FInstance.FCorners.Count == 0 && FInstance.FGraph.Count == 0)
        {
            FInstance = null;
            return true;
        }

        return false;
    }

    //============
    // Variables
    //============

    private Dictionary<string, Node> FGraph;
    private Dictionary<Vector3, Corner> FCorners;
	public float totalDamages;

    //============
    // Des/Constructor
    //============

    private Graph()
    {
        FGraph = new Dictionary<string, Node>();
        FCorners = new Dictionary<Vector3, Corner>();
		totalDamages = 0;
    }

    //============
    // Other methods
    //============


    // Graph methods
    //============
    public Node AddNode(Tile tile)
    {
        Node tmp = new Node(tile);
        FGraph.Add(tmp.Tile.name, tmp);

        return tmp;
    }

    public Node GetNode(string key)
    {
        return FGraph[key];
    }

    public void RenameNode(string key, string newKey)
    {
        Node node = FGraph[key];
        FGraph.Remove(key);
        FGraph.Add(newKey, node);
        node.Tile.name = newKey;
    }

    //public string GetRandomNode()
    //{
    //    string[] keys = new string[FGraph.Count];
    //    FGraph.Keys.CopyTo(keys, 0);

    //    return keys[UnityEngine.Random.Range(0, FGraph.Count)];
    //}

    public void RemoveNode(Node key)
    {
        FGraph.Remove(key.Tile.name);

        foreach (Node station in FGraph.Values)
        {
            if (station.Sons.ContainsKey(key))
                station.Sons.Remove(key);
        }
    }

    public Dictionary<string, Node>.ValueCollection GetNodes()
    {
        return FGraph.Values;
    }

    public bool Exists(string key)
    {
        return FGraph.ContainsKey(key);
    }

    public int GetSize()
    {
        return FGraph.Count;
    }

    public Node GetRandomNode()
    {
        Node[] keys = new Node[FCorners.Count];
        FGraph.Values.CopyTo(keys, 0);

        return keys[UnityEngine.Random.Range(0, FGraph.Count)];
    }

    public static Dictionary<Node, float> getNodesAtDistance(Node origin, int distance)
    {
        Dictionary<Node, float> result = new Dictionary<Node, float>();

        getNodesAtDistance_rec(origin, 1, distance, result);

        return result;
    }

    private static void getNodesAtDistance_rec(Node origin, float incr, float distance, Dictionary<Node, float> result)
    {
        if (distance > 0)
        {
            foreach (Node neighbor in origin.Sons.Keys)
            {
                if (result.ContainsKey(neighbor))
                {
                    if (result[neighbor] > incr)
                        result[neighbor] = incr;
                }
                else
                    result.Add(neighbor, incr);
                float difference = 1;
                if (neighbor.damaged > 0)
                    difference += 1;
                if (neighbor.roads.Contains(origin))
                    difference /= 2;
                getNodesAtDistance_rec(neighbor, incr + difference, distance - difference, result);
            }
        }
    }

    // Corners methods
    //============
    public Corner Corner(Vector3 position)
    {
        position.y = 0;
        if (!FCorners.ContainsKey(position))
            FCorners.Add(position, new Corner(position));
        return FCorners[position];
    }

    public Dictionary<Vector3, Corner>.ValueCollection GetCorners()
    {
        return FCorners.Values;
    }

    public Corner GetCorner(Vector3 position)
    {
        position.y = 0;
        return FCorners[position];
    }

    public Corner GetCornerIfExists(Vector3 position)
    {
        position.y = 0;
        if (FCorners.ContainsKey(position))
            return FCorners[position];
        else
            return null;
    }

    public Vector3 GetRandomCorner()
    {
        Vector3[] keys = new Vector3[FCorners.Count];
        FCorners.Keys.CopyTo(keys, 0);

        return keys[UnityEngine.Random.Range(0, FCorners.Count)];
    }

    public void RemoveCornerIfDisconnected(Vector3 position)
    {
        position.y = 0;
        Corner corner = FCorners[position];
        if (corner.Nodes.Count == 0)
        {
            foreach (Corner neighbor in corner.Sons)
                neighbor.RemoveSon(corner);
            FCorners.Remove(position);
        }
    }

	public static float GetDamages(Dictionary<string, Node>.ValueCollection nodes)
	{
		float damages = 0;
		foreach (Node node in nodes)
		{
			damages += node.damaged;
		}
		return damages;
	}
}
