﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Generation
{
    private int FSize;
    private int FSite;

    public Generation(int size, int site)
    {
        FSize = size;
        FSite = site;
    }

    public void CreateRoads()
    {
        float[] elevationThresholds = { 0f, 0.05f, 0.37f };

        Queue<Node> openList = new Queue<Node>();
        Dictionary<Node, int> centerContour = new Dictionary<Node, int>();
        Dictionary<Corner, int> cornerContour = new Dictionary<Corner, int>();

        foreach (Node node in Graph.Instance().GetNodes())
        {
            if (node.preType == BiomePreType.PreOcean || node.preType == BiomePreType.PreWater)
            {
                centerContour[node] = 1;
                openList.Enqueue(node);
            }
        }

        while (openList.Count > 0)
        {
            Node actual = openList.Dequeue();
            foreach (Node neighbor in actual.Sons.Keys)
            {
                int newLevel = centerContour.ContainsKey(actual) ? centerContour[actual] : 0;
                while ((elevationThresholds.Length > newLevel && neighbor.height > elevationThresholds[newLevel]) && neighbor.preType != BiomePreType.PreWater)
                    newLevel += 1;
                if (newLevel < (centerContour.ContainsKey(neighbor) ? centerContour[neighbor] : 999))
                {
                    centerContour[neighbor] = newLevel;
                    openList.Enqueue(neighbor);
                }
            }
        }

        foreach (Node node in Graph.Instance().GetNodes())
        {
            foreach (Corner corner in node.Corners)
            {
                cornerContour[corner] = Math.Min(cornerContour.ContainsKey(corner) ? cornerContour[corner] : 999, centerContour.ContainsKey(node) ? centerContour[node] : 999);
            }
        }

        foreach (Node node in Graph.Instance().GetNodes())
        {
            foreach (Node neighbor in node.Sons.Keys)
            {
                Corner v0 = null;
                Corner v1 = null;


                // Absolutly aweful, has to be changed
                foreach (Corner corner in node.Corners)
                    if (corner.Nodes.Contains(node) && corner.Nodes.Contains(neighbor))
                        if (v0 == null)
                            v0 = corner;
                        else
                        {
                            v1 = corner;
                            break;
                        }

                if (cornerContour[v0] != cornerContour[v1])
                {
                    node.roads.Add(neighbor);
                    neighbor.roads.Add(node);
                }
            }
        }
    }

    public void AssignCities()
    {
        Dictionary<Node, double> weightDic = new Dictionary<Node, double>();
        Dictionary<Node, double> memoryWeightDic = new Dictionary<Node, double>();

        foreach (Node node in Graph.Instance().GetNodes())
            if (node.height > 0.01 && node.height < 0.6 && node.preType != BiomePreType.PreWater)
            {
                float waterLever = 0f;
                foreach (Node neighbor in node.Sons.Keys)
                {
                    if (neighbor.preType == BiomePreType.PreWater)
                        waterLever += 0.8f;
                    else if (neighbor.preType == BiomePreType.PreOcean)
                        waterLever += 0.3f;
                }

                foreach (Corner corner in node.Corners)
                {
                    if (corner.river > 0)
                        waterLever += corner.river / 4f;
                }

                double weight = ((0.1 / (node.height + 0.1)) / 2 + Math.Cos(node.moisture * Math.PI - Math.PI / 4)) / 2 + node.roads.Count + waterLever;
                weightDic.Add(node, weight);
                memoryWeightDic.Add(node, weight);
            }

        double sum = 0f;

        foreach (double weight in weightDic.Values)
            sum += weight;

        for (int i = 0; i < FSite / 8; i++)
        {
            Node result = null;
            double total = 0f;
            double rand = UnityEngine.Random.Range(0f, Convert.ToSingle(sum) + 1);
            foreach (Node node in weightDic.Keys)
            {
                total += weightDic[node];
                if (total >= rand)
                {
                    result = node;
                    break;
                }
            }

            if (result != null)
            {
                double weight = memoryWeightDic[result];

                result.citySize += weight * 4;
                weightDic[result] += weight * 4;
                sum += weight;

                foreach (Node neighbor in result.Sons.Keys)
                {
                    if (weightDic.ContainsKey(neighbor))
                    {
                        weightDic[neighbor] += weight * 2;
                        sum += weight * 2;
                    }
                }
            }
        }

        double maxSize = -1;
        foreach (Node node in Graph.Instance().GetNodes())
            if (node.citySize > maxSize)
                maxSize = node.citySize;

        foreach (Node node in Graph.Instance().GetNodes())
            node.citySize /= maxSize;
    }

    public void DisplayCities(UnityEngine.Object cityPrefab)
    {
        foreach (Node node in Graph.Instance().GetNodes())
        {
            if (node.citySize > 0)
            {
                Vector3 cityPosition = new Vector3(node.Position.x, 0.5f, node.Position.y);
                GameObject clone = GameObject.Instantiate(cityPrefab, cityPosition, Quaternion.identity) as GameObject;
                clone.transform.parent = GameObject.FindGameObjectWithTag("Cities").transform;
                clone.transform.localScale = new Vector3(clone.transform.localScale.x * Convert.ToSingle(node.citySize + 2), clone.transform.localScale.y * Convert.ToSingle(node.citySize + 2), clone.transform.localScale.z * Convert.ToSingle(node.citySize + 2));
            }
        }
    }

    public void AssignGroupements()
    {
        int position = 0;
        foreach (Node node in Graph.Instance().GetNodes())
        {
            if (node.height > 0 && node.preType != BiomePreType.PreWater)
            {
                int rand = UnityEngine.Random.Range(Convert.ToInt32(Math.Round(node.citySize * FSite / 10 + 2, 0)) / 2, Convert.ToInt32(Math.Round(node.citySize * FSite / 10 + 2, 0)));

                for (int i = 0; i < rand; i++)
                {
                    int quantity = Convert.ToInt32(UnityEngine.Random.Range(Convert.ToSingle(5 * node.citySize) + 50, Convert.ToSingle(10 * node.citySize) + 100));
                    Groupments.Instance().AddGroupment("Groupment " + position, node, quantity);

                    position += 1;
                }
            }
        }

        int total = 0;
        foreach (Groupment groupment in Groupments.Instance().GetGroupments())
            total += groupment.Quantity;
    }

    public void GroupCities()
    {
        foreach (Node node in Graph.Instance().GetNodes())
        {
            if (node.citySize > 0 && node.city == null)
            {
                node.city = GameObject.Instantiate(GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerController>().city).GetComponent<City>();
                node.city.nodes.Add(node);

                Queue<Node> openList = new Queue<Node>();
                openList.Enqueue(node);

                while (openList.Count != 0)
                {
                    Node actual = openList.Dequeue();
                    foreach (Node neighbor in actual.Sons.Keys)
                    {
                        if (neighbor.citySize > 0 && neighbor.city == null)
                        {
                            neighbor.city = node.city;
                            node.city.nodes.Add(neighbor);
                            openList.Enqueue(neighbor);
                        }
                    }
                }

                node.city.Initiate();
            }
        }
    }
}
