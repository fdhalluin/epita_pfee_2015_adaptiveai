﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class View
{
    public static Color damagedColor = Color.yellow;
    public static Color injuredColor = Color.red;

    static public Color colorFromBiome(Node node)
    {
        switch (node.type)
        {
            case BiomeType.Ocean:
                return new Color(54 / 255f, 54 / 255f, 97 / 255f);
            case BiomeType.Marsh:
                return new Color(140 / 255f, 212 / 255f, 201 / 255f);
            case BiomeType.Ice:
                return new Color(20 / 255f, 255 / 255f, 255 / 255f);
            case BiomeType.Lake:
                return new Color(85 / 255f, 125 / 255f, 166 / 255f);
            case BiomeType.Beach:
                return new Color(255 / 255f, 243 / 255f, 191 / 255f);
            case BiomeType.Snow:
                return new Color(248 / 255f, 248 / 255f, 248 / 255f);
            case BiomeType.Tundra:
                return new Color(231 / 255f, 255 / 255f, 231 / 255f);
            case BiomeType.Bare:
                return new Color(187 / 255f, 187 / 255f, 187 / 255f);
            case BiomeType.Scorched:
                return new Color(153 / 255f, 153 / 255f, 153 / 255f);
            case BiomeType.Taiga:
                return new Color(164 / 255f, 192 / 255f, 147 / 255f);
            case BiomeType.Shrubland:
                return new Color(120 / 255f, 211 / 255f, 110 / 255f);
            case BiomeType.TemperateDesert:
                return new Color(228 / 255f, 232 / 255f, 202 / 255f);
            case BiomeType.TemperateRainForest:
                return new Color(72 / 255f, 127 / 255f, 62 / 255f);
            case BiomeType.TemperateDeliduousForest:
                return new Color(82 / 255f, 137 / 255f, 72 / 255f);
            case BiomeType.Grassland:
                return new Color(110 / 255f, 211 / 255f, 110 / 255f);
            case BiomeType.TropicalRainForest:
                return new Color(102 / 255f, 157 / 255f, 92 / 255f);
            case BiomeType.TropicalSeasonalForest:
                return new Color(112 / 255f, 167 / 255f, 102 / 255f);
            case BiomeType.SubtropicalDesert:
                return new Color(233 / 255f, 221 / 255f, 199 / 255f);
            default:
                return new Color(0, 0, 0);
        }
    }

    static public Color colorFromMoisture(Node node)
    {
        return new Color((54 + 201 * Convert.ToSingle(1 - node.moisture)) / 255f, (54 + 201 * Convert.ToSingle(1 - node.moisture)) / 255f, (97 + 148 * Convert.ToSingle(1 - node.moisture)) / 255f);
    }

    static public Color colorFromDamages(Node node)
    {
        if (Biomes.isDamageable(node.type))
            return Color.Lerp(Color.yellow, colorFromBiome(node) * 0.25f, node.damaged > 0 ? 0.1f : 1.0f);
        else
            return colorFromBiome(node);
    }

    static public Color colorFromInjured(Node node)
    {
        if (Biomes.isDamageable(node.type))
            //return Color.Lerp(Color.Lerp(Color.white * 0.5f, Color.black, node.damaged), Color.red, Mathf.Lerp(0f, 1f, (float)Groupments.injuredPopulation(node.Groupments)/100f));
            return Color.Lerp(Color.Lerp(colorFromBiome(node), Color.black, 0.77f), Color.red, Mathf.Pow(Mathf.Lerp(0f, 1f, (float)Groupments.injuredPopulation(node.Groupments) / 100f), 0.5f));
        else
            return colorFromBiome(node);
	}

    static public Color colorFromMix(Node node)
    {
        var color = colorFromBiome(node);
        color = Color.Lerp(color, damagedColor, Mathf.Pow(Mathf.Clamp01(node.damaged), 0.2f));
        color = Color.Lerp(color, injuredColor, Mathf.Clamp01(Mathf.Pow(Groupments.injuredPopulation(node.Groupments) / 100f, 0.75f)));
        return color;
    }
}
