﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Text;

public class Corner
{
    //============
    // Variables
    //============

    private Vector3 FPosition;
    [NonSerialized]
    private List<Node> FNodes;
    [NonSerialized]
    private List<Corner> FSons;

    public BiomePreType type;
    public float height;

    [NonSerialized]
    public Corner downslope;
    public int river;
    public double moisture;

    #region Getter / Setter
    //============
    // Se/Getter
    //============
    public List<Corner> Sons
    {
        get { return FSons; }
    }

    public Vector3 Position
    {
        get { return FPosition; }
    }

    public List<Node> Nodes
    {
        get { return FNodes; }
    }
    #endregion

    public Corner(Vector3 position)
    {
        FPosition = position;
        FNodes = new List<Node>();
        FSons = new List<Corner>();
        
        type = BiomePreType.None;
        height = 0.0f;

        downslope = null;
        river = 0;
    }

    public void AddNode(Node node)
    {
        if (!FNodes.Contains(node))
            FNodes.Add(node);
    }

    public void AddSon(Corner son)
    {
        if (!FSons.Contains(son))
            FSons.Add(son);
    }

    public int GetSizeNodes()
    {
        return FNodes.Count;
    }

    public void SetNodesMajorType()
    {
        int ocean = 0;
        int land = 0;
        int coast = 0;
        int water = 0;

        foreach (Node node in FNodes)
        {
            switch (node.preType)
            {
                case BiomePreType.PreCoast:
                    coast += 1;
                    break;
                case BiomePreType.PreLand:
                    land += 1;
                    break;
                case BiomePreType.PreOcean:
                    ocean += 1;
                    break;
                case BiomePreType.PreWater:
                    water += 1;
                    break;
                default:
                    Debug.Log("Undefined type node");
                    break;
            }
        }

        if (ocean != 0 && land != 0)
            type = BiomePreType.PreCoast;
        else if (ocean != 0 && land == 0)
            type = BiomePreType.PreOcean;
        else if (water > land)
            type = BiomePreType.PreWater;
        else if (land != 0 && ocean == 0)
            type = BiomePreType.PreLand;
        else if (coast != 0)
            type = BiomePreType.PreCoast;
        else
        {
            Debug.Log("Wrong type for corner (Total: " + FNodes.Count + ", ocean: " + ocean + ", land: " + land + ", water: " + water + ", coast: " + coast + ")");
            type = BiomePreType.None;
        }
    }

    public void RemoveSon(Corner corner)
    {
        if (FSons.Contains(corner))
            FSons.Remove(corner);
    }

    public void RemoveNode(Node node)
    {
        if (FNodes.Contains(node))
            FNodes.Remove(node);
    }

    public void AssignHeight(float value)
    {
        FPosition.y = height * value;
    }

    public void DisableHeight()
    {
        FPosition.y = 0;
    }
}

[Serializable]
public class Node
{
    //============
    // Variables
    //============

    [NonSerialized]
    private Tile FTile;

    [NonSerialized]
    private Dictionary<Node, float> FSons;
    [NonSerialized]
    private List<Corner> FCorners;
    private Vector2 FPosition;


    // Biomes stuff
    //============
    public BiomePreType preType; // deprecated once the map is built
    public BiomeType type;
    public float height;
    public double moisture;
    public double citySize;
    public float damaged;
    [NonSerialized]
    public List<Node> roads;

    public City city;
    public Suburb suburb;
    public List<Hospital> hospitals;
	public List<Depot> depots;

    private List<EngineerTeam> FIncomingAndActingEngineerTeams;
    private List<MedicalTeam> FIncomingAndActingMedicalTeams;

    private List<Groupment> FGroupments;

    private bool FUpdateMap;

    #region Getter / Setter
    //============
    // Se/Getter
    //============

    public Dictionary<Node, float> Sons
    {
        get { return FSons; }
    }

    public List<Corner> Corners
    {
        get { return FCorners; }
    }

    public Tile Tile
    {
        get { return FTile; }
    }

    public Vector2 Position
    {
        get { return FPosition; }
    }

    public List<Groupment> Groupments
    {
        get { return FGroupments; }
    }

    public int IncomingAndActingEngineerTeamsCount
    {
        get { return FIncomingAndActingEngineerTeams.Count; }
    }

    public int IncomingAndActingMedicalTeamsCount
    {
        get { return FIncomingAndActingMedicalTeams.Count; }
    }
    #endregion

    public float RoundedHeight
    {
        get { return (float)System.Math.Round(height, 4); }
    }

    public float RoundedMoisture
    {
        get { return (float)System.Math.Round(moisture, 2); }
    }

    //============
    // Des/Constructor
    //============

    public Node(Tile tile)
    {
        FTile = tile;
        FPosition = new Vector2(tile.transform.position.x, tile.transform.position.z);
        FSons = new Dictionary<Node, float>();
        FCorners = new List<Corner>();

        this.preType = BiomePreType.None;
        this.type = BiomeType.None;
        this.height = 0.0f;
        this.moisture = 0.0f;
        this.citySize = 0.0f;

        this.FGroupments = new List<Groupment>();

        this.roads = new List<Node>();

        this.FUpdateMap = false;

        FIncomingAndActingEngineerTeams = new List<EngineerTeam>();
        FIncomingAndActingMedicalTeams = new List<MedicalTeam>();
    }

    //============
    // Other methods
    //============

    public void AddSon(Node node)
    {
        if (Graph.Instance().Exists(node.Tile.name) && FTile.name != node.Tile.name)
        {
            FSons.Add(node, (FPosition - node.Position).magnitude);
        }
    }

    public void AddSon(Tile tile)
    {
        if (Graph.Instance().Exists(tile.name) && FTile.name != tile.name)
        {
            FSons.Add(tile.node, (FPosition - tile.node.Position).magnitude);
        }
    }

    public Corner AddCorner(Vector3 position)
    {
        Corner corner = Graph.Instance().Corner(position);
        if (!FCorners.Contains(corner))
        {
            FCorners.Add(corner);
            corner.AddNode(this);
        }

        return corner;
    }

    public void RemoveSon(Node node)
    {
        if (FSons.ContainsKey(node))
        {
            FSons.Remove(node);
        }
    }

    public Node RandomSon()
    {
        Node[] keys = new Node[FSons.Count];
        FSons.Keys.CopyTo(keys, 0);

        return keys[UnityEngine.Random.Range(0, FSons.Count)];
    }

    public void SetPreTypeResult()
    {
        int ocean = 0;
        int land = 0;
        int water = 0;
        int coast = 0;

        foreach (Corner corner in FCorners)
        {
            switch (corner.type)
            {
                case BiomePreType.PreOcean:
                    ocean += 1;
                    break;
                case BiomePreType.PreLand:
                    land += 1;
                    break;
                case BiomePreType.PreWater:
                    water += 1;
                    break;
                case BiomePreType.PreCoast:
                    break;
                default:
                    Debug.Log("Undefined type corner");
                    break;
            }
        }

        if (ocean != 0 && land != 0)
            preType = BiomePreType.PreCoast;
        else if (ocean != 0 && land == 0)
            preType = BiomePreType.PreOcean;
        else if (water > land / 2)
            preType = BiomePreType.PreWater;
        else if (land != 0 && ocean == 0)
            preType = BiomePreType.PreLand;
        else
            Debug.Log("Wrong type for " + FTile.name + " (Total: " + FCorners.Count + ", ocean: " + ocean + ", land: " + land + ", water: " + water + ", coast: " + coast + ")");
    }

    public void AddGroupment(Groupment groupment)
    {
        FGroupments.Add(groupment);
    }

    public void InitializeDamage(float state)
    {
        float cityFactor = (float)citySize * 10f;

        float rand = UnityEngine.Random.Range(cityFactor / 2, cityFactor + 0.2f);

        if (rand > 1)
            rand = 1;
        else if (rand < 0)
            rand = 0;

        damaged = rand * state;

        // Test to make all tiles damaged
        //damaged = 1;
    }

    public SimpleBiomeType GetSimpleType()
    {
        if (type == BiomeType.Ocean)
            return SimpleBiomeType.Ocean;
        else if (type == BiomeType.Ice)
            return SimpleBiomeType.Ice;
        else if (type == BiomeType.Lake || type == BiomeType.Marsh)
            return SimpleBiomeType.Lake;
        else if (type == BiomeType.Beach)
            return SimpleBiomeType.Beach;
        else if (type == BiomeType.Snow)
            return SimpleBiomeType.Snow;
        else if (type == BiomeType.SubtropicalDesert ||type == BiomeType.TemperateDesert
            || type == BiomeType.Tundra || type == BiomeType.Scorched)
            return SimpleBiomeType.Desert;
        else if (type == BiomeType.TemperateRainForest || type == BiomeType.TemperateDeliduousForest
            || type == BiomeType.TropicalRainForest || type == BiomeType.TropicalSeasonalForest
            || type == BiomeType.Taiga)
            return SimpleBiomeType.Forest;
        else if (type == BiomeType.Grassland ||type == BiomeType.Bare || type == BiomeType.Shrubland)
            return SimpleBiomeType.Grassland;

        return SimpleBiomeType.None;
    }

    public void AddIncomingOrActingTeam(EngineerTeam team)
    {
        if (!FIncomingAndActingEngineerTeams.Contains(team))
            FIncomingAndActingEngineerTeams.Add(team);
    }

    public void RemoveIncomingOrActingTeam(EngineerTeam team)
    {
        if (FIncomingAndActingEngineerTeams.Contains(team))
            FIncomingAndActingEngineerTeams.Remove(team);
    }

    public void AddIncomingOrActingTeam(MedicalTeam team)
    {
        if (!FIncomingAndActingMedicalTeams.Contains(team))
            FIncomingAndActingMedicalTeams.Add(team);
    }

    public void RemoveIncomingOrActingTeam(MedicalTeam team)
    {
        if (FIncomingAndActingMedicalTeams.Contains(team))
            FIncomingAndActingMedicalTeams.Remove(team);
    }

    public void ActivateUpdateMap()
    {
        FUpdateMap = true;
    }

    public bool GetUpdateMap()
    {
        bool tmp = FUpdateMap;
        FUpdateMap = false;
        return tmp;
    }
}

public class Tile : MonoBehaviour
{
    public Node node;
    public Material material;
    public Material selectedMaterial;
    public Material selectedCityMaterial;
    public Mesh mesh;
	public bool hasHospital;
	public bool hasDepot;

    private string FLastTag;

    public void Initiate()
    {
        node = Graph.Instance().AddNode(this);
		hasHospital = false;
		hasDepot = false;
    }

    public void AddCorners(Voronoi.Cell cell)
    {
        Queue<Voronoi.HalfEdge> queue = new Queue<Voronoi.HalfEdge>();
        while (queue.Count < cell.halfEdges.Count)
        {
            float maxAngle = float.NegativeInfinity;
            Voronoi.HalfEdge actual = null;
            foreach (Voronoi.HalfEdge halfEdge in cell.halfEdges)
            {
                if (halfEdge.angle > maxAngle && !queue.Contains(halfEdge))
                {
                    maxAngle = halfEdge.angle;
                    actual = halfEdge;
                }
            }

            queue.Enqueue(actual);
        }

        while (queue.Count != 0)
        {
            Voronoi.HalfEdge halfEdge = queue.Dequeue();

            Vector3 vectorA = new Vector3(Convert.ToSingle(Math.Round(halfEdge.edge.va.ToVector3().x, 2)), 0, Convert.ToSingle(Math.Round(halfEdge.edge.va.ToVector3().z, 2)));
            Vector3 vectorB = new Vector3(Convert.ToSingle(Math.Round(halfEdge.edge.vb.ToVector3().x, 2)), 0, Convert.ToSingle(Math.Round(halfEdge.edge.vb.ToVector3().z, 2)));

            Corner ca = node.AddCorner(vectorA);
            Corner cb = node.AddCorner(vectorB);
            ca.AddSon(cb);
            cb.AddSon(ca);
        }
    }

    public void CreateFanMesh(Voronoi.Cell cell)
    {
        if (cell.halfEdges.Count > 0)
        {
            this.GetComponent<MeshFilter>().sharedMesh = this.mesh = new Mesh();
            this.name = "Tile " + cell.site.id;

            Vector3[] vertices = new Vector3[cell.halfEdges.Count + 1];
            int[] triangles = new int[(cell.halfEdges.Count + 0) * 3];

            vertices[0] = cell.site.ToVector3() - this.transform.position;
            triangles[0] = 0;
            for (int v = 1, t = 1; v < vertices.Length; v++, t += 3)
            {
                vertices[v] = cell.halfEdges[v - 1].GetStartPoint().ToVector3() - this.transform.position;
                triangles[t] = v;
                triangles[t + 1] = v + 1;
            }
            triangles[triangles.Length - 1] = 1;

            this.mesh.vertices = vertices;
            this.mesh.triangles = triangles;
            this.mesh.RecalculateBounds();
            this.mesh.RecalculateNormals();

            this.GetComponent<MeshCollider>().sharedMesh = this.mesh;
        }
    }
    
    void Update()
    {
        if (gameObject.tag != FLastTag)
        {
            if (gameObject.tag == "Selected" || gameObject.tag == "SelectedHospital" || gameObject.tag == "SelectedDepot")
                GetComponent<Renderer>().sharedMaterial = selectedMaterial;
            else if (gameObject.tag == "SelectedCity")
                GetComponent<Renderer>().sharedMaterial = selectedCityMaterial;
            else
                GetComponent<Renderer>().sharedMaterial = material;
            FLastTag = gameObject.tag;
        }

        foreach (Groupment groupment in node.Groupments)
        {
            groupment.Update();
        }
    }

    void FixedUpdate()
    {
        if (node.GetUpdateMap())
        {
            Mode mode = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerController>().FMode;
            if (mode == Mode.Damages)
                AssignNewColor(View.colorFromDamages(node));
            else if (mode == Mode.Injured)
                AssignNewColor(View.colorFromInjured(node));
        }
    }

    void OnDestroy()
    {
        foreach (Corner corner in node.Corners)
        {
            corner.RemoveNode(node);
            Graph.Instance().RemoveCornerIfDisconnected(corner.Position);
        }
        foreach (Groupment groupment in node.Groupments)
            Groupments.Instance().RemoveGroupment(groupment);

        Graph.Instance().RemoveNode(node);
        node = null;
    }

    public void AssignNewColor(Color color)
    {
        material.color = color;
        selectedMaterial.color = color;

        FLastTag = null;
    }
}
