﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndScreen : MonoBehaviour {

	private int FSeed;
	private int FEventSeed;

	private InputField inputSeed;
	private InputField inputEventSeed;
	private Text scoreValue;
	private Text damageValue;
	private Text injuredValue;

	public GameObject canvas;
	public GameObject optionCanvas;
	public MedicalIA medicalIAChosen;
	public EngineerIA engineerIAChosen;


	// Use this for initialization
	void Start() {
		StartScreen startScreen = GameObject.Find("StartScreen").GetComponent<StartScreen>();
		FSeed = startScreen.GetSeed();
		FEventSeed = startScreen.GetEventSeed();
		inputSeed = GameObject.Find("MapSeed").GetComponent<InputField>();
		inputSeed.text = FSeed.ToString();
		inputEventSeed = GameObject.Find("EventSeed").GetComponent<InputField>();
		inputEventSeed.text = FEventSeed.ToString();
		scoreValue = GameObject.Find("ScoreValue").GetComponent<Text>();
		scoreValue.text = startScreen.score.ToString();
		damageValue = GameObject.Find("DamageValue").GetComponent<Text>();
		damageValue.text = startScreen.percentDamage.ToString()+"%";
		injuredValue = GameObject.Find("InjuredValue").GetComponent<Text>();
		injuredValue.text = startScreen.percentInjured.ToString()+"%";
		medicalIAChosen = startScreen.medicalIAChosen;
		engineerIAChosen = startScreen.engineerIAChosen;
	}
	
	// Update is called once per frame
	void Update() {
		if (Input.GetKeyDown(KeyCode.Return))
		{
			OnClick();
		}
		else if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (Application.isEditor)
				Debug.Log("Kikoo");
			else
				Application.Quit();
		}
	}

	public void EndEdit()
	{
		Text buttonText = GameObject.Find("RestartButton/Text").GetComponent<Text>();
		buttonText.text = "Play";
		//inputSeed = GameObject.Find("MapSeed").GetComponent<InputField>();
		if (!int.TryParse(inputSeed.text, out FSeed))
		{
			FSeed = 0;
		}
		else if (GetSeed() == 1)
			SetSeed(11);
		//inputEventSeed = GameObject.Find("EventSeed").GetComponent<InputField>();
		if (!int.TryParse(inputEventSeed.text, out FEventSeed))
		{
			FEventSeed = 0;
		}
	}

	public void OnClick()
	{
		StartScreen startScreen = GameObject.Find ("StartScreen").GetComponent<StartScreen>();
		startScreen.SetSeed(FSeed);
		startScreen.SetEventSeed(FEventSeed);
		Application.LoadLevel("Main");
	}

	public void OptionOnClick()
	{
		canvas.SetActiveRecursively(false);
		optionCanvas.SetActiveRecursively(true);
	}

	public void back()
	{
		canvas.SetActiveRecursively(true);
		optionCanvas.SetActiveRecursively(false);
	}
	
	public int GetSeed()
	{
		return FSeed;
	}
	
	public void SetSeed(int specifiedSeed)
	{
		FSeed = specifiedSeed;
	}
	
	public int GetEventSeed()
	{
		return FEventSeed;
	}
	
	public void SetEventSeed(int specifiedSeed)
	{
		FEventSeed = specifiedSeed;
	}

	public void basicMedicalIA()
	{
		medicalIAChosen = MedicalIA.Basic;
	}
	
	public void adaptativeMedicalIA()
	{
		medicalIAChosen = MedicalIA.Adaptative;
	}
	
	public void basicEngineerIA()
	{
		engineerIAChosen = EngineerIA.Basic;
	}
	
	public void adaptativeEngineerIA()
	{
		engineerIAChosen = EngineerIA.Adaptative;
	}
}
