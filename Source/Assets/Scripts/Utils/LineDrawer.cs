﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

public class LineDrawer : MonoBehaviour
{
    private struct LineCorner
    {
        public Corner start;
        public Corner end;
    };

    private struct LineVector3
    {
        public Vector3 start;
        public Vector3 end;
    };

    public Color edgeColor;
    public Color selectedEdgeColor;
    public Color pathColor;
    public Color riverColor;

    public Material mat;
    public bool edgeDisplayType;

    private GameObject FLastSelectedObject;
    private List<Vector3> FActualSelection;
    private List<LineCorner> FEdges;
    private List<LineCorner> FRivers;
    private List<Node> FRoadsStart;
    private List<Node> FRoadsEnd;
	private List<LineVector3> FRoads;
    private List<LineVector3> FAdditionalEdges;

    void Start()
    {
        FActualSelection = new List<Vector3>();
    }

    public void Initiate()
    {
        FEdges = new List<LineCorner>();
        FRivers = new List<LineCorner>();
        FRoadsStart = new List<Node>();
        FRoadsEnd = new List<Node>();
        FAdditionalEdges = new List<LineVector3>();

        edgeDisplayType = false;

        RecalculateEdges();
        RecalculateRivers();
        RecalculateRoads();
    }

    void Awake()
    {
        mat = new Material(Shader.Find("Sprites/Default"));

        edgeColor = new Color(0, 0, 0, 0.5f);
        selectedEdgeColor = new Color(1, 1, 1, 1);
        pathColor = new Color(0.2f, 0.2f, 0.2f, 1);
        riverColor = new Color(0, 0, 1, 1);
    }

    public void RecalculateEdges()
    {
        foreach (Corner corner in Graph.Instance().GetCorners())
        {
            foreach (Corner neighbor in corner.Sons)
            {
                if (!ContainsPair(FEdges, corner, neighbor))
                {
                    LineCorner tmp_line = new LineCorner();
                    tmp_line.start = corner;
                    tmp_line.end = neighbor;
                    FEdges.Add(tmp_line);
                }
            }
        }
    }

    public void RecalculateRivers()
    {
        foreach (Corner corner in Graph.Instance().GetCorners())
        {
            if (corner.river > 0 && corner.downslope != corner)
            {
                if (!ContainsPair(FRivers, corner, corner.downslope))
                {
                    LineCorner tmp_line = new LineCorner();
                    tmp_line.start = corner;
                    tmp_line.end = corner.downslope;
                    FRivers.Add(tmp_line);
                }
            }
        }
    }

    public void RecalculateRoads()
    {
        foreach (Node node in Graph.Instance().GetNodes())
        {
            foreach (Node road in node.roads)
            {
                int i = 0;
                for (; i < FRoadsStart.Count; i++)
                {
                    if (FRoadsStart[i] == road || FRoadsStart[i] == node)
                        if (FRoadsEnd[i] == node || FRoadsEnd[i] == road)
                            break;
                }

                if (i == FRoadsStart.Count)
                {
                    FRoadsStart.Add(node);
                    FRoadsEnd.Add(road);
                }
            }
        }

		//if (edgeDisplayType == true && FRoads == null)
		//{
		//	FRoads = new List<LineVector3>();
		//	for (int i = 0; i < FRoadsStart.Count; i++)
		//	{
		//		List<Corner> commonCorners = FRoadsStart[i].Corners.Intersect(FRoadsEnd[i].Corners).ToList();
		//		Vector3 commonCornersMiddle = new Vector3(0, 0, 0);
		//		foreach (Corner corner in commonCorners)
		//			commonCornersMiddle += corner.Position;
		//		commonCornersMiddle /= commonCorners.Count();

		//		LineVector3 halfRoad1 = new LineVector3();
		//		halfRoad1.start = FRoadsStart[i].Tile.transform.position;
		//		halfRoad1.end = commonCornersMiddle;
		//		FRoads.Add(halfRoad1);

		//		LineVector3 halfRoad2 = new LineVector3();
		//		halfRoad2.start = commonCornersMiddle;
		//		halfRoad2.end = FRoadsEnd[i].Tile.transform.position;
		//		FRoads.Add (halfRoad2);
		//	}
		//}
    }

    public void RecalculateAdditionalEdgesSecond()
    {
        if (FAdditionalEdges.Count == 0)
        {
            foreach (Node node in Graph.Instance().GetNodes())
            {
                int numberCorners = node.Tile.mesh.vertexCount / 2;

                for (int i = 1; i < numberCorners + 1; i++)
                {
                    LineVector3 tmp = new LineVector3();
                    tmp.start = node.Tile.mesh.vertices[i] + node.Tile.transform.position;
                    tmp.end = node.Tile.mesh.vertices[i + numberCorners] + node.Tile.transform.position;
                    FAdditionalEdges.Add(tmp);
                }

                for (int i = 1; i < numberCorners + 1; i++)
                {
                    LineVector3 tmp = new LineVector3();
                    tmp.start = node.Tile.mesh.vertices[i] + node.Tile.transform.position;
                    tmp.end = node.Tile.mesh.vertices[i % numberCorners + 1] + node.Tile.transform.position;
                    FAdditionalEdges.Add(tmp);
                }
            }
        }
    }

    public void RecalculateAdditionalEdgesThird()
    {
        if (FAdditionalEdges.Count == 0)
        {
            foreach (Node node in Graph.Instance().GetNodes())
            {
                int numberCorners = node.Tile.mesh.vertexCount / 2;

                for (int i = numberCorners + 1; i < node.Tile.mesh.vertexCount; i++)
                {
                    LineVector3 tmp = new LineVector3();
                    tmp.start = node.Tile.mesh.vertices[i] + node.Tile.transform.position;
                    tmp.end = node.Tile.mesh.vertices[i % numberCorners + numberCorners + 1] + node.Tile.transform.position;
                    FAdditionalEdges.Add(tmp);
                }
            }
        }
    }

    private bool ContainsPair(List<LineCorner> list, Corner first, Corner second)
    {
        foreach (LineCorner line in list)
        {
            if (line.start == first)
            {
                if (line.end == second)
                    return true;
            }
            else if (line.end == first)
            {
                if (line.start == second)
                    return true;
            }
        }

        return false;
    }

    public void DrawSelectedEdge()
    {
        GameObject selectedObject = GameObject.FindGameObjectWithTag("Selected");
        if (selectedObject == null)
            selectedObject = GameObject.FindGameObjectWithTag("SelectedCity");
        if (selectedObject == null)
            selectedObject = GameObject.FindGameObjectWithTag("SelectedHospital");
        if (selectedObject == null)
            selectedObject = GameObject.FindGameObjectWithTag("SelectedDepot");
        if (selectedObject != null && selectedObject != FLastSelectedObject)
        {
            FLastSelectedObject = selectedObject;

            Tile tile = selectedObject.GetComponent<Tile>();

            FActualSelection.Clear();
            if (edgeDisplayType == true)
            {
                for (int i = 1; i < tile.mesh.vertexCount / 2 + 1; i++)
                    FActualSelection.Add(tile.mesh.vertices[i] + tile.transform.position);
            }
            else
            {
                for (int i = 1; i < tile.mesh.vertexCount; i++)
                    FActualSelection.Add(tile.mesh.vertices[i] + tile.transform.position);
            }
        }
        else if (selectedObject == null)
            FActualSelection.Clear();

        if (FActualSelection != null)
		{
			GL.Color(selectedEdgeColor);
            for (int i = 0; i < FActualSelection.Count; i++)
                DrawLine(FActualSelection[i], FActualSelection[(i + 1) % FActualSelection.Count]);
		}
    }

    public void DrawRoads()
    {
		if (edgeDisplayType == true)
		{
			if (FRoads == null)
				RecalculateRoads();
			DrawLines(FRoads, pathColor);
		}
		else
		{
			GL.Color(pathColor);
	        for (int i = 0; i < FRoadsStart.Count; i++)
	            DrawLine(FRoadsStart[i].Tile.transform.position, FRoadsEnd[i].Tile.transform.position);
		}
    }

	private void DrawLine(Vector3 startPoint, Vector3 endPoint)
	{
		startPoint.y += 0.01f;
		endPoint.y += 0.01f;
		GL.Vertex3(startPoint.x, startPoint.y, startPoint.z);
		GL.Vertex3(endPoint.x, endPoint.y, endPoint.z);
	}

    private void DrawLine(Vector3 startPoint, Vector3 endPoint, Color color)
    {
        startPoint.y += 0.01f;
        endPoint.y += 0.01f;
        GL.Color(color);
        GL.Vertex3(startPoint.x, startPoint.y, startPoint.z);
        GL.Vertex3(endPoint.x, endPoint.y, endPoint.z);
    }

    private void DrawLines(List<LineCorner> lines, Color color)
    {
        GL.Color(color);
        foreach (LineCorner line in lines)
        {
            GL.Vertex3(line.start.Position.x, line.start.Position.y + 0.01f, line.start.Position.z);
            GL.Vertex3(line.end.Position.x, line.end.Position.y + 0.01f, line.end.Position.z);
        }
    }

    private void DrawLines(List<LineVector3> lines, Color color)
    {
        GL.Color(color);
        foreach (LineVector3 line in lines)
        {
            GL.Vertex3(line.start.x, line.start.y + 0.01f, line.start.z);
            GL.Vertex3(line.end.x, line.end.y + 0.01f, line.end.z);
        }
    }

    void OnPostRender()
    {
        GL.Begin(GL.LINES);
        mat.SetPass(0);

        if (!edgeDisplayType)
            DrawLines(FEdges, edgeColor);
        else
            DrawLines(FAdditionalEdges, edgeColor);
        DrawLines(FRivers, riverColor);
        DrawSelectedEdge();
        DrawRoads();

        GL.End();
    }
}
