﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InfoHospitalDisplay : MonoBehaviour
{
    private string vehicles = "";

    Text VehiclesHospital;

    void Start()
    {
        VehiclesHospital = GameObject.Find("VehiclesHospital").GetComponent<Text>();
    }

    void Update()
    {
        GameObject selectedHospital = GameObject.FindGameObjectWithTag("SelectedHospital");
        if (selectedHospital != null)
        {
            gameObject.GetComponent<Canvas>().enabled = true;

            Hospital hospital = selectedHospital.GetComponent<Tile>().node.hospitals[0];
            vehicles = hospital.getTeamCount().ToString();

            ChangeDepotDisplay();
        }
        else
            gameObject.GetComponent<Canvas>().enabled = false;
    }

    private void ChangeDepotDisplay()
    {
        VehiclesHospital.text = "Vehicles: " + vehicles;
    }
}
