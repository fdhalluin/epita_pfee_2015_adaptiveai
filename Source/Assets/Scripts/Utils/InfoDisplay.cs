﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoDisplay : MonoBehaviour
{
    public GameObject hospitalPrefab;
	public GameObject depotPrefab;
	public GameObject injuredProgress;
	public GameObject roadDamageProgress;

    public int maxHeight = 1000;
	public int score;
	public bool isStarted = false;

	public float injuredScale = 0;
	public float damageScale = 0;
	public bool injuredThreshold = false;
	public bool damageThreshold = false;

	public MedicalIA medicalIA;
	public EngineerIA engineerIA;

	private PlayerController controller;
    private string height, moisture, type, population, damaged, injured;
	Text Height, Moisture, Type, Population, Damaged, Injured, currentFunds, ScoreValue;

    void Start()
    {
		score = 0;
        Height = GameObject.Find("Height").GetComponent<Text>();
        Moisture = GameObject.Find("Moisture").GetComponent<Text>();
        Type = GameObject.Find("Type").GetComponent<Text>();
        Population = GameObject.Find("Population").GetComponent<Text>();
        Damaged = GameObject.Find("Damaged").GetComponent<Text>();
        Injured = GameObject.Find("Injured").GetComponent<Text>();
        currentFunds = GameObject.Find("FundsText/Number").GetComponent<Text>();
		ScoreValue = GameObject.Find("ScoreValue").GetComponent<Text>(); 

		medicalIA = GameObject.Find("StartScreen").GetComponent<StartScreen>().medicalIAChosen;
		engineerIA = GameObject.Find("StartScreen").GetComponent<StartScreen>().engineerIAChosen;

		controller = GameObject.FindWithTag("MainCamera").GetComponent<PlayerController>();
    }

    void Update()
    {
        GameObject selected = GameObject.FindGameObjectWithTag("Selected");
        GameObject selectedCity = GameObject.FindGameObjectWithTag("SelectedCity");
        GameObject selectedHospital = GameObject.FindGameObjectWithTag("SelectedHospital");
        GameObject selectedDepot = GameObject.FindGameObjectWithTag("SelectedDepot");

        if (selected || selectedCity || selectedHospital || selectedDepot)
        {
            if (selected == null)
            {
                if (selectedCity != null)
                    selected = selectedCity;
                else if (selectedHospital != null)
                    selected = selectedHospital;
                else if (selectedDepot != null)
                    selected = selectedDepot;
            }

            gameObject.GetComponent<Canvas>().enabled = true;

            Tile actual = selected.GetComponent<Tile>();
            height = System.Math.Round(actual.node.height * maxHeight).ToString();
            moisture = System.Math.Round(actual.node.moisture * 100).ToString();
            type = actual.node.GetSimpleType().ToString();

            int intPopulation = 0;
            foreach (Groupment groupment in actual.node.Groupments)
                intPopulation += groupment.Quantity;
            population = intPopulation.ToString();

            if (actual.node.damaged > 0)
                damaged = "D";
            else
                damaged = "Not d";
            damaged += "amaged";

            if (actual.node.damaged > 0)
                damaged += " (" + System.Math.Round(actual.node.damaged * 100) + "%)";

            int intInjured = 0;
            foreach (Groupment groupment in actual.node.Groupments)
                intInjured += groupment.Injured;
            injured = intInjured.ToString();

            ChangeDisplay();
        }
        else
            gameObject.GetComponent<Canvas>().enabled = false;

		progressDisplay();

        currentFunds.text = controller.funds.ToString();
	}

	private void progressDisplay()
	{
		Dictionary<string, Groupment>.ValueCollection population = Groupments.Instance().GetGroupments();
		int injured = Groupments.injuredPopulation(population);
		int totalInjured = Groupments.Instance().totalInjured;
		//float injuredScale = 0;
		if (totalInjured != 0)
			injuredScale = 1 - (float) injured / totalInjured;

		injuredProgress.transform.localScale = new Vector3(injuredScale, 1f, 1f);

		float damages = Graph.GetDamages(Graph.Instance().GetNodes());
		float totalDamages = Graph.Instance().totalDamages;
		//float damageScale = 0;
		if (totalDamages != 0)
			damageScale = 1 - damages / totalDamages;
		roadDamageProgress.transform.localScale = new Vector3(damageScale, 1f, 1f);
		if (!damageThreshold && damageScale >= 0.4f)
		{
			controller.funds += 50000;
			damageThreshold = true;
		}
		if (!injuredThreshold && injuredScale >= 0.4f)
		{
			controller.funds += 50000;
			injuredThreshold = true;
		}
		score = CalculateScore (injuredScale, damageScale);
		ScoreValue.text = score.ToString();
	}

	public static int CalculateScore(float percentHealed, float percentRepared)
	{
		float minScore = 0f;
		float maxScore = 10000;
		float percentRestablishment = (percentHealed * 2 + percentRepared) / 3;
		return (int)Mathf.Lerp(minScore, maxScore, percentRestablishment);

	}

    private void ChangeDisplay()
    {
        Type.text = type;
        Height.text = "Elevation: " + height + "m";
        Moisture.text = "Humidity: " + moisture + "%";
        Population.text = "Population: " + population;
        Damaged.text = damaged;
        Injured.text = "Injured: " + injured;
    }

	private bool prePhaseIsFinished()
	{
		return isStarted;//controller.Funds == 0 && isPrephase);
	}

    public void PutHospital()
    {
        GameObject selected = GameObject.FindGameObjectWithTag("Selected");
        if (selected)
        {
            Tile actual = selected.GetComponent<Tile>();
			BiomeType biome = actual.node.type;
			if (!actual.hasHospital && Biomes.isConstructible (biome) && !actual.hasDepot && controller.funds >= 50000)
			{
	            Vector3 hospitalPosition = new Vector3(actual.node.Position.x, -1.0f, actual.node.Position.y);

	            GameObject clone = Instantiate(hospitalPrefab, hospitalPosition, Quaternion.identity) as GameObject;
	            clone.name = "Hospital " + actual.name;
	            clone.transform.parent = actual.transform;
				clone.transform.position = actual.transform.position;

	            Hospital newHospital = clone.GetComponent<Hospital>();

	            newHospital.location = actual.node;
				newHospital.Initiate(medicalIA);

	            if (actual.node.hospitals == null)
	                actual.node.hospitals = new List<Hospital>();
	            actual.node.hospitals.Add(newHospital);
	            newHospital.location = actual.node;
                //buildingsLeft -= 1;
				actual.hasHospital = true;
                //if (prePhaseIsFinished())
                //{
                //	PlayerController.StartRescue();
                //	isPrephase = false;
                //}
                controller.funds -= 50000;
                actual.tag = "SelectedHospital";
            }
        }
        currentFunds.text = controller.funds.ToString();
    }

    public void PutDepot()
	{
		GameObject selected = GameObject.FindGameObjectWithTag("Selected");
		if (selected)
		{
			Tile actual = selected.GetComponent<Tile> ();
			BiomeType biome = actual.node.type;
			if (!actual.hasDepot && Biomes.isConstructible(biome) && !actual.hasHospital && controller.funds >= 50000)
			{
				Vector3 depotPosition = new Vector3(actual.node.Position.x, 0.0f, actual.node.Position.y);
			
				GameObject clone = Instantiate(depotPrefab, depotPosition, Quaternion.identity) as GameObject;
				clone.name = "Depot " + actual.name;
				clone.transform.parent = actual.transform;
				clone.transform.position = actual.transform.position;
			
				Depot newDepot = clone.GetComponent<Depot>();
			
				newDepot.location = actual.node;
				newDepot.Initiate(engineerIA);
			
				if (actual.node.depots == null)
					actual.node.depots = new List<Depot>();
				actual.node.depots.Add (newDepot);
				newDepot.location = actual.node;
				//buildingsLeft -= 1;
				actual.hasDepot = true;
                //if (prePhaseIsFinished())
                //{
                //	PlayerController.StartRescue();
                //	isPrephase = false;
                //}
                controller.funds -= 50000;
                actual.tag = "SelectedDepot";
            }
		}
        currentFunds.text = controller.funds.ToString();
    }

    public void AddVehicleToBuilding()
    {
        GameObject selected ;
        if ((selected = GameObject.FindGameObjectWithTag("SelectedHospital")) != null)
        {
            Tile tile = selected.GetComponent<Tile>();
            foreach (Transform building in tile.transform)
            {
                if (controller.funds >= 20000)
                {
                    building.GetComponent<Hospital>().AddTeam();
                    controller.funds -= 20000;
                }
            }
        }
        else if ((selected = GameObject.FindGameObjectWithTag("SelectedDepot")) != null)
        {
            Tile tile = selected.GetComponent<Tile>();
            foreach (Transform building in tile.transform)
            {
                if (controller.funds >= 20000)
                {
                    building.GetComponent<Depot>().AddTeam();
                    controller.funds -= 20000;
                }
            }
        }
		currentFunds.text = controller.funds.ToString();
    }

    public void HideAllBuildings()
    {
        GameObject[] hospitals = GameObject.FindGameObjectsWithTag("Hospital");
        foreach (GameObject hospital in hospitals)
            hospital.GetComponent<Renderer>().enabled = false;
        GameObject[] depots = GameObject.FindGameObjectsWithTag("Depot");
        foreach (GameObject depot in depots)
            depot.GetComponent<Renderer>().enabled = false;
    }

    public void ButtonHeight()
	{
		controller.SetMode(Mode.Height);
	}

	public void ButtonBasic()
	{
		controller.SetMode(Mode.Basic);
	}

	public void ButtonMoisture()
	{
		controller.SetMode(Mode.Moisture);
	}

	public void ButtonDamages()
	{
		controller.SetMode(Mode.Damages);
	}

	public void ButtonInjured()
	{
		controller.SetMode(Mode.Injured);
	}

	public void ButtonStart()
	{
		if (!isStarted)
		{
			PlayerController.StartRescue();
			isStarted = true;
		}
	}

	public void ButtonAddMedic()
	{
		AddVehicleToBuilding();
	}

	public void ButtonAddEngineer()
	{
		AddVehicleToBuilding();
    }
}