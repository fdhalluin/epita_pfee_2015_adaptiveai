﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InfoCityDisplay : MonoBehaviour
{
    private string population = "";
    private string damaged = "";
    private string injured = "";

    Text PopulationCity;
    Text DamagedCity;
    Text InjuredCity;

    void Start()
    {
        PopulationCity = GameObject.Find("PopulationCity").GetComponent<Text>();
        DamagedCity = GameObject.Find("DamagedCity").GetComponent<Text>();
        InjuredCity = GameObject.Find("InjuredCity").GetComponent<Text>();  
    }

    void Update()
    {
        GameObject selectedCity = GameObject.FindGameObjectWithTag("SelectedCity");
        if (selectedCity != null)
        {
            gameObject.GetComponent<Canvas>().enabled = true;

            City city = selectedCity.GetComponent<Tile>().node.city;

            population = city.Population().ToString();

            int damagedPercentage = city.Damaged();
            if (damagedPercentage > 0)
                damaged = "D";
            else
                damaged = "Not d";
            damaged += "amaged";
            if (damagedPercentage > 0)
                damaged += " (" + damagedPercentage + "%)";

            injured = city.Injured().ToString();

            ChangeCityDisplay();
        }
        else
            gameObject.GetComponent<Canvas>().enabled = false;
    }

    private void ChangeCityDisplay()
    {
        PopulationCity.text = "Population: " + population;
        DamagedCity.text = damaged;
        InjuredCity.text = "Injured: " + injured;
    }
}
