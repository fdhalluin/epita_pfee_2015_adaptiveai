﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;

public class PathFinding
{
    #region A star
    static public List<Node> pathFindingAStar(Node start, Node goal)
    {
        if (goal == null)
            return null;

        List<Node> closedList = new List<Node>();
        List<Node> openList = new List<Node>();

        Dictionary<Node, double> gScore = new Dictionary<Node, double>();
        Dictionary<Node, double> fScore = new Dictionary<Node, double>();

        Dictionary<Node, Node> origin = new Dictionary<Node, Node>();

        openList.Add(start);
        gScore.Add(start, 0);
        fScore.Add(start, manhattanDistance(start, goal));

        while (openList.Count > 0)
        {
            Node actual = popLowest(openList, fScore);
            if (actual.Tile.name.Equals(goal.Tile.name))
                return reconstructPath(origin, actual);
            
            closedList.Add(actual);

            foreach (Node node in actual.Sons.Keys)
            {
                if (!Biomes.isWalkable(node.type) || closedList.Contains(node))
                    continue;

                double actualScore = (gScore[actual] + (actual.Sons[node] * transactionCost(actual, node))) / (1f + (actual.roads.Contains(node) ? 1f : 0f));

                if (!openList.Contains(actual) || gScore[actual] > actualScore)
                {
                    origin[node] = actual;
                    gScore[node] = actualScore;
                    fScore[node] = gScore[node] + manhattanDistance(node, goal);

                    if (!openList.Contains(node))
                        openList.Add(node);
                }
            }
        }

        return null;
    }

    private static double transactionCost(Node actual, Node node)
    {
        return Math.Abs(actual.height - node.height) * 0.5 + 1;
    }

    static private Node popLowest(List<Node> openList, Dictionary<Node, double> score)
    {
        Node result = null;
        double valueLowest = double.MaxValue;

        foreach (Node node in openList)
        {
            if (score.ContainsKey(node) && valueLowest > score[node])
            {
                result = node;
                valueLowest = score[node];
            }
        }

        openList.Remove(result);
        return result;
    }

    static private List<Node> reconstructPath(Dictionary<Node, Node> origin, Node actual)
    {
        List<Node> path = new List<Node>();

        path.Add(actual);

        while (origin.ContainsKey(actual))
        {
            actual = origin[actual];
            path.Insert(0, actual);
        }

        return path;
    }
    #endregion
    #region Dijkstra
    static public List<Node> pathFindingDijkstra(Node start, Node goal)
    {
        List<Node> resPath = new List<Node>();
        Dictionary<Node, float> nodeScores = new Dictionary<Node, float>();
        Queue<Node> tmpNodes = new Queue<Node>();

        tmpNodes.Enqueue(start);
        nodeScores.Add(start, 0);

        while (tmpNodes.Count != 0)
        {
            Node lastNode = tmpNodes.Dequeue();
            float lastNodeScore;
            nodeScores.TryGetValue(lastNode, out lastNodeScore);

            foreach (Node son in lastNode.Sons.Keys)
            {
                float newValue = lastNodeScore + sqEuclideanDistance(lastNode, son);

                if (nodeScores.ContainsKey(son))
                {
                    float oldValue;
                    nodeScores.TryGetValue(son, out oldValue);

                    if (newValue < oldValue)
                    {
                        nodeScores.Remove(son);
                        nodeScores.Add(son, newValue);
                    }
                }
                else
                {
                    tmpNodes.Enqueue(son);
                    nodeScores.Add(son, newValue);
                }
            }
        }

        if (nodeScores.ContainsKey(goal))
        {
            Node lastNode = goal;
            float lastNodeScore;
            nodeScores.TryGetValue(lastNode, out lastNodeScore);

            resPath.Add(goal);

            while (lastNode != start)
            {
                foreach (Node son in lastNode.Sons.Keys)
                {
                    float tmpDist = sqEuclideanDistance(lastNode, son);
                    float tmpValue;
                    nodeScores.TryGetValue(son, out tmpValue);

                    if (lastNodeScore - tmpDist == tmpValue)
                    {
                        resPath.Add(son);
                        lastNode = son;
                        break;
                    }
                }
            }

            resPath.Reverse();
        }

        return resPath;
    }
    #endregion

    static private double manhattanDistance(Node start, Node goal)
    {
        return (Math.Abs(goal.Position.x - start.Position.x)
            + Math.Abs(goal.Position.y - start.Position.y))
            * transactionCost(start, goal);
    }

	static private float sqEuclideanDistance(Node par_start, Node par_end)
	{
		float tmpHeight = par_end.height - par_start.height;
		return (par_end.Position - par_start.Position).SqrMagnitude() - tmpHeight * tmpHeight;
	}
}
