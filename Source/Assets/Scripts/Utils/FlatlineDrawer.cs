﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlatlineDrawer : MonoBehaviour {

	private Material roadMat;
	private Color roadColor;

	private struct Triangle3
	{
		public Vector3[] points;
		public Vector3 normal;
	};

	public void Start ()
	{
	
	}

	public void Awake()
	{
		roadMat = new Material(Shader.Find("Sprites/Default"));
		roadColor = new Color(0.5f, 0.5f, 0.5f, 1);
	}

	private Vector3 GetPositionFromScaleonTriangleBorder(Triangle3 triangle, int border, float scale)
	{
		Vector3 position = new Vector3();

		if (border == 0)
			position = scale * triangle.points[1] + (1-scale) * triangle.points[0];
		else if (border == 1)
			position = scale * triangle.points[2] + (1-scale) * triangle.points[1];
		else // if (border == 2)
			position = scale * triangle.points[0] + (1-scale) * triangle.points[2];

		return position;
	}

	private List<Vector3> GetIntersectPointsFromProjections(Triangle3 triangle, float[] triangleProjs, int[] triangleIndexes, float projValue)
	{
		List<Vector3> intersectPoints = new List<Vector3>();

		if (projValue <= triangleProjs[0])
			intersectPoints.Add(triangle.points[triangleIndexes[0]]);
		else if (projValue >= triangleProjs[2])
			intersectPoints.Add(triangle.points[triangleIndexes[2]]);
		else
		{
			float scale = (projValue - triangleProjs[0]) / (triangleProjs[2] - triangleProjs[0]);
			intersectPoints.Add(scale * triangle.points[triangleIndexes[2]] + (1-scale) * triangle.points[triangleIndexes[0]]);
			
			if (projValue <= triangleProjs[1])
			{
				scale = (projValue - triangleProjs[0]) / (triangleProjs[1] - triangleProjs[0]);
				intersectPoints.Add(scale * triangle.points[triangleIndexes[1]] + (1-scale) * triangle.points[triangleIndexes[0]]);
			}
			else
			{
				scale = (projValue - triangleProjs[1]) / (triangleProjs[2] - triangleProjs[1]);
				intersectPoints.Add(scale * triangle.points[triangleIndexes[2]] + (1-scale) * triangle.points[triangleIndexes[1]]);
			}
		}

		return intersectPoints;
	}

	private List<Vector3> DefineRoadPolygonOnTriangleFromBorders(Triangle3 triangle, int borderStart, int borderEnd, float borderStartScale, float borderEndScale, float width)
	{
		List<Vector3> roadPoints = new List<Vector3>(3);

		borderStartScale = Mathf.Clamp(borderStartScale, 0, 1);
		borderEndScale = Mathf.Clamp(borderEndScale, 0, 1);
		borderStart = borderStart % 3;
		borderEnd = borderEnd % 3;

		Vector3 roadStart = GetPositionFromScaleonTriangleBorder(triangle, borderStart, borderStartScale);
		Vector3 roadEnd = GetPositionFromScaleonTriangleBorder(triangle, borderEnd, borderEndScale);
		Vector3 roadDirection = roadEnd - roadStart;
		Vector3 roadWidthDirection = Vector3.Cross(triangle.normal, roadDirection);
		roadWidthDirection.Normalize();

		// Projecting the triangle points and the left and right side of the road on the same axis
		float[] triangleProjs = new float[]
			{Vector3.Dot(roadWidthDirection, triangle.points[0] - roadStart),
			Vector3.Dot(roadWidthDirection, triangle.points[1] - roadStart),
			Vector3.Dot(roadWidthDirection, triangle.points[2] - roadStart)};
		int[] triangleIndexes = new int[] {0, 1, 2};
		System.Array.Sort(triangleProjs, triangleIndexes);

		if (-width <= triangleProjs[1] && width >= triangleProjs[1])
			roadPoints.Add(triangle.points[triangleIndexes[1]]);

		// Finding the extremities of the left side of the road
		roadPoints.AddRange(GetIntersectPointsFromProjections(triangle, triangleProjs, triangleIndexes, -width));

		// Finding the extremities of the right side of the road
		roadPoints.AddRange(GetIntersectPointsFromProjections(triangle, triangleProjs, triangleIndexes, width));

		return roadPoints;
	}
}
