﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System.Collections;

// Follow the parent transform with some delay.
public class DelayedVisual : MonoBehaviour {

    public Vector3 position;
    public Vector3 targetPosition;
    public Vector3 direction;
    public Vector3 targetDirection;
    public float damping = 0.5f;

	void Start () {
        position = transform.position;
        targetPosition = position;
        direction = transform.forward;
        targetDirection = direction;
	}
	
	void Update () {
        targetPosition = transform.parent.position;
        position = Utils.Damp(position, targetPosition, damping, Time.deltaTime);
        Vector3 rawDirection = (targetPosition - position);
        rawDirection.y = 0;
        if (rawDirection.magnitude > 0.1f)
            targetDirection = rawDirection.normalized;
        direction = Utils.Damp(direction, targetDirection, damping, Time.deltaTime);
        transform.position = position;
        transform.rotation = Quaternion.LookRotation(direction);
	}
}
