﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System.Collections;

/*
 * Script for controlling camera with keyboard and mouse
 */
public class CameraController : MonoBehaviour
{
    // Movement
    public float moveSpeed = 20f;
    [Range(10, 30)]
    public float mouseMoveSpeed = 20f;

    private bool isMouseMoving;
    private Vector3 targetPosition;
    private Vector3 oldTargetPosition;

    // Movement to point
    [Range(1, 10)]
    public float moveToPointSpeed = 5f;

    private bool isMovingToPoint;
    private Vector3 point;

    // Zoom
    public float minZoom = 5f;
    public float maxZoom = 500f;
    [Range(0, 1)]
    public float zoomSensitivity = 0.39f;
    public float dampingFactor = 0.1f;
    public AnimationCurve zoomCurve;

    [Range(0, 1)]
    public float startCurrentZoom = 1f;
    [Range(0, 1)]
    public float startTargetZoom = 0.6f;

    private float currentZoom;
    private float targetZoom;

    // Rotation
    public float rotationSpeed = 2f;
    public float mouseRotationSpeed = 9f;
    public float minHorizontalAngle = 15f;
    public float maxHorizontalAngle = 90f;
    [Range(15, 90)]
    public float startHorizontalAngle = 90.0f;

    private bool isRotating;
    private bool isMouseRotating;
    private float horizontalAngle;

    private Vector3 mouseOrigin;

    void Start()
    {
        currentZoom = startCurrentZoom;
        targetZoom = startTargetZoom;

        horizontalAngle = startHorizontalAngle;

        isMovingToPoint = false;
        point = Vector3.zero;
    }

    void Update()
    {
        GameObject tile = GameObject.FindGameObjectWithTag("Selected");
        if (tile == null)
            tile = GameObject.FindGameObjectWithTag("SelectedCity");

        if (tile != null && Input.GetKeyDown(KeyCode.Space))
        {
            isMovingToPoint = true;
            point = tile.transform.position;
        }

        if (isMovingToPoint)
            moveToPoint();

        isRotating = Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl);

        if (isRotating)
            rotate();
        else
            move();

        zoom();

        if (Input.GetMouseButtonDown(1))
        {
            mouseOrigin = Input.mousePosition;
            isMouseRotating = true;

            isMovingToPoint = false;
        }
        if (Input.GetMouseButtonDown(2))
        {
            mouseOrigin = Input.mousePosition;
            oldTargetPosition = targetPosition;
            isMouseMoving = true;

            isMovingToPoint = false;
        }

        if (!Input.GetMouseButton(1))
            isMouseRotating = false;
        if (!Input.GetMouseButton(2))
            isMouseMoving = false;

        if (isMouseRotating)
            mouseRotate();
        if (isMouseMoving)
            mouseMove();

        ajustHorizontalAngle();
        transform.rotation = Quaternion.Euler(horizontalAngle, transform.eulerAngles.y, transform.eulerAngles.z);

        ajustTargetPosition();
        transform.position = targetPosition - transform.rotation * Vector3.forward * Mathf.Lerp(minZoom, maxZoom, currentZoom);
    }

    private void ajustTargetPosition()
    {
        float size = GetComponent<PlayerController>().size;
        if (targetPosition.x < -size / 2f)
            targetPosition.x = -size / 2f + 0.5f;
        if (targetPosition.x > size / 2f)
            targetPosition.x = size / 2f - 0.5f;
        if (targetPosition.z < -size / 2f)
            targetPosition.z = -size / 2f + 0.5f;
        if (targetPosition.z > size / 2f)
            targetPosition.z = size / 2f - 0.5f;
    }

    private void ajustHorizontalAngle()
    {
        if (horizontalAngle < minHorizontalAngle)
            horizontalAngle = minHorizontalAngle;
        if (horizontalAngle > maxHorizontalAngle)
            horizontalAngle = maxHorizontalAngle;
    }

    private void rotate()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));

        if (Physics.Raycast(ray, out hit, 1000.0f))
        {
            horizontalAngle += Input.GetAxis("Vertical") * rotationSpeed;

            transform.RotateAround(hit.point, new Vector3(0, 1, 0), Input.GetAxis("Horizontal") * rotationSpeed);
        }
    }

    private void move()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        if (moveHorizontal != 0 || moveVertical != 0)
            isMovingToPoint = false;

        float yRoation = transform.eulerAngles.y;

        Quaternion moveOrientation = Quaternion.Euler(0, yRoation, 0);
        Vector3 movement = moveOrientation * new Vector3(moveHorizontal, 0, moveVertical);

        float size = GetComponent<PlayerController>().size;
        targetPosition += movement * moveSpeed * (size / 10.0f) * (currentZoom + 0.1f) * Time.deltaTime;
    }

    private void zoom()
    {
        float moveZoom = Input.GetAxis("Mouse ScrollWheel");
        targetZoom -= moveZoom * zoomSensitivity;
        targetZoom = Mathf.Clamp01(targetZoom);
        currentZoom = Utils.Damp(currentZoom, zoomCurve.Evaluate(targetZoom), dampingFactor, Time.deltaTime);
    }

    private void mouseRotate()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));

        if (Physics.Raycast(ray, out hit, 1000.0f))
        {
            Vector3 pos = Input.mousePosition - mouseOrigin;
            horizontalAngle -= pos.y * mouseRotationSpeed * Time.fixedDeltaTime;

            transform.RotateAround(hit.point, new Vector3(0, 1, 0), pos.x * mouseRotationSpeed * Time.fixedDeltaTime);
        }
        mouseOrigin = Input.mousePosition;
    }

    private void mouseMove()
    {
        Vector3 pos = Input.mousePosition - mouseOrigin;

        float yRoation = transform.eulerAngles.y;

        Quaternion moveOrientation = Quaternion.Euler(0, yRoation, 0);
        Vector3 movement = moveOrientation * new Vector3(-pos.x, 0, -pos.y);
        targetPosition = oldTargetPosition + movement * mouseMoveSpeed * (currentZoom + 0.025f) * Time.fixedDeltaTime;
    }

    private void moveToPoint()
    {
        Vector3 target = new Vector3(targetPosition.x, transform.position.y, targetPosition.z);
        Vector3 dir = point - target;
        dir.y = 0;

        targetPosition += dir * moveToPointSpeed * Time.deltaTime;

        if (Mathf.Abs(targetPosition.x - point.x) < 0.15f && Mathf.Abs(targetPosition.z - point.z) < 0.15f)
            isMovingToPoint = false;
    }
}
