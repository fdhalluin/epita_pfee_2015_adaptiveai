﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Depot : MonoBehaviour
{
	public Node location;
	private Func<EngineerTeam, int, Node> FIntelligenceUsed;
	
	private List<EngineerTeam> FTeams = new List<EngineerTeam>();
	
	private int FRange;
	
	void Start()
	{
		for (int i = 0; i < 2; i++)
			FTeams.Add(Instantiate(GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerController>().engineerTeam).GetComponent<EngineerTeam>());
		foreach (EngineerTeam team in FTeams)
		{
			team.transform.parent = transform;
			team.position = location;
			team.origin = this;
			team.transform.position = team.position.Tile.transform.position;
		}

        FRange = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerController>().numSites / 300;
	}

    public void Initiate(EngineerIA ia)
    {
        if (ia == EngineerIA.Basic)
            FIntelligenceUsed = findRandomDamagedTile;
        else if (ia == EngineerIA.Adaptative)
            FIntelligenceUsed = findNearbyDamagedTile;
    }
	
	void Update()
	{
		foreach (EngineerTeam team in FTeams)
		{
			if (team.IsAvailable())
			{
				if (team.position.damaged != 0)
					team.destination = null;
                else if (team.destination == null)
				{
                    team.destination = FIntelligenceUsed(team, FRange);
                    if (team.destination != null)
                        team.destination.AddIncomingOrActingTeam(team);
                    else
                        team.SetStationary();
                }
            }
		}
	}

    public Func<EngineerTeam, int, Node> GetIntelligence()
    {
        return FIntelligenceUsed;
    }

	static Node findRandomDamagedTile(EngineerTeam team, int distance)
	{
        Node[] keys = new Node[team.position.Sons.Count];
        int count = 0;
        foreach (Node node in team.position.Sons.Keys)
        {
            if (node.damaged != 0)
            {
                keys[count] = node;
                count += 1;
            }
        }

        if (count == 0)
            return null;
        return keys[UnityEngine.Random.Range(0, count)];
    }

    internal static Node findNearbyDamagedTile(EngineerTeam team, int distance)
	{
		Node maxDamaged = null;
		float maxDamagedValue = 0;
		float ratio = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerController>().numSites;
		
		foreach (KeyValuePair<Node, float> pairNodeFloat in Graph.getNodesAtDistance(team.position, distance))
		{
            float tempDamagedRatio = pairNodeFloat.Key.damaged / ((pairNodeFloat.Key.IncomingAndActingEngineerTeamsCount * 3 + 1f) * pairNodeFloat.Value);
			if (tempDamagedRatio > maxDamagedValue)
			{
				maxDamaged = pairNodeFloat.Key;
				maxDamagedValue = tempDamagedRatio;
			}
		}
		
		if (maxDamagedValue <= 0)
			return null;
		return maxDamaged;
	}

    public void AddTeam()
    {
        EngineerTeam team = Instantiate(GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerController>().engineerTeam).GetComponent<EngineerTeam>();
        FTeams.Add(team);
        team.transform.parent = transform;
        team.position = location;
        team.origin = this;
        team.transform.position = team.position.Tile.transform.position;
    }

    public int getTeamCount()
    {
        return FTeams.Count;
    }
}