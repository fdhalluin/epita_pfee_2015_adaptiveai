﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Groupment
{
    public string name;

    private Node FPosition;

    private int FQuantity;

    private int FHealthy;
    private int FSlightly;
    private int FMildly;
    private int FSeverly;
    private int FDead;
    private int slpoints, mipoints, sepoints;
    private int mitime = 40, setime = 80;
    private int s2mtime = 80, m2stime = 80, s2dtime = 80;

    public float update;

    #region Getter / Setter
    //============
    // Se/Getter
    //============

    public int Quantity
    {
        get { return FQuantity; }
    }

    public int Injured
    {
        get { return FSlightly + FMildly + FSeverly; }
    }
    #endregion

    public Groupment(Node position, int quantity, float state)
    {
        FPosition = position;

        FQuantity = quantity;

        FHealthy = 0;
        FSlightly = 0;
        FMildly = 0;
        FSeverly = 0;
        FDead = 0;
    }

    public void InitiateInjuries(float state)
    {
        int r1 = 5 * UnityEngine.Random.Range(0, FQuantity) / 13;
        int r2 = 4 * UnityEngine.Random.Range(0, FQuantity) / 13;
        int r3 = 3 * UnityEngine.Random.Range(0, FQuantity) / 13;
        int r4 = 1 * UnityEngine.Random.Range(0, FQuantity) / 13;

        int diff = (FQuantity - r1 - r2 - r3 - r4) / 4;

        FSlightly = Convert.ToInt32((r1 + diff) * state);
        FMildly = Convert.ToInt32((r2 + diff) * state);
        FSeverly = Convert.ToInt32((r3 + diff) * state);
        FDead = Convert.ToInt32((r4 + diff) * state);
        FHealthy = FQuantity - FSlightly - FMildly - FSeverly - FDead;
    }

    public void Update()
    {
        if (this.update != 0)
        {
            slpoints = (int)Math.Ceiling(update * 10.0f / 100.0f);
            mipoints = (int)Math.Ceiling(update * 30.0f / 100.0f);
            sepoints = (int)Math.Ceiling(update * 60.0f / 100.0f);

            if (FSlightly > slpoints)
                FSlightly -= slpoints;
            else
                FSlightly = 0;

            if (mitime == 0)
            {
                if (FMildly > mipoints)
                {
                    FSlightly += mipoints;
                    FMildly -= mipoints;
                }
                else
                {
                    FSlightly += FMildly;
                    FMildly = 0;
                }
                mitime = 2;
            }
            else
                mitime--;

            if (setime == 0)
            {
                if (FSeverly > sepoints)
                {
                    FMildly += sepoints;
                    FSeverly -= sepoints;
                }
                else
                {
                    FMildly += FSeverly;
                    FSeverly = 0;
                }
                sepoints = 4;
            }
            else
                setime--;

            if (s2mtime == 0)
            {
                if (FSlightly > FSlightly * 0.1f)
                {
                    FMildly += (int)Math.Floor(FSlightly * 0.1f);
                    FSlightly -= (int)Math.Floor(FSlightly * 0.1f);
                }
                else
                {
                    FMildly += FSlightly;
                    FSlightly = 0;
                }
                s2mtime = 4;
            }
            else
                s2mtime--;

            if (m2stime == 0)
            {
                if (FMildly > FMildly * 0.1f)
                {
                    FSeverly += (int)Math.Floor(FMildly * 0.1f);
                    FMildly -= (int)Math.Floor(FMildly * 0.1f);
                }
                else
                {
                    FSeverly += FMildly;
                    FMildly = 0;
                }
                m2stime = 4;
            }
            else
                m2stime--;

            if (s2dtime == 0)
            {
                if (FSeverly > FSeverly * 0.1f)
                {
                    FDead += (int)Math.Floor(FSeverly * 0.1f);
                    FSeverly -= (int)Math.Floor(FSeverly * 0.1f);
                }
                else
                {
                    FDead += FSeverly;
                    FSeverly = 0;
                }
                s2dtime = 4;
            }
            else
                s2dtime--;

            FHealthy = FQuantity - FSlightly - FMildly - FSeverly - FDead;

            this.update = 0;
        }
    }
}


public class Groupments
{
    //============
    // Singleton
    //============
    
    private static Groupments FInstance;

    static public Groupments Instance()
    {
        if (FInstance == null)
            FInstance = new Groupments();
        return FInstance;
    }

    static public bool Delete()
    {
        if (FInstance != null && FInstance.FGroupments.Count == 0)
        {
            FInstance = null;
            return true;
        }

        return false;
    }

    //============
    // Variables
    //============

    private Dictionary<string, Groupment> FGroupments;
	public int totalInjured;

    //============
    // Des/Constructor
    //============

    private Groupments()
    {
        FGroupments = new Dictionary<string, Groupment>();
		totalInjured = 0;
    }

    //============
    // Other methods
    //============

    public void UpdateAll()
    {
        foreach (Groupment groupment in FGroupments.Values)
        {
            groupment.Update();
        }
    }

    public void AddGroupment(string name, Node position, int quantity)
    {
        Groupment groupment = new Groupment(position, quantity, 0);
        groupment.name = name;
        FGroupments.Add(name, groupment);
        position.AddGroupment(groupment);
    }

    public void RemoveGroupment(Groupment key)
    {
        FGroupments.Remove(key.name);
    }

    public Dictionary<string, Groupment>.ValueCollection GetGroupments()
    {
        return FGroupments.Values;
    }


	public static int injuredPopulation(Dictionary<string, Groupment>.ValueCollection groupments)
	{
		int injured = 0;
		foreach (Groupment groupment in groupments)
		{
			injured += groupment.Injured;
		}
		
		return injured;
	}
	
	public static int totalPopulation(Dictionary<string, Groupment>.ValueCollection groupments)
	{
		int totalPop = 0;
		
		foreach (Groupment groupment in groupments)
		{
			totalPop += groupment.Quantity;
		}
		
		return totalPop;
	}

	public static int injuredPopulation(List<Groupment> groupments)
	{
		int injured = 0;
		foreach (Groupment groupment in groupments)
		{
			injured += groupment.Injured;
		}
		
		return injured;
	}

    public static int totalPopulation(List<Groupment> groupments)
    {
        int totalPop = 0;

        foreach (Groupment groupment in groupments)
        {
            totalPop += groupment.Quantity;
        }

        return totalPop;
    }
}