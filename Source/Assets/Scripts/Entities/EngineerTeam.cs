﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum EngineerTeamState
{
    Idle,
    Travelling,
    ActingOnTile,
    Stationary
}

public class EngineerTeam : MonoBehaviour
{
    public Depot origin;

    public Node position;
    private EngineerTeamState FState;
    private EngineerTeamState FNextState;

    private List<Node> FPath;
    public Node destination;

    public float travelTimeMult = 0.5f;
    public float repairSpeed = 0.50f;

    private float FTimeSpent = -1;

    private int FRange;

    void Start()
    {
        FRange = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerController>().numSites / 350;
        destination = null;
        FState = EngineerTeamState.Idle;
        FNextState = EngineerTeamState.Idle;
    }

    void Update()
    {
        FState = FNextState;

        switch (FState)
        {
            case EngineerTeamState.Idle:
                if (destination == null && position.damaged != 0)
                    FNextState = EngineerTeamState.ActingOnTile;
                else if (destination != null)
                {
                    FPath = PathFinding.pathFindingAStar(position, destination);
					if (FPath == null)
					{
						Debug.Log(name + " doing nothing (null path)");
						FNextState = EngineerTeamState.Stationary;
					}
					else
					{
                    	FPath.RemoveAt(0);
                    	FNextState = EngineerTeamState.Travelling;
                	}
					FTimeSpent = 0;
				}
                break;
            case EngineerTeamState.Travelling:
                FTimeSpent += Time.deltaTime;
                float travelCost = position.Sons[FPath[0]] * travelTimeMult;
                if (FTimeSpent > travelCost)
                {
                    FTimeSpent -= travelCost;
                    position = FPath.ElementAt(0);
                    transform.Translate(position.Tile.transform.position - transform.position);
                    FPath.RemoveAt(0);
                    if (FPath.Count == 0)
                    {
                        FTimeSpent = -1;
                        FNextState = EngineerTeamState.Idle;
                        destination = null;

                        if (position.damaged == 0)
                        {
                            destination.RemoveIncomingOrActingTeam(this);
                            destination = origin.GetIntelligence()(this, FRange);
                            if (destination == null && position != origin.location)
                                destination = origin.location;
                            destination.AddIncomingOrActingTeam(this);
                        }
                    }
                }
                break;
            case EngineerTeamState.ActingOnTile:
                if (position.damaged <= 0)
                {
                    position.damaged = 0;
                    position.RemoveIncomingOrActingTeam(this);
                    if (destination == null && position != origin.location)
                    {
                        destination = origin.GetIntelligence()(this, FRange);
                        if (destination == null)
                            destination = origin.location;
                        destination.AddIncomingOrActingTeam(this);
                    }
                    FNextState = EngineerTeamState.Idle;
                }
                else
                {
                    position.damaged -= repairSpeed * Time.deltaTime;
                    position.ActivateUpdateMap();
                }
                break;
            case EngineerTeamState.Stationary:
                FTimeSpent += Time.deltaTime;
                if (FTimeSpent > 30)
                {
                    FNextState = EngineerTeamState.Idle;
                    FTimeSpent = -1;
                }
                break;
        }
	}

    void OnDrawGizmos()
    {
        if (destination != null && destination.Tile != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, destination.Tile.transform.position);
        }
    }

    public bool IsAvailable()
    {
        return FNextState == EngineerTeamState.Idle && position == origin.location;
    }

    public void SetStationary()
    {
        FNextState = EngineerTeamState.Stationary;
        FTimeSpent = 0;
    }

    public void SetActive()
    {
        FNextState = EngineerTeamState.Idle;
        FTimeSpent = -1;
    }
}
