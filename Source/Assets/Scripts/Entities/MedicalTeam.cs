﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum MedicalTeamState
{
    Idle,
    Travelling,
    ActingOnTile,
    Stationary
}

public class MedicalTeam : MonoBehaviour
{
    public Hospital origin;

    public Node position;
    private MedicalTeamState FState;
    private MedicalTeamState FNextState;

    private List<Node> FPath;
    public Node destination;

    public float FHealingSpeed = 300f;
    public float FTravelTimeMultClean = 0.5f;
    public float FTravelTimeMultDamaged = 2f;

    private float FTimeSpent = -1;

    private int FRange;

    void Start()
    {
        FRange = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerController>().numSites / 350;
        destination = null;
        FState = MedicalTeamState.Idle;
        FNextState = MedicalTeamState.Idle;
    }

    void Update()
    {
        FState = FNextState;

        switch (FState)
        {
	        case MedicalTeamState.Idle:
                if (destination == null)
                {
                    if (Groupments.injuredPopulation(position.Groupments) != 0)
                        FNextState = MedicalTeamState.ActingOnTile;
                    else
                        FNextState = MedicalTeamState.Stationary;
                }
                else if (destination != null)
                {
                    FPath = PathFinding.pathFindingAStar(position, destination);
                    if (FPath == null)
                    {
                        Debug.Log(this.name + " doing nothing (null path)");
                        FNextState = MedicalTeamState.Stationary;
                    }
                    else
                    {
                        FPath.RemoveAt(0);
                        FNextState = MedicalTeamState.Travelling;
                    }
                    FTimeSpent = 0;
                }
                break;
            case MedicalTeamState.Travelling:
                FTimeSpent += Time.deltaTime;
                var travelCost = position.Sons[FPath[0]] * (position.damaged > 0.01f ? FTravelTimeMultDamaged : FTravelTimeMultClean);
                if (FTimeSpent > travelCost)
                {
                    FTimeSpent -= travelCost;
                    position = FPath.ElementAt(0);
                    transform.Translate(position.Tile.transform.position - transform.position);
                    FPath.RemoveAt(0);
                    if (FPath.Count == 0)
                    {
                        FTimeSpent = -1;
                        FNextState = MedicalTeamState.Idle;
                        destination = null;

                        if (Groupments.injuredPopulation(position.Groupments) == 0)
                        {
                            position.RemoveIncomingOrActingTeam(this);
                            destination = origin.GetIntelligence()(this, FRange);
                            if (destination == null && position != origin.location)
                                destination = origin.location;
                            destination.AddIncomingOrActingTeam(this);
                        }
                    }
                }
                break;
            case MedicalTeamState.ActingOnTile:
                int notInjuredGroupments = 0;
                foreach (Groupment groupment in position.Groupments)
                {
                    if (groupment.Injured > 0)
                        groupment.update += Time.deltaTime * FHealingSpeed;
                    else
                        notInjuredGroupments += 1;
                }
                if (notInjuredGroupments == position.Groupments.Count)
                {
                    position.RemoveIncomingOrActingTeam(this);
                    if (destination == null && position != origin.location)
                    {
                        destination = origin.GetIntelligence()(this, FRange);
                        if (destination == null)
                            destination = origin.location;
                        destination.AddIncomingOrActingTeam(this);
                    }
                    FNextState = MedicalTeamState.Idle;
                }
                else
                    position.ActivateUpdateMap();
                break;
            case MedicalTeamState.Stationary:
                FTimeSpent += Time.deltaTime;
                if (FTimeSpent > 30)
                {
                    FNextState = MedicalTeamState.Idle;
                    FTimeSpent = -1;
                }
                break;

        }
	}

    void OnDrawGizmos()
    {
        if (destination != null && destination.Tile != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, destination.Tile.transform.position);
        }
    }

    public bool IsAvailable()
    {
        return FNextState == MedicalTeamState.Idle && position == origin.location;
    }

    public void SetStationary()
    {
        FNextState = MedicalTeamState.Stationary;
        FTimeSpent = 0;
    }

    public void SetActive()
    {
        FNextState = MedicalTeamState.Idle;
        FTimeSpent = -1;
    }
}
