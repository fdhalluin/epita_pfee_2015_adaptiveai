﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class City : MonoBehaviour
{
    public List<Node> nodes = new List<Node>();
    public Node center = null;

    private int FRange;

    public void Initiate()
    {
        int maxPop = -1;
        Node maybeCenter = null;

        foreach (Node node in nodes)
        {
            int totalPop = Groupments.totalPopulation(node.Groupments);
            if (totalPop > maxPop)
            {
                maxPop = totalPop;
                maybeCenter = node;
            }
        }

        if (maybeCenter != null)
        {
            center = maybeCenter;
            gameObject.transform.parent = center.Tile.transform;
            gameObject.transform.position = center.Tile.transform.position;
            gameObject.transform.localScale = Vector3.one * Mathf.Lerp(0.7f, 1f, (float) center.citySize);
        }
        else
            Debug.LogError("City without center.");

        foreach (Node node in nodes)
        {
            if (node != center)
            {
                node.suburb = GameObject.Instantiate(GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerController>().suburb).GetComponent<Suburb>();
                node.suburb.transform.parent = node.Tile.transform;
                node.suburb.transform.position = node.Tile.transform.position;
                node.suburb.transform.localScale = node.suburb.transform.localScale * Mathf.Lerp(0.7f, 1f, (float)node.citySize);
                node.suburb.city = this;
                node.suburb.node = node;
            }
        }
    }

    void Update()
    {

    }

    //============
    // Other methods
    //============

    public int Population()
    {
        int result = 0;

        foreach (Node node in nodes)
            result += Groupments.totalPopulation(node.Groupments);

        return result;
    }

    public int Damaged()
    {
        float damaged = 0;

        foreach (Node node in nodes)
            damaged += node.damaged;

        damaged /= nodes.Count;

        return (int)System.Math.Round(damaged * 100);
    }

    public int Injured()
    {
        int result = 0;

        foreach (Node node in nodes)
            result += Groupments.injuredPopulation(node.Groupments);

        return result;
    }
}
