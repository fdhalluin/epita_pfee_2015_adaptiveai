﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Hospital : MonoBehaviour
{
    public Node location;
    private Func<MedicalTeam, int, Node> FIntelligenceUsed;

    private List<MedicalTeam> FTeams = new List<MedicalTeam>();

    private int FRange;

    void Start()
    {
        for (int i = 0; i < 2; i++)
            FTeams.Add(Instantiate(GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerController>().medicalTeam).GetComponent<MedicalTeam>());
        foreach (MedicalTeam team in FTeams)
        {
            team.transform.parent = transform;
            team.position = location;
            team.origin = this;
            team.transform.position = team.position.Tile.transform.position;
        }

        FRange = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerController>().numSites / 200;
    }

    public void Initiate(MedicalIA ia)
    {
        if (ia == MedicalIA.Basic)
            FIntelligenceUsed = findRandomInjuredGroupment;
        else if (ia == MedicalIA.Adaptative)
            FIntelligenceUsed = findNearbyInjuredGroupment;
    }

    void Update()
    {
        foreach (MedicalTeam team in FTeams)
        {
            if (team.IsAvailable())
            {
                int injured = 0;
                foreach (Groupment groupment in team.position.Groupments)
                    injured += groupment.Injured;
                if (injured != 0)
                    team.destination = null;
                else if (team.destination == null)
                {
                    team.destination = FIntelligenceUsed(team, FRange);
                    if (team.destination != null)
                        team.destination.AddIncomingOrActingTeam(team);
                    else
                        team.SetStationary();
                }
            }
        }
    }

    public Func<MedicalTeam, int, Node> GetIntelligence()
    {
        return FIntelligenceUsed;
    }

	public static Node findRandomInjuredGroupment(MedicalTeam team, int distance)
	{
        Node[] keys = new Node[team.position.Sons.Count];
        int count = 0;
        foreach (Node node in team.position.Sons.Keys)
        {
            if (Groupments.injuredPopulation(node.Groupments) != 0)
            {
                keys[count] = node;
                count += 1;
            }
        }

        if (count == 0)
            return null;
        return keys[UnityEngine.Random.Range(0, count)];
    }

    public static Node findNearbyInjuredGroupment(MedicalTeam team, int distance)
    {
        Node maxInjured = null;
        float maxInjuredValue = 0;
        float ratio = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerController>().numSites;

        foreach (KeyValuePair<Node, float> pairNodeFloat in Graph.getNodesAtDistance(team.position, distance))
        {
            float totalInjured = Groupments.injuredPopulation(pairNodeFloat.Key.Groupments);

            totalInjured = totalInjured / ((pairNodeFloat.Key.IncomingAndActingMedicalTeamsCount + 1f) * pairNodeFloat.Value);
            if (totalInjured > maxInjuredValue)
            {
                Debug.Log(pairNodeFloat.Key.IncomingAndActingMedicalTeamsCount);
                maxInjured = pairNodeFloat.Key;
                maxInjuredValue = totalInjured;
            }
        }

        if (maxInjuredValue <= 0)
            return null;
        return maxInjured;
    }

    public void AddTeam()
    {
        MedicalTeam team = Instantiate(GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerController>().medicalTeam).GetComponent<MedicalTeam>();
        FTeams.Add(team);
        team.transform.parent = transform;
        team.position = location;
        team.origin = this;
        team.transform.position = team.position.Tile.transform.position;
    }

    public int getTeamCount()
    {
        return FTeams.Count;
    }
}
