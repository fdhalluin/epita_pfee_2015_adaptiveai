﻿/*
Copyright 2016 A.L.E.R.T.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/





using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public float timerValue = 3; // Timer value in minutes

    private Text text;
    private float timeRemaining;
	private float cashTime;
    private bool isRunning = true;
	private bool isStarted = false;

	public void setStarted(bool started)
	{
		isStarted = started;
	}

    void Start()
    {
        text = GetComponent<Text>();
        timeRemaining = timerValue * 60 - 1;
		cashTime = timerValue * 60 - 30;
		UpdateTimerText();
    }

    void Update()
    {
		//int hospitalsLeft = GameObject.Find("CanvasInfos").GetComponent<InfoDisplay>().hospitalsLeft;
		if (isStarted)
		{
			if (isRunning && timeRemaining >= 0)
				timeRemaining -= Time.deltaTime;

			if (timeRemaining < cashTime)
			{
				cashTime -= 30;
				PlayerController controller = GameObject.FindWithTag("MainCamera").GetComponent<PlayerController>();
				controller.funds += 30000;
			}

			if (timeRemaining < 0)
				EndTimer();

			UpdateTimerText();
		}
    }

    private void UpdateTimerText()
    {
        if (timeRemaining >= 0)
        {
            int min = ((int)timeRemaining + 1) / 60;
            int sec = (int)timeRemaining - min * 60 + 1;

            text.text = "";
            if (min < 10)
                text.text += "  ";
            text.text += min + ":";

            if (sec < 10)
                text.text += "0" + sec;
            else
                text.text += sec;
        }
        else
            text.text = "  0:00";
    }

    public void ResetTimer()
    {
		GameObject.Destroy(GameObject.Find("StartScreen"));
        Application.LoadLevel("Start");
    }

	public void EndTimer()
	{
		InfoDisplay infos = GameObject.Find("Canvas").GetComponent<InfoDisplay>();
		StartScreen startScreen = GameObject.Find("StartScreen").GetComponent<StartScreen>();
		int finalScore = infos.score;
		double percentInjured = System.Math.Round(infos.injuredScale * 100);
		double percentDamage = System.Math.Round(infos.damageScale * 100);
		Application.LoadLevel("End");
		startScreen.score = finalScore;
		startScreen.percentDamage = percentDamage;
		startScreen.percentInjured = percentInjured;
	}

    public void AddTime(float value)
    {
        timeRemaining += value;
    }
}
